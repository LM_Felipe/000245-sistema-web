const {spawn} = require('child_process');
let CNPJ = require('consulta-cnpj-ws');
let cnpj = new CNPJ();
const bcrypt = require('bcrypt-nodejs');
const nodemailer = require('nodemailer');
var https = require('https');
const archiver = require('archiver');
var fs = require('fs');



// app/routes.js
module.exports = function(app, passport/*,bot*/) {

	app.post('/get_fotos', async function(req, res){

		var ano = req.body.selectAno;
		var mes = req.body.selectMes;
		var tipo = req.body.selectTipo;

		if (tipo=='Cartão') {
			tipo = "concat('',cartao*1)";
		}

		

		// create a file to stream archive data to.
		const output = fs.createWriteStream('public/fotos.zip');
		const archive = archiver('zip', {
		  zlib: { level: 9 } // Sets the compression level.
		});

		var select_query=`select caminho_imagem from notinhas where month(str_to_date(data,'%d/%m/%y'))=? and year(str_to_date(data,'%d/%m/%y'))=? and cartao=?`;
		connection.query(select_query,[mes,ano,tipo],function(err, recordset){
			if (err) {
				console.log(err);
			}
			else{
				console.log(select_query);

				for (i of recordset){
					archive.file('public/'+i['caminho_imagem'], { name: i['id'] });
					//console.log(i['caminho_imagem'])
				}
				// pipe archive data to the file
				archive.pipe(output);

				// finalize the archive (ie we are done appending files but streams have to finish yet)
				// 'close', 'end' or 'finish' may be fired right after calling this method so register to them beforehand
				archive.finalize();
				res.send('ok');
			}			
		})


	});

	app.post('/get_ano_notinhas', isLoggedIn, function(req, res){

		var select_query=`select date_format(str_to_date(data,'%d/%m/%Y'),'%Y') as date from notinhas where date_format(str_to_date(data,'%d/%m/%Y'),'%Y') is not null group by date order by date desc`;
		connection.query(select_query,function(err, recordset){
			if (err) {
				console.log(err);
			}
			else{
				console.log(recordset);
				res.send(recordset);
			}			
		})
	});

	app.post('/get_mes_notinhas', isLoggedIn, function(req, res){

		var select_query=`select date_format(str_to_date(data,'%d/%m/%Y'),'%m') as date from notinhas where YEAR(str_to_date(data,'%d/%m/%Y'))=2021 group by date`;
		connection.query(select_query, function(err, recordset){
			if (err) {
				console.log(err);
			}
			else{
				console.log(recordset);
				res.send(recordset);
			}			
		})
	});

	app.get('/get_fotos', async function(req, res){

		// create a file to stream archive data to.
		const output = fs.createWriteStream('example.zip');
		const archive = archiver('zip', {
		  zlib: { level: 9 } // Sets the compression level.
		});

		var select_query=`select caminho_imagem from notinhas where month(str_to_date(data,'%d/%m/%y'))=6 and cartao='Caixa' and status = 'Lancada'`;
		connection.query(select_query,function(err, recordset){
			if (err) {
				console.log(err);
			}
			else{
				console.log(recordset);

				for (i of recordset){
					archive.file('public/'+i['caminho_imagem'], { name: i['caminho_imagem'] });
					//console.log(i['caminho_imagem'])
				}
				// pipe archive data to the file
				archive.pipe(output);

				// finalize the archive (ie we are done appending files but streams have to finish yet)
				// 'close', 'end' or 'finish' may be fired right after calling this method so register to them beforehand
				archive.finalize().then(result =>{
					res.download('example.zip');

				})
			}			
		})


	});

	app.post('/get_mes_relatorio', isLoggedIn, function(req, res){
		var colab = req.body.colab;

		var select_query=`select date_format(str_to_date(data,'%d/%m/%Y'),'%m/%Y') as date from relatorio where colaborador=? group by date`;
		connection.query(select_query,[colab], function(err, recordset){
			if (err) {
				console.log(err);
			}
			else{
				console.log(recordset);
				res.send(recordset);
			}			
		})
	});

	app.post('/get_colab_relatorio', isLoggedIn, function(req, res){

		var select_query=`select distinct colaborador from relatorio order by colaborador asc`;
		connection.query(select_query, function(err, recordset){
			if (err) {
				console.log(err);
			}
			else{
				console.log(recordset);
				res.send(recordset);
			}			
		})
	});

	app.post('/erro_abastecimento', isLoggedIn, function(req, res) {

		url_id = req.param('id');
		observacoes = req.param('observacoes');
		console.log("id = " + url_id);

	    var insertQuery = "update my_schema.abastecimento set status='Erro detectado' WHERE id = ?";
	        connection.query(insertQuery,[url_id], function(err, rows){
	            if (err) {
	                //return done(err
					req.flash('error', err);
				}
				else 
				{
	            	req.flash('ver_abastecimentos_sucess', 'Erro reportado com sucesso');
	            	var insertQuery = "select chat_id, msg_id from my_schema.abastecimento where id = ?";
	            	connection.query(insertQuery,[url_id], function(err, recordset){
	            		if (err) {
	            			req.flash('error', err);
	            		}
	            		else
	            		{
	            			var chat_id = recordset[0]["chat_id"];
	            			var msg_id = recordset[0]["msg_id"];

	            			var insertQuery = "select *from my_schema.dados_usuario A left join my_schema.users B on A.username = B.username WHERE B.id = ?";
	            			connection.query(insertQuery,[req.user.id], function(err, recordset){
	            				if (err) {
	            					req.flash('error', err);
	            				}
	            				else
	            				{
	            					var nome = recordset[0]["nome"];
		                			/*bot.telegram.sendMessage(chat_id, 
		                				"Aviso:\nFoi reportado um erro no abastecimento de id de número: " 
		                				+ url_id + ".\n\nObservações do(a) " + nome + ": " + observacoes, {'reply_to_message_id':msg_id});    */ 
		                			
		                			text = "Aviso:%0AFoi reportado um erro no abastecimento de id de número: "+ url_id + ".%0A%0AObservações do(a) " + nome + ": " + observacoes
									token = "1664029734:AAGLO1anpVlfAQ4_mTcesN0r6ZD6pwcWACk"
									url_req = "https://api.telegram.org/bot" + token + "/sendMessage" + "?chat_id=" + chat_id + "&text=" + text 
									
									https.get(url_req);              					
	            				}
	            			});
	            		}
	            	});

	            	res.redirect('/ver_abastecimentos');          
	            }
	            
	        });


	});






	app.post('/erro_deslocamento', isLoggedIn, function(req, res) {

	    url_id = req.param('id');
	    observacoes = req.param('observacoes');

	    var insertQuery = "update my_schema.deslocamentos set status='Erro detectado' WHERE id = ?";
	        connection.query(insertQuery,[url_id], function(err, rows){
	            if (err) {
	                //return done(err
	                req.flash('error', err);
	            }
	            else 
	            {
	                req.flash('ver_deslocamentos_sucess', 'Erro reportado com sucesso');

	                var insertQuery = "select chat_id, msg_id from my_schema.deslocamentos where id = ?";
	                connection.query(insertQuery,[url_id], function(err, recordset){
	                    if (err) {
	                        req.flash('error', err);
	                    }
	                    else
	                    {
	                        var chat_id = recordset[0]["chat_id"];
	                        var msg_id = recordset[0]["msg_id"];

	                        var insertQuery = "select *from my_schema.dados_usuario A left join my_schema.users B on A.username = B.username WHERE B.id = ?";
	                        connection.query(insertQuery,[req.user.id], function(err, recordset){
	                            if (err) {
	                                req.flash('error', err);
	                            }
	                            else
	                            {
	                                var nome = recordset[0]["nome"];/*
	                                bot.telegram.sendMessage(chat_id, 
	                                "Aviso:\nFoi reportado um erro no deslocamento de id de número: " 
	                                + url_id + ".\n\nObservações: do(a) " + nome + ": " + observacoes, {'reply_to_message_id':msg_id});   */    
	                                

	                                text = "Aviso:%0AFoi reportado um erro no deslocamento de id de número: "+ url_id + ".%0A%0AObservações do(a) " + nome + ": " + observacoes
									token = "1664029734:AAGLO1anpVlfAQ4_mTcesN0r6ZD6pwcWACk"
									url_req = "https://api.telegram.org/bot" + token + "/sendMessage" + "?chat_id=" + chat_id + "&text=" + text 
									
									https.get(url_req);  
								}
	                        });
	                    }
	                });


	                res.redirect('/ver_custos_todos');           
	            }
	            
	        });

	});




	app.post('/erro_nota', isLoggedIn, function(req, res) {

	    url_id = req.param('id');
	    observacoes = req.param('observacoes');

	    var insertQuery = "update my_schema.notinhas set status='Erro detectado' WHERE id = ?";
	        connection.query(insertQuery,[url_id], function(err, rows){
	            if (err) {
	                //return done(err
	                req.flash('error', err);
	            }
	            else 
	            {
	                req.flash('ver_deslocamentos_sucess', 'Erro reportado com sucesso');

	                var insertQuery = "select chat_id, msg_id from my_schema.notinhas where id = ?";
	                connection.query(insertQuery,[url_id], function(err, recordset){
	                    if (err) {
	                        req.flash('error', err);
	                    }
	                    else
	                    {
	                        var chat_id = recordset[0]["chat_id"];
	                        var msg_id = recordset[0]["msg_id"];

	                        var insertQuery = "select *from my_schema.dados_usuario A left join my_schema.users B on A.username = B.username WHERE B.id = ?";
	                        connection.query(insertQuery,[req.user.id], function(err, recordset){
	                            if (err) {
	                                req.flash('error', err);
	                            }
	                            else
	                            {
	                                var nome = recordset[0]["nome"];/*
	                                bot.telegram.sendMessage(chat_id, 
	                                "Aviso:\nFoi reportado um erro no custo de id de número: " 
	                                + url_id + ".\n\nObservações: do(a) " + nome + ": " + observacoes, {'reply_to_message_id':msg_id});    */  

	                                text = "Aviso:%0AFoi reportado um erro no custo de id de número: "+ url_id + ".%0A%0AObservações do(a) " + nome + ": " + observacoes
									token = "1664029734:AAGLO1anpVlfAQ4_mTcesN0r6ZD6pwcWACk"
									url_req = "https://api.telegram.org/bot" + token + "/sendMessage" + "?chat_id=" + chat_id + "&text=" + text 
									
									https.get(url_req);                             
	                            }
	                        });
	                    }
	                });


	                res.redirect('/ver_custos_todos');           
	            }
	            
	        });


	});







	app.post('/finalizaProjeto', isLoggedIn, function(req, res) {

		url_cc = req.param('cc');

	    var insertQuery = "UPDATE my_schema.projetos SET status='finalizado' WHERE cc = ?";
        connection.query(insertQuery,[url_cc], function(err, rows){
            if (err) {
                //return done(err
				req.flash('error', err);
			}
			else 
			{
				console.log('Centro de custo '+url_cc+' finalizado com sucesso');
            	req.flash('editar_projeto_sucess', 'Projeto finalizado com sucesso');
            	res.redirect('/editar_projeto');           
            }
            
        });

	});

	app.get('/registro_abastecimentos', function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT id, D.nome, valor, cnpj, cc, \`data\`, A.cartao, placa, litros, km, status, descricao FROM my_schema.abastecimento A
							left join dados_usuario D on A.chat_id=D.id_telegram WHERE STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE(?,'%d/%m/%Y') AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE(?,'%d/%m/%Y') order by id desc`;
            
            connection.query(insertQuery, [url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT id, nome, valor, cnpj, cc, \`data\`, cartao, placa, litros, km, status, descricao FROM my_schema.abastecimento left join dados_usuario D on A.chat_id=D.id_telegram`;
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }
	});	


	app.get('/ver_abastecimentos', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "ver_abastecimentos"], function(err, recordset){

	        if (err) {
	        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('ver_abastecimentos.ejs', { message: req.flash('ver_abastecimentos_sucess')});
        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    	});
	});

	app.post('/get_chart', isLoggedIn, function(req, res){

		var data = new Date(req.param('mes'));
		var valor = 0;

		//var insertQuery = "select * from my_schema.notinhas WHERE SUBSTRING(data, 6, 1) = ?";
		var insertQuery = "select CAST(str_to_date(data,'%d/%m/%Y') AS char) as data, cc, valor from my_schema.notinhas where status = 'Lancada'";
        connection.query(insertQuery, [mes,ano], function(err, recordset){
            if (err) {
                //return done(err
				req.flash('error', err);
			}
			else 
			{

				for(i of recordset){
					
					var data_select = new Date(i['data']);

					if(data_select.getMonth() == (data.getMonth() - 1) && data_select.getYear() == data.getYear())
					{
						valor = valor + i['valor'];
					}

				}
				res.send([valor]);
            }
            
        });

	});


	app.post('/get_despesas_mes', isLoggedIn, function(req, res){

		var data = new Date(req.param('mes'));
		var valor = 0;

		var mes = data.getMonth();
		var ano = data.getYear();
		ano = ano + 1900;

		var select_query=`select CAST(str_to_date(data,'%d/%m/%Y') AS char) as data, cc, valor 
		from my_schema.notinhas where status = 'Lancada' and MONTH(str_to_date(data,'%d/%m/%Y'))=?
		and YEAR(str_to_date(data,'%d/%m/%Y'))=?`;
		connection.query(select_query, [mes,ano], function(err, recordset){
			if (err) {
				console.log(err);
			}
			else{
				for(i of recordset){
					
					var data_select = new Date(i['data']);

					valor = valor + i['valor'];
				}
				res.send([valor]);
			}			
		})
	});


	app.post('/get_despesas_dia', isLoggedIn, function(req, res){

		var valor = [];

		var data = new Date(req.param('mes'));

		var mes = data.getMonth();
		var ano = data.getYear();
		ano = ano + 1900;
		
		var j = 0;

		var select_query=`select CAST(str_to_date(data,'%d/%m/%Y') AS char) as data, 
		cc, sum(valor) as valor from my_schema.notinhas where status = 'Lancada' and 
		MONTH(str_to_date(data,'%d/%m/%Y'))=? and YEAR(str_to_date(data,'%d/%m/%Y'))=? 
		group by data order by data asc`;
		connection.query(select_query, [mes, ano], function(err, recordset){
			if (err) {
				console.log(err);
			}
			else{
				for(i of recordset){

					var data_select = new Date(i['data']);
					var push_vetor = i['valor'];
					valor.push(push_vetor);

					j++;
					
				}

				var vetor_res = [valor, recordset];

				res.send(vetor_res);
			}			
		})
	});

	app.get('/grafico', function(req, res) {
    	res.render('chartjs.ejs');
	});

	app.get('/base', function(req, res) {
    	res.render('base.ejs', { message: ''});
	});

	// Chama arquivo em python para ler tags do CLP
	app.post('/base_leitura', isLoggedIn, function(req, res) {
		var dado;
		
		// spawn new child process to call the python script
		var processo = spawn('../../Users/lmrmattos/AppData/Local/Programs/Python/Python39/python.exe',['./python/readTAG.py']);

		// collect data from script
		processo.stdout.on('data', function (data) {
			dado = data;
		});			
		
		// in close event we are sure that stream is from child process is closed
		processo.on('close', (code) => {
			console.log(`child process close all stdio with code ${code}`);
			// send data to browser
			res.send(dado)

			return;
	 	});
	});

	app.post('/base_escrita', isLoggedIn, function(req, res) {
		var arg = req.body.sarr_l_conta;
		console.log('\n\n'+arg+'\n\n')
		var dado;
		
		// spawn new child process to call the python script
		var processo = spawn('../../Users/lmrmattos/AppData/Local/Programs/Python/Python39/python.exe',['./python/writeTAG.py', arg]);

		// collect data from script
		processo.stdout.on('data', function (data) {
			dado = data;
		});			

		// in close event we are sure that stream is from child process is closed
		processo.on('close', (code) => {
			console.log(`child process close all stdio with code ${code}`);
			// send data to browser
			res.send(dado)

			return;
	 	});
	});

	// Chama arquivo em python para verificar comunicação com CLP
    app.post('/confere_lifecouter',function(req,res){	
		var dado;
		
		// spawn new child process to call the python script
		var processo = spawn('../../Users/lmrmattos/AppData/Local/Programs/Python/Python39/python.exe',['./python/comunica_clp.py']);

		// collect data from script
		processo.stdout.on('data', function (data) {
			dado = data;
		});			

		// in close event we are sure that stream is from child process is closed
		processo.on('close', (code) => {
			console.log(`child process close all stdio with code ${code}`);
			// send data to browser
			res.send(dado)

			return;
	 	});
    }); 	

    // Executa arquivo que escreve o tempo de timeout no CLP
	app.post('/definir_timeout', isLoggedIn, function(req, res) {
		var arg = req.body.tempo_timeout;
		arg = arg*1000;
		var dado;

		// spawn new child process to call the python script
		var processo = spawn('../../Users/lmrmattos/AppData/Local/Programs/Python/Python39/python.exe',['./python/definir_timeout.py', arg]);

		// collect data from script
		processo.stdout.on('data', function (data) {
			dado = data;
		});			

		// in close event we are sure that stream is from child process is closed
		processo.on('close', (code) => {
			console.log(`child process close all stdio with code ${code}`);
			// send data to browser
			res.send(dado)

			return;
	 	});
	});

    // Chama arquivo em python para abrir porta da recepção
	app.post('/abre_porta_base', isLoggedIn, function(req, res) {
		var dado;
		
		// spawn new child process to call the python script
		var processo = spawn('../../Users/lmrmattos/AppData/Local/Programs/Python/Python39/python.exe',['./python/abre_porta.py']);

		// collect data from script
		processo.stdout.on('data', function (data) {
			dado = data;
		});			

		// in close event we are sure that stream is from child process is closed
		processo.on('close', (code) => {
			console.log(`child process close all stdio with code ${code}`);
			// send data to browser
			res.send(dado)

			return;
	 	});
	});

    // Chama arquivo em python para abrir portão externo
	app.post('/abre_portao_base', isLoggedIn, function(req, res) {
		var dado;
		
		// spawn new child process to call the python script
		var processo = spawn('../../Users/lmrmattos/AppData/Local/Programs/Python/Python39/python.exe',['./python/abre_portao.py']);

		// collect data from script
		processo.stdout.on('data', function (data) {
			dado = data;
		});			

		// in close event we are sure that stream is from child process is closed
		processo.on('close', (code) => {
			console.log(`child process close all stdio with code ${code}`);
			// send data to browser
			res.send(dado)

			return;
	 	});
	});

	// Página do Power Monitor
	app.get('/power_monitor_base', function(req, res) {
    	res.render('power_monitor_base.ejs');
	});

	// Pega histórico do Power Monitor
	app.get('/registro_power_monitor_base', function(req, res) {
        var insertQuery = `SELECT convert(tempo, char) as time,round(corrente_l1,2) as corrente_l1,
        round(corrente_l2,2) as corrente_l2,round(corrente_l3,2) as corrente_l3,
        round(tensao_l1_l2,2) as tensao_l1_l2,round(tensao_l2_l3,2) as tensao_l2_l3,
        round(tensao_l3_l1,2) as tensao_l3_l1,round(tensao_l1_n,2) as tensao_l1_n,round(tensao_l2_n,2) as 
        tensao_l2_n, round(tensao_l3_n,2) as tensao_l3_n,round(frequencia,2) as frequencia 
        FROM my_schema.power_monitor_base order by tempo desc limit 1000`;
            
        connection.query(insertQuery, function(err, recordset){
            if (err) console.log(err)
            console.log(recordset[0]['time'])
            res.send(recordset);
		});
	});

	// Pega histórico de falhas de comunicação com o CLP
	app.get('/registro_comunicacao_clp', function(req, res) {
        var insertQuery = `SELECT contagem,(tempo_falha/1000) as tempo_falha,convert(momento_falha, char) 
        as momento_falha from registro_falha_comunicacao_clp order by contagem desc limit 1000`;
            
        connection.query(insertQuery, function(err, recordset){
            if (err) console.log(err)
            console.log(recordset[0]['time'])
            res.send(recordset);
		});
	});	

	// Página do controle de log do bot do Telegram
	app.get('/controle_log_bot', function(req, res) {
    	res.render('controle_log_bot.ejs');
	});

	// Pega histórico de log do bot do Telegram
	app.get('/registro_log_bot', function(req, res) {
        var insertQuery = `SELECT convert(tempo, char) as tempo, alteracao, nome FROM 
        					my_schema.controle_log_bot A, dados_usuario B where id_telegram = 
        					chat_id order by tempo desc limit 1000`;
            
        connection.query(insertQuery, function(err, recordset){
            if (err) console.log(err)
            res.send(recordset);
		});
	});

	// Página de log de geração de relatórios pelo bot do Telegram
	app.get('/log_relatorio', function(req, res) {
    	res.render('log_relatorio.ejs');
	});

	// Pega histórico de log dos relatórios do bot
	app.get('/registro_log_relatorio', function(req, res) {
        var insertQuery = `select nome, filtro_data, filtro_usuario, filtro_acao, convert(tempo, char) as tempo 
        					from log_relatorio left join dados_usuario on chat_id = id_telegram`;
            
        connection.query(insertQuery, function(err, recordset){
            if (err) console.log(err)
            res.send(recordset);
		});
	});

	app.post('/getData',function(req,res){
        var y = req.body.y;
        var x = req.body.x;
        var escala = req.body.escala;
        var order = req.body.order;
        var filtro=req.body.ano;
        var status=req.body.status;
        var colaborador = '';
        var join = '';
        console.log('\n\nstatus = '+status+'\n\n')
        if(status=='lancada' || status == undefined){
        	status = `status='Lancada' AND`;
        }
        else if(status=='lancada_aberto'){
        	status = `(status='Lancada' or status='Aberto') and`;
        }        
        else{
        	status = `cc=cc AND`;
        }
        if (escala=='ano') {
            formatoData='%Y';
            AnoMes='ano';
        }else{
            formatoData='%m/%Y';
            AnoMes='mes';           
        }
        if (x=='data') {
            ccDATA=AnoMes
        }else if(x=='cc'){
            ccDATA='trim(leading 0 from cc)'
        }
        else{
        	ccDATA=`D.nome`;
        	colaborador = ccDATA+' as eixo_x,';
        	join = `left join dados_usuario D on D.id_telegram = A.chat_id`;
        }
        if (filtro=='Todos') {
            filtro=`AND (DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+2020+` or
            DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+2021+`
        	or DATE_FORMAT(str_to_date(data,'%d/%m/%y'),'%y') = `+21+`)`;
        }else{
            filtro='='+filtro;
        }
        if(x=='data'){
            var AscDesc='asc';
            //order="str_to_date("+AnoMes+",'"+formatoData+"')";
            order = "DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')";
        }
        else{
            var AscDesc='desc';
            order='eixo_y';
        }        
        if(y == 'despesas'){
            tabela = 'notinhas A';
            eixo_y = 'sum(valor) as eixo_y';
        }
        else if(y == 'deslocamentos'){
        	tabela = 'deslocamentos A';
        	eixo_y = 'sum(final - inicial) as eixo_y';
        }
        else if(y == 'abastecimento'){
        	tabela = 'abastecimento A';
        	eixo_y = 'sum(valor) as eixo_y';
        }
        else{
        	eixo_y = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60) as eixo_y`;
 			if(x=='cc'){
 				ccDATA = 'trim(leading 0 from cc)';
 				colab = ccDATA+' as eixo_x,';
 				grupo = 'trim(leading 0 from cc)';
 				order = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60)`;
				AscDesc = 'desc';
 			}
			else if(x=='colaborador'){
 				ccDATA = `D.nome`;
 				colab = ccDATA+' as eixo_x,';
 				grupo = `D.nome`;
 				join = `left join dados_usuario D on D.id_telegram = A.id_telegram`;
 				order = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60)`;
				AscDesc = 'desc';
			}
			else{
				teste="data,'%d/%m/%Y')";
	        	ccDATA = "DATE_FORMAT(str_to_date("+teste+",'"+formatoData+"')";
	        	colab = '';
	        	grupo = "DATE_FORMAT(str_to_date("+teste+",'"+formatoData+"')";
	        	order = "DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')";
			}
        	
        	filtro_mes = ''; 
        	tabela = 'relatorio A';
        	status = '';
        	colaborador = colab;
        }
        var select_query = `select `+colaborador+` trim(leading 0 from cc) as cc, DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'`+formatoData+`') 
        as `+AnoMes+`,`+eixo_y+` from `+tabela+` `+join+` where cc < 1000 and `+status+` 
        DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') `+filtro+` group by `+ccDATA+` 
        order by `+order+` `+AscDesc;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });

	app.post('/getData2',function(req,res){
        var y = req.body.y;
        var x = req.body.x;
        var order = req.body.order;
        var status=req.body.status;
        var colaborador = '';
        var join = '';
        var ini=req.body.ini;
        var fim=req.body.fim;
        var grafico=req.body.grafico;
		order = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y'),
	        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%m/%Y'),
	        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')`;
        AnoMes="data";
        formatoData='%d/%m/%Y';
        if(status=='lancada' || status == undefined){
        	status = `status='Lancada' AND`;
        }
        else if(status=='lancada_aberto'){
        	status = `(status='Lancada' or status='Aberto') and`;
        }        
        else{
        	status = `cc=cc AND`;
        }
        /*if (escala=='ano') {
            formatoData='%Y';
            AnoMes='ano';
        }else{
            formatoData='%m/%Y';
            AnoMes='mes';           
        }*/
        if (x=='data') {
            ccDATA=AnoMes
        }else if(x=='cc'){
            ccDATA='trim(leading 0 from cc)'
        }
        else{
        	ccDATA=`D.nome`;
        	colaborador = ccDATA+' as eixo_x,';
        	join = `left join dados_usuario D on D.id_telegram = A.chat_id`;
        }
        /*if (filtro=='Todos') {
            filtro=`AND (DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+2020+` or
            DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+2021+`
        	or DATE_FORMAT(str_to_date(data,'%d/%m/%y'),'%y') = `+21+`)`;
        }else{
            filtro='='+filtro;
        }*/
        if(x=='data'){
            var AscDesc='asc';
            //order="str_to_date("+AnoMes+",'"+formatoData+"')";
            order = "DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')";
        }
        else{
            var AscDesc='desc';
            order='eixo_y';
        }        
        if(y == 'despesas'){
            tabela = 'notinhas A';
            eixo_y = 'sum(valor) as eixo_y';
        }
        else if(y == 'deslocamentos'){
        	tabela = 'deslocamentos A';
        	eixo_y = 'sum(final - inicial) as eixo_y';
        }
        else if(y == 'abastecimento'){
        	tabela = 'abastecimento A';
        	eixo_y = 'sum(valor) as eixo_y';
        }
        else{
        	eixo_y = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60) as eixo_y`;
 			if(x=='cc'){
 				ccDATA = 'trim(leading 0 from cc)';
 				colab = ccDATA+' as eixo_x,';
 				grupo = 'trim(leading 0 from cc)';
 				order = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60)`;
				AscDesc = 'desc';
 			}
			else if(x=='colaborador'){
 				ccDATA = `D.nome`;
 				colab = ccDATA+' as eixo_x,';
 				grupo = `D.nome`;
 				join = `left join dados_usuario D on D.id_telegram = A.id_telegram`;
 				order = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60)`;
				AscDesc = 'desc';
			}
			else{
				teste="data,'%d/%m/%Y')";
	        	ccDATA = "DATE_FORMAT(str_to_date("+teste+",'"+formatoData+"')";
	        	colab = '';
	        	grupo = "DATE_FORMAT(str_to_date("+teste+",'"+formatoData+"')";
	        	order = "DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')";
			}
        	
        	filtro_mes = ''; 
        	tabela = 'relatorio A';
        	status = '';
        	colaborador = colab;
        }
	    if(grafico == 'pizza'){
	    	order = `eixo_y`;
	    	AscDesc = `desc`;
	    }        
        var select_query = `select `+colaborador+` trim(leading 0 from cc) as cc, 
        DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'`+formatoData+`') 
        as data, `+eixo_y+` from `+tabela+` `+join+` where cc < 1000 and
        STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE('`+ini+`','%d/%m/%Y') 
		AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE('`+fim+`','%d/%m/%Y') and `+status+` 
        DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') group by `+ccDATA+` 
        order by `+order+` `+AscDesc;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });

	app.post('/getDataCard',function(req,res){
        var y = req.body.y;
        var cc = req.body.cc;
        var colaborador = req.body.colaborador;
        var ini=req.body.ini;
        var fim=req.body.fim;
        if(cc == 'todos'){
        	cc = 'cc';
        }
        if(colaborador == 'todos'){
        	nome = '';
        }
        else{
        	nome = `and D.nome = '`+colaborador+`'`;
        }
        join = `left join dados_usuario D on D.id_telegram = A.chat_id`;
        status = `and status = 'Lancada' `;  
        if(y == 'despesas'){
            tabela = 'notinhas A';
            eixo_y = 'sum(valor) as eixo_y';
        }
        else if(y == 'deslocamentos'){
        	tabela = 'deslocamentos A';
        	eixo_y = 'sum(final - inicial) as eixo_y';
        }
        else if(y == 'abastecimento'){
        	tabela = 'abastecimento A';
        	eixo_y = 'sum(valor) as eixo_y';
        }
        else{
        	eixo_y = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60) as eixo_y`;
        	tabela = 'relatorio A';
        	join = `left join dados_usuario D on D.id_telegram = A.id_telegram`;
        	status = '';
        }      
        var select_query = `select `+eixo_y+`, cc, D.nome from `+tabela+` `+join+` 
        where cc < 1000 and cc = `+cc+` `+nome+` `+status+` and
        STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE('`+ini+`','%d/%m/%Y') 
		AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE('`+fim+`','%d/%m/%Y')`;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });        

	app.post('/getDataCC',function(req,res){
		console.log('\n\nooo\n\n')
        var y = req.body.y;
        var x = req.body.x;
        var order = req.body.order;
        var ccNome=req.body.cc;
        var join = '';
        var status=req.body.status;
        var as='';
        var ini=req.body.ini;
        var fim=req.body.fim;
		order = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y'),
	        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%m/%Y'),
	        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')`;
        AnoMes="data";
        formatoData='%d/%m/%Y';                
        if(status=='lancada'){
        	status = `status='Lancada'`;
        }
        else if(status=='lancada_aberto'){
        	status = `(status='Lancada' or status='Aberto')`;
        }        
        else{
        	status = `cc=cc`;
        }        
        if(y == 'despesas'){
            tabela = 'notinhas A';
            eixo_y = 'sum(valor) as eixo_y';
        }
        else if(y == 'deslocamentos'){
        	tabela = 'deslocamentos A';
        	eixo_y = 'sum(final - inicial) as eixo_y';
        }
        else if(y == 'abastecimento'){
        	tabela = 'abastecimento A';
        	eixo_y = 'sum(valor) as eixo_y';	
        }
        else{
        	tabela = 'relatorio A';
        	eixo_y = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60) as eixo_y`;
 			status = `cc=cc`;
 			join = `left join dados_usuario D on D.id_telegram = A.id_telegram`;	
        }
        /*if (escala=='ano') {
            formatoData='%Y';
            AnoMes='ano';
        }else{
            formatoData='%m/%Y';
            AnoMes='mes';           
        }*/
        if (x=='data') {
            ccDATA=AnoMes;
            var hasNumber = /\d/;
            if(hasNumber.test(ccNome)){
            	comCC=`and trim(leading 0 from cc) = `+ccNome;
            }
            else{
            	comCC=`and SUBSTRING_INDEX(SUBSTRING_INDEX(D.nome, ' ', 1), ' ', -1) = '`+ccNome+`'`;
	        	if(tabela != 'relatorio A')
	        	{
	        		join = `left join dados_usuario D on D.id_telegram = A.chat_id`;
	        	}
            }
            AscDesc = 'desc';
            as = 'as eixo_x';
        }else if(x=='colaborador'){
        	comCC=`and SUBSTRING_INDEX(SUBSTRING_INDEX(D.nome, ' ', 1), ' ', -1) = '`+ccNome+`'`;
        	ccDATA = 'trim(leading 0 from cc)';
        	AscDesc = 'desc';
        	as = 'as eixo_x';
        	if(tabela != 'relatorio A')
        	{
        		join = `left join dados_usuario D on D.id_telegram = A.chat_id`;
        	}        	
        }
        else{
            ccDATA='trim(leading 0 from cc)';
            comCC='';
            AscDesc = 'desc';
            as = 'as cc';
        }
        /*if (filtro=='Todos') {
            filtro=`AND (DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+2020+` or
            DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+2021+`
        	or DATE_FORMAT(str_to_date(data,'%d/%m/%y'),'%y') = `+21+`)`;
        }else{
            filtro='='+filtro;
        }*/
        /*if(order=='data'){
            var AscDesc='asc';
            TotalData="str_to_date("+AnoMes+",'"+formatoData+"')";
        }
        else{
            var AscDesc='desc';
            TotalData='eixo_y';
        }*/
        TotalData='eixo_y';
        var select_query = `select trim(leading 0 from cc) `+as+`, 
        DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'`+formatoData+`') 
        as `+AnoMes+`,`+eixo_y+` from `+tabela+` `+join+` where cc < 1000 and
        STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE('`+ini+`','%d/%m/%Y') 
		AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE('`+fim+`','%d/%m/%Y') and
		 `+status+` AND DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') `+comCC+` 
		group by `+ccDATA+` order by `+TotalData+` `+AscDesc;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });    

	app.post('/getInfoGrafico',function(req,res){
        var info=req.body.info;
        var eixo_y=req.body.y;
        var status=req.body.status;
        var horasCC=req.body.horasCC;
        var ccs=req.body.ccs;
        var join = '';
        var ini=req.body.ini;
        var fim=req.body.fim;
		order = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y'),
	        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%m/%Y'),
	        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')`;
        AnoMes="data,'%d/%m/%Y')";
        formatoData='%d/%m/%Y';
        if(status=='lancada'){
        	status = `status='Lancada'`;
        }
        else if(status=='lancada_aberto'){
        	status = `(status='Lancada' or status='Aberto')`;
        }
        else{
        	status = `cc=cc`;	
        }
        if(info == 'cc'){
        	eixo_x = 'trim(leading 0 from cc)';
        	grupo = 'trim(leading 0 from cc)';
        	AscDesc='desc';
        	order = 'y';
        }
        else if(info == 'data'){
        	eixo_x = "DATE_FORMAT(str_to_date("+AnoMes+",'"+formatoData+"')";
        	grupo = "DATE_FORMAT(str_to_date("+AnoMes+",'"+formatoData+"')";
        	AscDesc='asc';
        }
        else{
        	eixo_x = `D.nome`;
        	grupo = `D.nome`;
        	AscDesc = '';
        	join = `left join dados_usuario D on D.id_telegram = A.chat_id`;
        }
        if(eixo_y == 'despesas'){
        	eixo_y = `sum(valor) as y`;
        	tabela = 'notinhas A';
        }
        else if(eixo_y == 'deslocamentos'){
        	eixo_y = `sum(final - inicial) as y`;
        	tabela = 'deslocamentos A';
        }
        else if(eixo_y == 'abastecimento'){
        	eixo_y = `sum(valor) as y`;
        	tabela = 'abastecimento A'; 
        }
        else{
        	eixo_y = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60) as y`;
 			if(info=='cc'){
 				eixo_x = 'trim(leading 0 from cc)';
 				grupo = 'trim(leading 0 from cc)';
 				where = 'where cc < 1000';
	        	order = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
	 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60)`;
	        	AscDesc = 'desc'; 				
 			}
			else if(info=='colaborador'){
 				eixo_x = `D.nome`;
 				grupo = `D.nome`;
 				where = 'where D.nome = D.nome';
 				join = `left join dados_usuario D on D.id_telegram = A.id_telegram`;
	        	order = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
	 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60)`;
	        	AscDesc = 'desc'; 				
			}
			else{
	        	eixo_x = "DATE_FORMAT(str_to_date("+AnoMes+",'"+formatoData+"')";
	        	grupo = "DATE_FORMAT(str_to_date("+AnoMes+",'"+formatoData+"')";
	        	AscDesc='asc';
			}
        	tabela = 'relatorio A';
        	status = `cc=cc`;
			if(horasCC == 'colaborador'){
				eixo_x = `colaborador`; 
				grupo = `colaborador`;
				status = status+' and trim(leading 0 from cc) = '+ccs;
	        }
	        else if(horasCC == 'funcao'){
	        	eixo_x = `funcao`; 
	        	grupo = `funcao`;
	        	status = status+' and trim(leading 0 from cc) = '+ccs;
	        }
        }
        var where = `where `+status+``;
        var select_query = `select `+eixo_y+`, `+eixo_x+` as eixo_x 
        from `+tabela+` `+join+` `+where+` and cc < 1000 and 
        STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE('`+ini+`','%d/%m/%Y') 
		AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE('`+fim+`','%d/%m/%Y') 
		group by `+grupo+` order by `+order+` `+AscDesc+`
        `;            

        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });

	app.post('/getDataGrafico',function(req,res){
        var info=req.body.info;
        var escala=req.body.escala;
        var mes=req.body.mes;
        var eixo_y=req.body.y;
        var date=req.body.data;
        var status=req.body.status;
        if(status=='lancada'){
        	status = `status='Lancada'`;
        }
        else if(status=='lancada_aberto'){
        	status = `(status='Lancada' or status='Aberto')`;
        }        
        else{
        	status = `cc=cc`;
        }        
        if (escala=='ano') {
            formatoData='%Y';
            AnoMes="data,'%d/%m/%Y')";
            order = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y')`;
            filtro = order;
            var data = [];
            dateString = date.toString();
            if(dateString.includes('/'))
            {            	
            	data = dateString.split("/");
				date = data[2];
            }
			if(date == undefined)
			{
				date = data[1];
				if(date == undefined)
				{
					date = data[0];
				}
			}
        }else if(escala=='mes'){
            formatoData='%m/%Y';
            AnoMes="data,'%d/%m/%Y')";
        	order = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y'),
        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%m/%Y')`;
        	filtro = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y')`;
        	var data = [];
        	dateString = date.toString();
            if(dateString.includes('/'))
            {            	
            	data = dateString.split("/");
				date = data[2];
            }
			if(date == undefined)
			{
				date = data[1];
				if(date == undefined)
				{
					date = data[0];
				}
			}                 	         
        }else{
        	formatoData='%d/%m/%Y';
        	AnoMes="data,'%d/%m/%Y')";
        	order = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')`;
        	filtro = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%m/%Y')`;
        }
        if(mes!=undefined && escala != 'ano'){
        	filtro_mes="AND DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%m') = "+mes;
        }
        else{
        	filtro_mes="";
        }
    	eixo_x = "DATE_FORMAT(str_to_date("+AnoMes+",'"+formatoData+"')";
    	grupo = "DATE_FORMAT(str_to_date("+AnoMes+",'"+formatoData+"')";
    	AscDesc='asc';
    	TotalData="DATE_FORMAT(str_to_date("+AnoMes+",'"+formatoData+"')";
        if(eixo_y == 'despesas'){
        	eixo_y = `sum(valor) as y`;
        	tabela = 'notinhas';
        }
        else if(eixo_y == 'deslocamentos'){
        	eixo_y = `sum(final - inicial) as y`;
        	tabela = 'deslocamentos';
        }
        else if(eixo_y == 'abastecimento'){
        	eixo_y = `sum(valor) as y`;
        	tabela = 'abastecimento';        	
        }
        else{
        	eixo_y = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60) as y`;
        	tabela = 'relatorio';
        	status = `cc=cc`;        	
        }
        var select_query = `select `+eixo_y+`, `+eixo_x+` as eixo_x 
        from `+tabela+` where cc < 1000 and `+status+` and `+filtro+` = '`+date+`' 
        `+filtro_mes+` group by `+grupo+`
        order by `+order+` `+AscDesc+`
        `;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });     

    app.post('/getInfoByCCToData',function(req,res){
        var cc = req.body.cc;
        var eixo_y=req.body.y;
        var x=req.body.x;
        var status=req.body.status;
        var join='';
        var ini=req.body.ini;
        var fim=req.body.fim;
        var colaboradorCCData=req.body.colaboradorCCData;
		order = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y'),
	        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%m/%Y'),
	        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')`;
        AnoMes="data,'%d/%m/%Y')";
        formatoData='%d/%m/%Y';          
        if(status=='lancada'){
        	status = `status='Lancada'`;
        }
        else if(status=='lancada_aberto'){
        	status = `(status='Lancada' or status='Aberto')`;
        }
        else{
        	status = `cc=cc`;	
        }      
        /*if (escala=='ano') {
            formatoData='%Y';
            ordem = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y')`;
        	var data = [];
        	dateString = date.toString();
            if(dateString.includes('/'))
            {            	
            	data = dateString.split("/");
				date = data[2];
            }
			if(date == undefined)
			{
				date = data[1];
				if(date == undefined)
				{
					date = data[0];
				}
			}            
            filtro = `and DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+date;
            if(date == 'Todos')
            {
	            filtro = `and (DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+2020+` 
	            or DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+2021+`)`;         	
            }
        }else if(escala=='mes'){
            formatoData='%m/%Y';
        	ordem = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y'),
        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%m/%Y')`;
        	var data = [];
        	dateString = date.toString();
            if(dateString.includes('/'))
            {            	
            	data = dateString.split("/");
				date = data[2];
            }
			if(date == undefined)
			{
				date = data[1];
				if(date == undefined)
				{
					date = data[0];
				}
			}             	
        	filtro = `and DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+date+``;                   
        }else{
        	formatoData='%d/%m/%Y';
        	ordem = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')`;       	
        	filtro = `and DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%m/%Y') = '`+date+`'`;
        }*/
        grupo = "DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'"+formatoData+"')";
        eixo_x = "DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'"+formatoData+"')";
        AscDesc='asc';
        if(eixo_y == 'despesas'){
        	eixo_y = `sum(valor) as y`;
        	tabela = 'notinhas A';
        }
        else if(eixo_y == 'deslocamentos'){
        	eixo_y = `sum(final - inicial) as y`;
        	tabela = 'deslocamentos A';
        }
        else if(eixo_y == 'abastecimento'){
        	eixo_y = `sum(valor) as y`;
        	tabela = 'abastecimento A';
        }
        else{
        	eixo_y = `sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
 					+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60) as y`;
        	tabela = 'relatorio A';
        	status = `cc=cc`;
        }
    	if(x=='colaborador'){
	    	grupo = 'trim(leading 0 from cc)';
	    	eixo_x = 'trim(leading 0 from cc)';
    	}        
        var hasNumber = /\d/;
        if(hasNumber.test(cc)){
        	var dado = 'trim(leading 0 from cc) = '+cc;
        }
        else{
        	var dado = `SUBSTRING_INDEX(SUBSTRING_INDEX(D.nome, ' ', 1), ' ', -1) = '`+cc+`'`;
        	if(eixo_y != 'sum(valor) as y' && eixo_y != 'sum(final - inicial) as y'){
        		join = `left join dados_usuario D on D.id_telegram = A.id_telegram`;
        	}
        	else{
        		join = `left join dados_usuario D on D.id_telegram = A.chat_id`;
        	}
        }
        if(colaboradorCCData!=''){
        	var dado = `SUBSTRING_INDEX(SUBSTRING_INDEX(D.nome, ' ', 1), ' ', -1) = 
        	'`+colaboradorCCData+`' and trim(leading 0 from cc) = `+cc;
        	if(eixo_y != 'sum(valor) as y' && eixo_y != 'sum(final - inicial) as y'){
        		join = `left join dados_usuario D on D.id_telegram = A.id_telegram`;
        	}
        	else{
        		join = `left join dados_usuario D on D.id_telegram = A.chat_id`;

        	}
        }
        var select_query = `select `+eixo_y+`, `+eixo_x+` as eixo_x 
        from `+tabela+` `+join+` where cc < 1000 and
		STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE('`+ini+`','%d/%m/%Y') 
		AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE('`+fim+`','%d/%m/%Y')
        and `+status+` and `+dado+` group by `+grupo+` order by `+order+` `+AscDesc+``;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });

    app.post('/getCC',function(req,res){
        var select_query = `select trim(leading 0 from cc) as cc from projetos 
        where cc < 1000 group by trim(leading 0 from cc) order by cc;`;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });

    app.post('/getColaborador',function(req,res){
        var select_query = `select nome as colaborador from dados_usuario;`;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });    

    app.post('/getColaboradorByFuncao',function(req,res){
    	var cc=req.body.cc;
    	var funcao=req.body.funcao;
    	var ini=req.body.ini;
    	var fim=req.body.fim;
    	if(funcao == 'T. em Automação' || funcao == 'T. em automação'){
    		funcao = 'Técnico em Automação';
    	}
    	else if(funcao == 'C. de Software'){
    		funcao = 'Coordenador de Software';
    	}
    	else if(funcao == 'C. de Projetos'){
    		funcao = 'Coordenador de Projetos';
    	}
    	else if(funcao == 'C. tecnico'){
    		funcao = 'Coordenador tecnico';	
    	}
        var select_query = `select sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
		+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60)
		as horas, SUBSTRING_INDEX(SUBSTRING_INDEX(colaborador, ' ', 1), ' ', -1)
		as colaborador, trim(leading 0 from cc) as cc
		from relatorio where cc < 1000 and
		STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE('`+ini+`','%d/%m/%Y') 
		AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE('`+fim+`','%d/%m/%Y')
		and trim(leading 0 from cc) = `+cc+`
		and funcao = '`+funcao+`'
		group by SUBSTRING_INDEX(SUBSTRING_INDEX(colaborador, ' ', 1), ' ', -1)`;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });

    app.post('/getColaboradorByData',function(req,res){
    	var cc=req.body.cc;
    	var colaborador=req.body.colaborador;
    	var ini=req.body.ini;
    	var fim=req.body.fim;    	
        /*if (escala=='ano') {
            formatoData='%Y';
            ordem = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y')`;
        	var data = [];
        	dateString = date.toString();
            if(dateString.includes('/'))
            {            	
            	data = dateString.split("/");
				date = data[2];
            }
			if(date == undefined)
			{
				date = data[1];
				if(date == undefined)
				{
					date = data[0];
				}
			}            
            filtro = `and DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+date;
            if(date == 'Todos')
            {
	            filtro = `and (DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+2020+` 
	            or DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+2021+`)`;         	
            }
        }else if(escala=='mes'){
            formatoData='%m/%Y';
        	ordem = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y'),
        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%m/%Y')`;
        	var data = [];
        	dateString = date.toString();
            if(dateString.includes('/'))
            {            	
            	data = dateString.split("/");
				date = data[2];
            }
			if(date == undefined)
			{
				date = data[1];
				if(date == undefined)
				{
					date = data[0];
				}
			}             	
        	filtro = `and DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y') = `+date+``;                   
        }else{
        	formatoData='%d/%m/%Y';
        	ordem = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')`;       	
        	filtro = `and DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%m/%Y') = '`+date+`'`;
        }*/
        grupo = "DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')";
		order = `DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%Y'),
	        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%m/%Y'),
	        	DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')`;        
        eixo_x = "DATE_FORMAT(str_to_date(data,'%d/%m/%Y'),'%d/%m/%Y')";
        AscDesc='asc';
        var select_query = `select sum(hour(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))
		+ minute(TIME_FORMAT(timediff(saida, entrada), '%H:%i'))/60) as horas, `+eixo_x+` 
		as eixo_x from relatorio where cc < 1000 and 
		STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE('`+ini+`','%d/%m/%Y') 
		AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE('`+fim+`','%d/%m/%Y')
		and trim(leading 0 from cc) = `+cc+` 
		and SUBSTRING_INDEX(SUBSTRING_INDEX(colaborador, ' ', 1), ' ', -1) = '`+colaborador+`' 
		group by `+grupo+` order by `+order+` `+AscDesc+``;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });    

    app.post('/progressoProjetos',function(req,res){
        var select_query = `select trim(leading 0 from A.cc) as cc, 
        auxiliar_administrativo, marketing,
        tecnico_de_seguranca, gerente_tecnico, compras, 
        recursos_humanos, coordenador_de_software, coordenador_de_projetos, 
        programador, projetista, tecnico_em_automacao, coordenador_tecnico,
        sum(hour(TIME_FORMAT(timediff(B.saida, B.entrada), 
        '%H:%i')) + minute(TIME_FORMAT(timediff(B.saida, B.entrada), '%H:%i'))/60) as horas_trabalhadas
		from projetos A, relatorio B where trim(leading 0 from A.cc) = 
		trim(leading 0 from B.cc) group by trim(leading 0 from A.cc);`;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });

    app.post('/progressoProjetosFuncao',function(req,res){
    	var cc = req.body.cc;
        var select_query = `select trim(leading 0 from A.cc) as cc, funcao,
							auxiliar_administrativo, marketing, tecnico_de_seguranca, gerente_tecnico, 
							compras, recursos_humanos, coordenador_de_software, coordenador_de_projetos, 
							programador, projetista, tecnico_em_automacao, coordenador_tecnico,
							sum(hour(TIME_FORMAT(timediff(B.saida, B.entrada),
							'%H:%i')) + minute(TIME_FORMAT(timediff(B.saida, B.entrada), '%H:%i'))/60) 
							as horas_trabalhadas from projetos A, relatorio B where 
							trim(leading 0 from A.cc) = trim(leading 0 from B.cc) and 
							trim(leading 0 from A.cc) = `+cc+` group by funcao `;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });

    // Pega dados dos veículos
    app.post('/getDataVeiculo',function(req,res){
        var select_query = `select sum(final - inicial) as km,
        trim(leading 0 from cc) as cc from deslocamentos where cc < 1000 and status = 'Lancada'
        and (final != 0 and inicial != 0)
		group by trim(leading 0 from cc) order by trim(leading 0 from cc) asc`;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });

    // Pega dados dos veículos
    app.post('/getVeiculos',function(req,res){
        var select_query = `select modelo, REPLACE(A.placa, '-', '') as veiculo, 
		max(inicial) as inicial, max(final) as final, B.km_oleo as km_oleo, B.km_revisao 
		as km_revisao, DATE_FORMAT(max(str_to_date(data,'%d/%m/%Y')),'%d/%m/%Y') as data,
		data_oleo, data_lic, data_seg, data_revisao, data_checklist 
		from deslocamentos A, veiculos B where (status = 'Lancada' or status = 'Aberto') 
		and REPLACE(A.placa, '-', '') = REPLACE(B.placa, '-', '') and status_veiculo != 'vendido'
		group by REPLACE(A.placa, '-', '')`;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });

    // Informações sobre troca de óleo
    app.post('/progressoTrocaOleo',function(req,res){
        var select_query = `select REPLACE(B.placa, '-', '') as veiculo, max(B.final) as final, 
        max(B.inicial) as inicial, A.km_oleo, 
		DATE_FORMAT(max(str_to_date(data,'%d/%m/%Y')),'%d/%m/%Y') as data from veiculos A, 
		deslocamentos B where (status = 'Lancada' or status = 'Aberto') and status_veiculo != 'vendido'
		and REPLACE(A.placa, '-', '') = REPLACE(B.placa, '-', '') group by REPLACE(B.placa, '-', '')`;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });

    // Informações sobre revisão dos veículos
    app.post('/progressoRevisao',function(req,res){	
        var select_query = `select REPLACE(B.placa, '-', '') as veiculo, max(B.final) as final, 
        max(B.inicial) as inicial, A.km_revisao, 
		DATE_FORMAT(max(str_to_date(data,'%d/%m/%Y')),'%d/%m/%Y') as data from veiculos A, 
		deslocamentos B where (status = 'Lancada' or status = 'Aberto') and status_veiculo != 'vendido'
		and REPLACE(A.placa, '-', '') = REPLACE(B.placa, '-', '') group by REPLACE(B.placa, '-', '')`;
        console.log(select_query);
        connection.query(select_query,function(err, recordset){
            if (err) {
                console.log(err);
            }
            else{
                console.log(recordset);
                res.send(recordset);
            }
        })
    });

	app.post('/reabreProjeto', isLoggedIn, function(req, res) {

		url_cc = req.param('cc');

	    var insertQuery = "UPDATE my_schema.projetos SET status='aberto' WHERE cc = ?";
        connection.query(insertQuery,[url_cc], function(err, rows){
            if (err) {
                //return done(err
				req.flash('error', err);
			}
			else 
			{
				console.log('Centro de custo '+url_cc+' reaberto com sucesso');
            	req.flash('editar_projeto_sucess', 'Projeto reaberto com sucesso');
            	res.redirect('/editar_projeto');           
            }
            
        });

	});

	// =====================================
	// Altera registro de deslocamento =====
	// =====================================

	app.post('/deleta_deslocamento', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.deslocamentos set status='Apagada' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_deslocamentos_sucess', 'Deslocamento apagado com sucesso');
                	res.redirect('/ver_deslocamento');           
                }
                
            });

	});

	app.post('/lancar_abastecimento', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.abastecimento set status='Lancada' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_abastecimentos_sucess', 'Abastecimento lançado com sucesso');
                	res.redirect('/ver_abastecimentos');           
                }
                
            });

	});	

	app.post('/get_img_url_abastecimentos', isLoggedIn, function(req, res) {

		url_id = req.param('id');
		console.log("ID = " + url_id);

        var insertQuery = "select caminho_imagem from my_schema.abastecimento WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, recordset){
                if (err) {
                    //return done(err
                    console.log("NÃO deu certo");
					req.flash('error', err);
				}
				else 
				{
                	res.send(recordset);   
                	console.log("deu certo");       
                }
                
            });

	});	

	app.post('/erro_abastecimento', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.abastecimento set status='Erro detectado' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_abastecimentos_sucess', 'Erro reportado com sucesso');
                	res.redirect('/ver_abastecimentos');           
                }
                
            });

	});

	app.post('/lancar_deslocamento', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.deslocamentos set status='Lancada' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_deslocamentos_sucess', 'Deslocamento lançado com sucesso');
                	res.redirect('/ver_deslocamento');           
                }
                
            });

	});

	app.post('/lancar_nota', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.notinhas set status='Lancada' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_deslocamentos_sucess', 'Deslocamento lançado com sucesso');
                	res.redirect('/ver_custos_todos');           
                }
                
            });

	});

	app.post('/get_img_url', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "select caminho_imagem from my_schema.notinhas WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, recordset){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	res.send(recordset);          
                }
                
            });

	});

	app.post('/get_img_url_deslocamentos', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "select caminho_imagem,caminho_imagem2 from my_schema.deslocamentos WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, recordset){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	res.send(recordset);          
                }
                
            });

	});

	app.post('/erro_nota', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.notinhas set status='Erro detectado' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_deslocamentos_sucess', 'Erro reportado com sucesso');
                	res.redirect('/ver_custos_todos');           
                }
                
            });

	});

	app.post('/erro_deslocamento', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.deslocamentos set status='Erro detectado' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_deslocamentos_sucess', 'Erro reportado com sucesso');
                	res.redirect('/ver_custos_todos');           
                }
                
            });

	});


	app.post('/update_deslocamento', isLoggedIn, function(req, res) {

		var id = req.body.id;
		var data = req.body.data;
		var nome = req.body.nome;
		var km = req.body.km;
		var cc = req.body.cc;
		var cnpj = req.body.cnpj;
		var placa = req.body.placa;
		var descricao = req.body.descricao;
		var tipo = req.body.tipo;
		var telefone = req.body.telefone;
		var status = req.body.status;


        var insertQuery = "UPDATE `my_schema`.`deslocamentos` SET `data` = ?, `km` = ?, `cc` = ?, `cnpj` = ?, `placa` = ?, `descricao` = ?, `tipo` = ?, `numero_telefone` = ?, `status` = ? WHERE `id` = ?";
            connection.query(insertQuery,[ data, km, cc, cnpj, placa, descricao, tipo, telefone, status, id], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('ver_deslocamentos_sucess', 'Registro atualizado com sucesso');
                	res.redirect('/ver_deslocamento');
                }

                
            });

	});

	app.get('/registro_deslocamentos', function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `select id, DU.nome, inicial,final,final-inicial as deslocamento, cnpj, cc, CAST(str_to_date(data,'%d/%m/%Y') AS char) as data, placa, descricao, tipo, D.numero_telefone, status from deslocamentos D left join dados_usuario DU on D.chat_id=DU.id_telegram WHERE STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE(?,'%d/%m/%Y') AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE(?,'%d/%m/%Y')`;
            connection.query(insertQuery, [url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `select id, DU.nome, inicial,final, cnpj, cc, CAST(str_to_date(data,'%d/%m/%Y') AS char) as data, placa, descricao, tipo, D.numero_telefone, status from deslocamentos D left join dados_usuario DU on D.chat_id=DU.id_telegram`;
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }

	});

	// Mostra veículos cadastrados
	app.get('/registro_veiculos', function(req, res) {

    	var insertQuery = `select * from veiculos where status_veiculo != 'vendido'`;
        connection.query(insertQuery, function(err, recordset){
	        if (err) console.log(err)

	        // send records as a response
	        res.send(recordset);

        });
	});

	// Mostra responsáveis pelos veículos, e dias e km faltantes para avisos
	app.post('/get_set_veiculos', function(req, res) {
		
    	var insertQuery = `select * from set_veiculos`;
        connection.query(insertQuery, function(err, recordset){
	        if (err) console.log(err)

	        // send records as a response
	        res.send(recordset);
        });
	});

	// Lista opções de e-mails para escolher responsáveis por veículos
	app.post('/get_emails', function(req, res) {
		
    	var insertQuery = `select email from dados_usuario`;
        connection.query(insertQuery, function(err, recordset){
	        if (err) console.log(err)

	        // send records as a response
	        res.send(recordset);
        });
	});	

	// Adiciona novo veículo
	app.post('/newVeiculo', isLoggedIn, function(req, res) {

		function formatDate(input){
			console.log('\n\ninput = '+input+'\n\n')
			input = (input.replace('-','/')).replace('-','/');
			console.log('\n\nreplace = '+input+'\n\n')
			var datePart = input.match(/\d+/g);
			year = datePart[0].substring(2);
			month = datePart[1];
			day = datePart[2];

			return day+'/'+month+'/'+year;
		}

		var marca = req.body.marca;
		var modelo = req.body.modelo;
		var placa = req.body.placa;
		var oleokm = req.body.oleokm;
		var oleodata = req.body.oleodata;
		var lic = req.body.lic;
		var seg = req.body.seg;
		var revkm = req.body.revkm;
		var dataRev = req.body.dataRev;
		var check = req.body.check;

		oleodata = formatDate(oleodata);
		lic = formatDate(lic);
		seg = formatDate(seg);
		dataRev = formatDate(dataRev);
		check = formatDate(check);

		console.log('\n\n'+oleodata+'\n\n')
		console.log('\n\n'+lic+'\n\n')
		console.log('\n\n'+seg+'\n\n')
		console.log('\n\n'+dataRev+'\n\n')

        var insertQuery = "INSERT INTO veiculos values (id,?,?,?,?,?,?,?,?,?,?,?)";
       
        connection.query(insertQuery,[marca,modelo,placa,oleokm,oleodata,lic,seg,revkm,dataRev,check,'ativo'], function(err, rows){
            if (err) {
                //return done(err
				req.flash('error', err);
			}
			else 
			{
            	// all is well, return successful user
            	req.flash('ponto_sucess', 'Veículo cadastrado com sucesso');
            	res.redirect('/cadastro_veiculo');
            }

            
        });

	});

	// Altera responsáveis, dias e km faltantes para dar avisos
	app.post('/setVeiculo', isLoggedIn, function(req, res) {
		var email = req.body.responsavel1;
		var email2 = req.body.responsavel2;
		var km_limite = req.body.km_limite;
		var dias_limite = req.body.dias_limite;
        var insertQuery = "update set_veiculos set email = ?, email2 = ?, km_limite = ?, dias_limite = ?";
       
        connection.query(insertQuery,[email,email2,km_limite,dias_limite], function(err, rows){
            if (err) {
                //return done(err
				req.flash('error', err);
			}
			else 
			{
            	// all is well, return successful user
            	req.flash('ponto_sucess', 'Configurações alteradas com sucesso');
            	res.redirect('/cadastro_veiculo');         	
            }  
        });
	});

	app.get('/ver_deslocamento', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "ver_deslocamento"], function(err, recordset){

	        if (err) {
	        	res.redirect('/acesso_negado');
	        	}
	        	else {
	        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('ver_deslocamentos.ejs', { message: req.flash('ver_deslocamentos_sucess')});
					        		}
	        		else
	        		{
		        		res.redirect('/acesso_negado');
		        	}
	        	} 
    	});
	});

	// =====================================
	// Compras      ==================
	// =====================================
	app.get('/cadastro_compras', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "cadastro_compras"], function(err, recordset){

        	if (err) {
        		res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
				// render the page and pass in any flash data if it exists
				res.render('cadastro_compras.ejs', { message: req.flash('cadastro_compras_sucess')});
				        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    	});
	});

	// Mostra compras na página compras
	app.get('/registro_compras', function(req, res) {

    	var insertQuery = `select id, area,observacoes_comprador, observacoes_fornecedores, nome as solicitante, area, material, especificacoes, quantidade, marca, cc, status, justificativa, fornecedores, date_format(prazo_entrega,'%Y-%m-%d') as prazo_entrega, unidades, aprovacao_com, aprovacao_tec from comprasteste C left join dados_usuario D on D.username=C.solicitante`;
        connection.query(insertQuery, function(err, recordset){
	        if (err) console.log(err)
	        // send records as a response
	    	console.log("prazo_entrega = "+recordset[0]["prazo_entrega"]);
	        res.send(recordset);
        });
	});

	// Aprovação da compra
	app.post('/confirmar_compra', isLoggedIn, function(req, res) {

		url_id = req.param('id');
		var valor_compra = req.param('valor_compra');
		var valor_restante;
		var cc;
		var recurso_material;
		var limite_aprovacao;
		var area_projetos;
		var area_compras;
		var indice;
		var status;
		var aprovacao_com;

		console.log("id = " + url_id);

        var insertQuery = `select A.cc, A.area as area_compras, B.recurso_material, B.limite_aprovacao, B.recurso_restante,
        B.area as area_projetos from my_schema.comprasteste A, my_schema.projetos B 
        WHERE A.id = ? and B.cc = A.cc`;
        connection.query(insertQuery,[url_id], function(err, rows){
            if (err) {
                //return done(err
				req.flash('error', err);
			}
			else 
			{
				recurso_material = rows[0]["recurso_material"];
				recurso_material = JSON.parse(recurso_material);
				limite_aprovacao = rows[0]["limite_aprovacao"];
				limite_aprovacao = JSON.parse(limite_aprovacao);
				valor_restante = rows[0]["recurso_restante"];
				valor_restante = JSON.parse(valor_restante);
				cc = rows[0]["cc"];
				area_compras = rows[0]["area_compras"];
				area_projetos = rows[0]["area_projetos"];
				area_projetos = JSON.parse(area_projetos);

				for(var i = 0; i < area_projetos.length; i++)
				{
					if(area_projetos[i] == area_compras)
					{
						indice = i;
					}
				}
				console.log("area_projetos = "+area_projetos[indice]);
				console.log("indice"+indice);
				console.log("cc"+cc);
				console.log("valor_compra = "+valor_compra);
				console.log("\nvalor_restante = "+valor_restante);
				console.log("\nlimite_aprovacao[indice] = "+ limite_aprovacao[indice]);

				limite_aprovacao[indice] = parseFloat(limite_aprovacao[indice]);

				if(valor_compra > limite_aprovacao[indice])
				{
					status = 'Compra excedeu o limite';
					aprovacao_com = 'Não aprovado';
					console.log("\nvalor maior que o limite");
				}
				else
				{
					valor_restante[indice] = parseFloat(valor_restante[indice]);
					console.log("valor_restante = "+valor_restante[indice]);
					if(valor_restante[indice] < valor_compra)
					{
						status = 'Compra excedeu o recurso disponível';
						aprovacao_com = 'Não aprovado';
						console.log("\nvalor excedeu o recurso disponível");
					}
					else
					{
						valor_restante[indice] = parseFloat(valor_restante[indice]);

						if(valor_restante[indice] == 0)
						{
							status = 'Não há recurso disponível para essa compra';
							aprovacao_com = 'Não aprovado';
							console.log("\nNão há recurso disponível para essa compra");							
						}
						else
						{
							valor_restante[indice] = valor_restante[indice] - valor_compra;
							console.log("\nvalor_restante = "+valor_restante);
							console.log("valor_restante = " + valor_restante[indice]);
							valor_restante = JSON.stringify(valor_restante);
							console.log("\nvalor_restante = "+valor_restante);
							status = 'Compra aprovada';
							aprovacao_com = 'Aprovado';					
						}
				    	var insertQuery = "update my_schema.projetos set recurso_restante = ? WHERE cc = ?";
				        connection.query(insertQuery,[valor_restante,cc], function(err, rows){
				            if (err) {
				                //return done(err
								req.flash('error', err);
							}
							else 
							{
				            	req.flash('cadastro_compras', 'Compra confirmada');         
				            }
				            
				    	});
					}
				}
            }
            
	    	var insertQuery = `update my_schema.comprasteste set status = ?, 
	    	aprovacao_com = ? WHERE id = ?`;
	        connection.query(insertQuery,[status,aprovacao_com,url_id], function(err, rows){
	            if (err) {
	                //return done(err
	                console.log("não atualizou o banco");
	                console.log(err);
					req.flash('error', err);
				}
				else 
				{
	            	//res.redirect('/cadastro_compras');           
	            }
	            
	    	});
        });

    	res.redirect('/cadastro_compras');
	});	

	// Atualizar status da compra
	app.post('/atualizar_status_compra', isLoggedIn, function(req, res) {

		url_id = req.param('id');
		var status = req.param('status');
            
    	var insertQuery = `update my_schema.comprasteste set status = ? WHERE id = ?`;
        connection.query(insertQuery,[status,url_id], function(err, rows){
            if (err) {
                //return done(err
                console.log("não atualizou o banco");
                console.log(err);
				req.flash('error', err);
			}
			else 
			{
            	//res.redirect('/cadastro_compras');           
            }
            
    	});

    	res.redirect('/cadastro_compras');
	});

	// Função utilizada em compras
	function fieldCount(campo,req){
			var count=0;
			var lista=[];
			var body=req.body;
			for (i of Object.keys(body)){
				str= campo;
				var regex = new RegExp(str,'g');
				if(i.match(regex)){
					count++;
					lista.push(body[i]);
				}
			}
			return lista
	}

	// Adiciona nova compra
	app.post('/newCompras', isLoggedIn, function(req, res) {
		var solicitante = req.user.username;
		var material = JSON.stringify(fieldCount('material',req));
		var especificacoes = JSON.stringify(fieldCount('especificacoes',req));
		var quantidade = JSON.stringify(fieldCount('quantidade',req));
		var unidades = JSON.stringify(fieldCount('unidades',req));
		var marca = JSON.stringify(fieldCount('marca',req));
		var cc = req.body.cc;
		var justificativa = req.body.justificativa;
		var fornecedores = JSON.stringify(fieldCount('fornecedor',req));
		var area = req.body.area;
		var observacoes_fornecedores = req.body.observacoes_fornec;
		var observacoes_comprador = req.body.observacoes_comprador;
		var prazo_entrega = req.body.prazo_entrega;
		var email = req.param('email');
		console.log('email = '+email);
		if ((fornecedores).length>0) {
			var status = 'Aguardando Orçamentos';
		}
		else{
			var status = 'Fornecedor pendente';
		}
		console.log("unidades = "+unidades);
        var insertQuery = "INSERT INTO comprasteste (id,solicitante,material,especificacoes,quantidade,marca,cc,status,justificativa,fornecedores,observacoes_comprador,observacoes_fornecedores,area,prazo_entrega,unidades,aprovacao_com,aprovacao_tec) values (id,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        connection.query(insertQuery,[solicitante,material,especificacoes,quantidade,marca,cc,status,justificativa,fornecedores,observacoes_comprador,observacoes_fornecedores,area,prazo_entrega, unidades,'Pendente','Pendente'], function(err, rows){
            if (err) {
                //return done(err
                console.log(err);
				req.flash('error', err);
			}
			else 
			{
				console.log('compra CADASTRADA\n\n\n\n\n\n\n');
				var dados = [];

				quantidade = JSON.parse(quantidade);
				unidades = JSON.parse(unidades);
				material = JSON.parse(material);
				especificacoes = JSON.parse(especificacoes);
				marca = JSON.parse(marca);

			    for (i in material)
			    {
			        dados.push([quantidade[i],unidades[i],material[i],especificacoes[i],marca[i]]);
			    }

				var html = `<html>
				<head>
				<style>
				.borda {
				  border: 1px solid black;
				  border-collapse: collapse;
				}
				</style>
				</head>
				<body>
				<p>Prezado(a),<br><br>
				Gentileza orçar para a LMLogix os itens abaixo:<br></p>
				<table class="borda">
				  <tr class="borda">
				    <th class="borda">QTD.</th>
				    <th class="borda">Uni</th>
				    <th class="borda">Descrição</th>
				    <th class="borda">Especificação</th>
				    <th class="borda">Marca</th>
				  </tr>`;

				console.log("dados = "+dados);
				  
				for (i of dados)
				{
					html = html+`<tr>
					    <td class="borda" style="width: 50px; text-align: center;">`+i[0]+`</td>
					    <td class="borda" style="width: 50px; text-align: center;">`+i[1]+`</td>
					    <td class="borda" style="width: 150; text-align: center;">`+i[2]+`</td>
					    <td class="borda" style="width: 300px; text-align: center;">`+i[3]+`</td>
					    <td class="borda" style="width: 300px; text-align: center;">`+i[4]+`</td>
					  </tr>`;
				}
				html = html + `</table><br>`+observacoes_fornecedores+`<br><br>
				<p>Contamos com sua colaboração e caso haja dúvidas, favor entrar em contato.<br>
				Obrigado.</p><br></body>`

				var fs = require('fs'); //Filesystem
				var content = fs.readFileSync("teste.html","utf-8");

				html = html + content;

				// Configurações de e-mail
				const transporter = nodemailer.createTransport({
				  host: "mail.lmlogix.com.br",
				  port: 587,
				  secure: false, // true for 465, false for other ports
				  auth: {
				  	user: "noreply@lmlogix.com.br",
				  	pass: "logix16"
				  },
				  tls: { rejectUnauthorized: false }
				});
				var data = new Date();
				var corpo = "<h1>teste de envio de email</h1>";
				if(email == 1)
				{
					var email_input = 'vitalli.centeno@lmlogix.com.br';
				}
				else
				{
					var email_input = 'vitalli.centeno@lmlogix.com.br';
				}
				const mailOptions = {
				  from: 'noreply@lmlogix.com.br',
				  to: email_input,
				  subject: 'E-mail Automático',
				  html: html
				};
				transporter.sendMail(mailOptions, function(error, info){
				  if (error) {
				    console.log(error);
				  } else {
				    console.log('Email enviado');
	            	req.flash('compras', 'solicitação cadastrada com sucesso');
	            	res.redirect('/cadastro_compras');
				    // render the page and pass in any flash data if it exists
				  }
				});
            	req.flash('compras', 'solicitação cadastrada com sucesso');
            	//res.redirect('/cadastro_compras');
            }
            res.redirect('/cadastro_compras');
        });
	});

	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	app.get('/', function(req, res) {
		res.render('login.ejs', { message: req.flash('loginMessage'),message_success: req.flash('password_change') });
	});

	// =====================================
	// LOGIN ===============================
	// =====================================
	// show the login form
	app.get('/login', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('login.ejs', { user : req.user, // get the user out of session and pass to template
								  message: req.flash('loginMessage'), message_success: req.flash('password_change') });
	});

	app.get('/localizar_conta', function(req, res)
	{
		res.redirect('/login');
	});

	// chama página para enviar código por e-mail
	app.post('/localizar_conta', function(req, res) {
		var email_input = req.body.email;
		var select_email = "SELECT * FROM my_schema.dados_usuario WHERE email = ?";

		connection.query(select_email, [email_input], function(err, recordset){
	        if (err) {
	        	res.render('set_password.ejs', { message: 'Algo deu errado, tente novamente mais tarde'});
	    	}
	    	else {
	    		if (recordset.length) {

	    			// selecionar nome do usuário para enviar por e-mail
	    			var select_nome = "SELECT nome FROM my_schema.dados_usuario WHERE email = ?";
				    connection.query(select_nome, [email_input], (err, rows) => {
				    	
				    	// pega o primeiro nome apenas
				    	var nome = rows[0]["nome"].split(' ')[0];

		    			// gera código para enviar por e-mail
		    			var codigo = Math.random().toString(36).slice(-6).toUpperCase();

		    			//exclui registro antigo
		    			var deleta = "DELETE FROM my_schema.codigos_senha WHERE email = ?";
				    	connection.query(deleta, [email_input]);

		    			// inserir código no banco de dados para ser consultado para verificação
		    			var select_query="INSERT INTO my_schema.codigos_senha (codigo, email) VALUES (?,?)";
		    			connection.query(select_query, [bcrypt.hashSync(codigo, null, null),email_input], (err, rows) => { 
		    				if (err) {
		    					res.render('set_password.ejs', { message: 'Algo deu errado, tente novamente mais tarde'});
							}
							else{
								// Configurações de e-mail
								const transporter = nodemailer.createTransport({
							        service: "Outlook365", // Office 365 server
							        auth: {
							            user: 'rafael.mattos@lmlogix.com.br',
							            pass: 'tfqfwhcnvtjzcwbb'
							        },
							    });

								var data = new Date();

								const mailOptions = {
								  from: 'noreply@lmlogix.com.br',
								  to: email_input,
								  subject: 'E-mail Automático para Redefinição de Senha',
								  html: "<h1>Olá, " + nome + "!</h1> Recebemos uma solicitação para redefinir a senha de sua conta no SARR. Aqui está seu código para criar uma nova senha:<br><br><h2 align='center'>" + codigo + "</h2><br>Quando isto ocorreu: <br>" + data + "."
								};

								transporter.sendMail(mailOptions, function(error, info){
								  if (error) {
								    console.log(error);
								    res.render('set_password.ejs', { message: 'Algo deu errado, tente novamente mais tarde'});
								  } else {
								    console.log('Email enviado: ' + info.response);
								    // render the page and pass in any flash data if it exists
									res.render('codigo_reseta_senha.ejs', { message: req.flash('cod_fail'),email: email_input});
								  }
								});
			                }

		    			});

			    	});
	    		}
	    		else
	    		{
	        		res.render('set_password.ejs', { message: 'Nenhum usuário encontrado'});
	        	}
	    	} 
        });
	});

	// caso "check_code" seja chamado sem passar por "set_password"
	app.get('/check_code', function(req, res)
	{
		res.redirect('/login');
	});

	// chama página para inserir código
	app.post('/check_code', function(req, res) {
		var code = req.body.code;
		var email = req.body.email;
		var select_code = "SELECT * FROM my_schema.codigos_senha WHERE email = ?";

		connection.query(select_code, [email], function(err, recordset){
	        if (err) {
	        	console.log(err);
	        	res.render('codigo_reseta_senha.ejs', { message: req.flash('message','Algo deu errado, tente novamente mais tarde'), email:email});
	    	}
	    	else {
	    		if (recordset.length) {
	    				var now= new Date();
	    				if (bcrypt.compareSync(code.toUpperCase(), recordset[0]["codigo"])) {
	    					if ((now.getTime() - recordset[0]["geradoEm"].getTime())>2*60*1000) {
	    						req.flash('expired_code','O código expirou, tente novamente');
	    						res.redirect('/set_password');

	    					}
	    					else{
	    						res.render('nova_senha.ejs', { message: req.flash('cod_fail'),email: recordset[0]["email"]});
	    					}
	    				}
	    				else{
	    					req.flash('cod_fail', 'Código inválido');
	    					res.render('codigo_reseta_senha.ejs', { message: req.flash('cod_fail'), email:email});
	    				}
				    	
				}
	    		else {
	    			req.flash('cod_fail', 'Algo deu errado');
	        		res.render('codigo_reseta_senha.ejs', { message: req.flash('message'), email:email});
	        	}
	    	}
        });
	});

	app.post('/change_password', function(req, res)
	{
		var senha = req.body.senha;
		var confirmaSenha = req.body.confirmaSenha;
		var email = req.body.email;

		console.log('este é o email: '+email);

		if(senha == confirmaSenha)
		{
			var select_senha = "SELECT username from my_schema.dados_usuario where email = ?";
			connection.query(select_senha, [email], function(err, recordset)
			{
				
		        if (err) {
	        		res.render('nova_senha.ejs', { message: 'ALgo deu errado, tente novamente mais tarde',email: email});
	    		}
	    		else
	    		{
	    			if(recordset.length)
	    			{
	    				console.log('recordset');
	    				var update_senha = "UPDATE my_schema.users set password = ? where username = ?";
	    				connection.query(update_senha, [bcrypt.hashSync(senha, null, null), recordset[0]["username"]], function(err, recordset)
	    				{
	    					if(err)
	    					{
	    						req.flash('cod_fail','Algo deu errado, tente novamente mais tarde');
	    						res.render('nova_senha.ejs', { message: req.flash('cod_fail'),email: email});
	    					}
	    					else
	    					{
	    						req.flash('password_change','Senha alterada com sucesso');
	    						res.redirect('/login');
	    					}
	    				});
	    			}
	    			else
	    			{
	    				req.flash('cod_fail','Algo deu errado, tente novamente');
	    				res.render('nova_senha.ejs', { message: req.flash('cod_fail'),email: email});
	    			}
	    		}

			});
		}
		else
		{
			req.flash('cod_fail','Senhas não conferem');
			res.render('nova_senha.ejs', { message: req.flash('cod_fail'),email: email});
		}
	});





	// show the login form
	app.get('/signup_2', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('signup_2.ejs', { message: req.flash('loginMessage') });
	});

	app.get('/cadastro_veiculo', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "cadastro_veiculo"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
				// render the page and pass in any flash data if it exists
				res.render('cadastro_veiculo.ejs', { message: req.flash('cadastro_veiculo_sucess')});
				        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	app.post('/permissao_paginas', function(req, res) {
    	var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ?";
    	connection.query(insertQuery, [req.user.id], function(err, recordset){
	        if (err) {
	        	res.send(recordset);
        	}
        	else {
        		if (recordset.length) {
        			res.send(recordset);
				}
        		else
        		{
	        		res.send(recordset);
	        	}
        	}    		
    	});
    });  


	// show the login form
	app.get('/acesso_negado', isLoggedIn, function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('acesso_negado.ejs', { message: req.flash('ponto_sucess') });
	});

	// show the login form
	app.get('/ponto', isLoggedIn, function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('ponto_2.ejs', { message: req.flash('ponto_sucess') });
	});

	// show the login form
	app.get('/cadastro_projeto', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "cadastro_projeto"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('cadastro_projeto_2.ejs', { message: req.flash('cadastro_projeto_sucess'), message_error: req.flash('cadastro_projeto_error') });
					        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	// show the login form
	app.get('/editar_projeto', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "editar_projeto"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('editar_projeto.ejs', { message: req.flash('editar_projeto_sucess'), message_error: req.flash('editar_projeto_error') });
				}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	// show the login form
	app.get('/ver_ponto', isLoggedIn, function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('ver_ponto.ejs', { message: req.flash('ver_ponto_sucess'), message_danger: req.flash('ver_ponto_danger') });
	});

	// show the login form
	app.get('/exporta_planilha', isLoggedIn, function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('exporta_planilha.ejs', { message: req.flash('ver_ponto_sucess'), message_danger: req.flash('ver_ponto_danger') });
	});

	// show the login form
	app.get('/ver_atividades_todos', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "ver_atividades_todos"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('ver_ponto_todos.ejs', { message: req.flash('ver_ponto_todos_sucess') });
				        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	// show the login form
	app.get('/aprovacao', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "aprovacao"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('aprovacao.ejs', { message: req.flash('aprovacao_sucess') });
				        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	// show the login form
	app.get('/ver_custos', isLoggedIn, function(req, res) {

        res.render('ver_custos.ejs', { message: req.flash('ver_custo_sucess')});
	});

	// show the login form
	app.get('/ver_custos_temp', function(req, res) {

        res.render('ver_custos_todos_temp.ejs', { message: req.flash('ver_custo_sucess')});
	});

	// show the login form
	app.get('/ver_desl_prov', function(req, res) {

        res.render('ver_desl_todos_prov.ejs', { message: req.flash('ver_custo_sucess')});
	});


	// show the login form
	app.get('/ver_custos_todos', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "ver_custos_todos"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
				// render the page and pass in any flash data if it exists
				res.render('ver_custos_todos.ejs', { message: req.flash('ver_custo_sucess')});
				        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	// show the login form
	app.get('/editar_permissoes', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "editar_permissoes"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('editar_permissoes.ejs', { message: req.flash('editar_permissoes_sucess')});
					        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	// show the login form
	app.get('/editar_acesso_paginas', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "editar_acesso_paginas"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('editar_acesso_paginas.ejs', { message: req.flash('editar_permissoes_sucess')});
				}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	}
	});
    });

	// process the login form
	app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/ponto', // redirect to the secure profile section
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
		}),
        function(req, res) {
            console.log("hello");

            if (req.body.remember) {
              req.session.cookie.maxAge = 1000 * 60 * 3;
            } else {
              req.session.cookie.expires = false;
            }
        res.redirect('/login');
    });

	// =====================================
	// SIGNUP ==============================
	// =====================================
	// show the signup form
	app.get('/signup', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('signup.ejs', { message: req.flash('signupMessage') });
	});

	// =====================================
	// SETPASSWORD ==============================
	// =====================================
	// show the signup form	
	app.get('/set_password', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('set_password.ejs', { message: req.flash('expired_code') });
	});

	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/tempo_real', isLoggedIn, function(req, res) {
		res.render('tempo_real.ejs', {
			user : req.user // get the user out of session and pass to template
		});
	});

	// process the signup form
	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/ponto', // redirect to the secure profile section
		failureRedirect : '/signup', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/profile', isLoggedIn, function(req, res) {
		res.render('profile.ejs', {
			user : req.user // get the user out of session and pass to template
		});
	});

	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/login');
	});

	function fieldCount(campo,req){
			var count=0;
			var lista=[];
			var body=req.body;
			for (i of Object.keys(body)){
				str= campo;
				var regex = new RegExp(str,'g');
				if(i.match(regex)){
					count++;
					lista.push(body[i]);
				}
			}
			return lista
	}

	// =====================================
	// Cadastra Projeto ====================
	// =====================================
	app.post('/cadastro_projeto', isLoggedIn, function(req, res) {
		var solicitante = req.user.username;
		var nome_lm = req.body.nome_lm;
		var nome_cliente = req.body.nome_cliente;
		var email = req.body.email_cliente;
		var telefone_cliente = req.body.telefone_cliente;
		var cnpj = req.body.cnpj;
		var nome_empresa = req.body.nome_empresa;
		var cep = req.body.cep;
		var cidade = req.body.cidade;
		var estado = req.body.estado;
		var pais = req.body.pais;
		var tipo_projeto = req.body.tipo_projeto;
		var olm = req.body.olm;
		var cc = req.body.cc;
		var nome_obra = req.body.nome_obra;

		var area_projeto = JSON.stringify(fieldCount('area_projeto',req));
		var recurso_material = JSON.stringify(fieldCount('recurso_material',req));
		var limite_aprovacao = JSON.stringify(fieldCount('limite_aprovacao',req));
		//var recurso_restante = JSON.stringify(fieldCount('recurso_restante',req));

		var tecnico_em_automacao = req.body.tecnico_em_automacao;
		var projetista = req.body.projetista;
		var programador = req.body.programador;
		var coordenador_de_projetos = req.body.coordenador_de_projetos;
		var coordenador_de_software = req.body.coordenador_de_software;
		var recursos_humanos = req.body.recursos_humanos;
		var compras = req.body.compras;
		var gerente_tecnico = req.body.gerente_tecnico;
		var tecnico_de_seguranca = req.body.tecnico_de_seguranca;
		var marketing = req.body.marketing;
		var auxiliar_administrativo = req.body.auxiliar_administrativo;
		var coordenador_tecnico = req.body.coordenador_tecnico;

		var data_pedido = req.body.data;
		var valor_pedido = req.body.valor_pedido;


        var insertQuery = `INSERT INTO projetos ( nome_lm, nome_cliente, email, telefone_cliente, cnpj, 
        	nome_empresa, cep, cidade, estado, pais, tipo_projeto, olm, cc, nome_obra, 
			area, recurso_material, limite_aprovacao, recurso_restante, auxiliar_administrativo, 
			marketing, tecnico_de_seguranca, gerente_tecnico, compras, recursos_humanos, coordenador_de_software,
			coordenador_de_projetos, programador, projetista, tecnico_em_automacao, coordenador_tecnico,
			data_pedido,valor_pedido, 
			status) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, "aberto")`;
            connection.query(insertQuery,[ nome_lm, nome_cliente, email, telefone_cliente, cnpj, nome_empresa, 
            	cep, cidade, estado, pais, tipo_projeto, olm, cc, nome_obra, area_projeto, recurso_material, 
            	limite_aprovacao, recurso_material, auxiliar_administrativo, marketing, tecnico_de_seguranca, 
            	gerente_tecnico, compras, recursos_humanos, coordenador_de_software, coordenador_de_projetos, 
            	programador, projetista, tecnico_em_automacao, coordenador_tecnico,
            	data_pedido, valor_pedido], function(err, rows){
            	if (err) {
                    //return done(err

                    if (err.code == "ER_DUP_ENTRY") {
                    	req.flash('cadastro_projeto_error', "Esse número de CC já existe, favor utilizar outro número");
                    	res.redirect('/cadastro_projeto');
                    }
                    else {
                    	req.flash('cadastro_projeto_error', err.sqlMessage);
						res.redirect('/cadastro_projeto');
                    }

                    console.log(err);
					
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('cadastro_projeto_sucess', 'Projeto criado com sucesso');
                	res.redirect('/cadastro_projeto');
                }
	}); });


	// =====================================
	// Cadastra Projeto ====================
	// =====================================
	app.post('/editar_projeto', function(req, res) {

		var nome_lm = req.body.nome_lm;
		var nome_cliente = req.body.nome_cliente;
		var email = req.body.email_cliente;
		var telefone_cliente = req.body.telefone_cliente;
		var cnpj = req.body.cnpj;
		var nome_empresa = req.body.nome_empresa;
		var cep = req.body.cep;
		var cidade = req.body.cidade;
		var estado = req.body.estado;
		var pais = req.body.pais;
		var tipo_projeto = req.body.tipo_projeto;
		var olm = req.body.olm;
		var cc = req.body.cc;
		var nome_obra = req.body.nome_obra;

		var area_projeto = JSON.stringify(fieldCount('area_projeto',req));
		var recurso_material = JSON.stringify(fieldCount('recurso_material',req));
		var limite_aprovacao = JSON.stringify(fieldCount('limite_aprovacao',req));
		var recurso_restante = JSON.stringify(fieldCount('recurso_material',req));

		var tecnico_em_automacao = req.body.tecnico_em_automacao;
		var projetista = req.body.projetista;
		var programador = req.body.programador;
		var coordenador_de_projetos = req.body.coordenador_de_projetos;
		var coordenador_de_software = req.body.coordenador_de_software;
		var recursos_humanos = req.body.recursos_humanos;
		var compras = req.body.compras;
		var gerente_tecnico = req.body.gerente_tecnico;
		var tecnico_de_seguranca = req.body.tecnico_de_seguranca;
		var marketing = req.body.marketing;
		var auxiliar_administrativo = req.body.auxiliar_administrativo;
		var coordenador_tecnico = req.body.coordenador_tecnico;

		var data_pedido = req.body.data;
		var valor_pedido = req.body.valor_pedido;

		var cc_old = req.body.cc;


        var insertQuery = `UPDATE projetos SET nome_lm=?, nome_cliente=?, email=?, telefone_cliente=?, 
        				   cnpj=?, nome_empresa=?, cep=?, cidade=?, estado=?, pais=?, tipo_projeto=?, 
        				   olm=?, cc=?, nome_obra=?, area=?, recurso_material=?, limite_aprovacao=?,
        				   recurso_restante=?, auxiliar_administrativo=?, marketing=?, 
        				   tecnico_de_seguranca=?, gerente_tecnico=?, compras=?, recursos_humanos=?,
        				   coordenador_de_software=?, coordenador_de_projetos=?, programador=?,
        				   projetista=?, tecnico_em_automacao=?, coordenador_tecnico=?,
        				   data_pedido=?,valor_pedido=? WHERE cc=?`;

            connection.query(insertQuery,[ nome_lm, nome_cliente, email, telefone_cliente, cnpj, nome_empresa, 
            								cep, cidade, estado, pais, tipo_projeto, olm, cc, nome_obra,
            								area_projeto, recurso_material, limite_aprovacao, recurso_restante,
            								auxiliar_administrativo, marketing, tecnico_de_seguranca,
            								gerente_tecnico, compras, recursos_humanos, coordenador_de_software,
            								coordenador_de_projetos, programador, projetista, 
            								tecnico_em_automacao, coordenador_tecnico,
            								data_pedido,valor_pedido,cc_old], 
            								function(err, rows){
            	if (err) {
                    //return done(err

                    if (err.code == "ER_DUP_ENTRY") {
                    	req.flash('cadastro_projeto_error', "Esse número de CC já existe, favor utilizar outro número");
                    	res.redirect('/cadastro_projeto');
                    }
                    else {
                    	req.flash('cadastro_projeto_error', err.sqlMessage);
						res.redirect('/cadastro_projeto');
                    }

                    console.log(err);
					
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('cadastro_projeto_sucess', 'Projeto alterado com sucesso');
                	res.redirect('/cadastro_projeto');
                }
	}); });

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/ponto', isLoggedIn, function(req, res) {

		var data = req.body.data;
		var inicio = req.body.inicio;
		var fim = req.body.fim;
		var cc = req.body.cc;
		var cargo = req.body.cargo;
		var modalidade = req.body.modalidade;
		var local = req.body.local;
		var atividade = req.body.atividade;
		var etapa = req.body.etapa;

		var colegas = req.body.colegas;

		if (typeof(colegas) != "undefined") {

			if (Array.isArray(colegas)) {
				colegas.forEach(myFunction);

				function myFunction(value, index, array) {
				  var insertQuery = "INSERT INTO marcacao (quem_marcou, id, data, inicio, fim, cc, modalidade, local, etapa) values (?,?,?,?,?,?,?,?,?)";
	              connection.query(insertQuery,[req.user.id, value, data, inicio, fim, cc, modalidade, local, etapa]);
				}
			}
			else {
			  var insertQuery = "INSERT INTO marcacao (quem_marcou, id, data, inicio, fim, cc, modalidade, local, etapa) values (?,?,?,?,?,?,?,?,?)";
              connection.query(insertQuery,[req.user.id, colegas, data, inicio, fim, cc, modalidade, local, etapa]);
		}

		}

        var insertQuery = "INSERT INTO ponto ( usuario, data, inicio, fim, cc, cargo, modalidade, local, atividade, aprovacao, etapa ) values (?,?,?,?,?,?,?,?,?,\"Não aprovado\", ?)";
            connection.query(insertQuery,[req.user.id, data, inicio, fim, cc, cargo, modalidade, local, atividade, etapa ], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('ponto_sucess', 'Atividade registrada com sucesso');
                	res.redirect('/ponto');
                }

                
            });

	});

	// =====================================
	// Deleta atividade ==================
	// =====================================
	app.post('/deleta_atividade', isLoggedIn, function(req, res) {

		url_id = req.param('id');
		origem = req.param('origem');

        var insertQuery = "DELETE FROM my_schema.ponto WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
					if (origem == 1) {
	                	// all is well, return successful user
	                	req.flash('ver_ponto_danger', 'Atividade apagada com sucesso');
	                	res.redirect('/ver_ponto'); 
	                }
	                else if (origem == 2) {
	                	// all is well, return successful user
	                	req.flash('ver_ponto_todos_sucess', 'Atividade apagada com sucesso');
	                	res.redirect('/ver_atividades_todos');  
	                }              
                }
                
            });

	});

	// =====================================
	// Deleta atividade ==================
	// =====================================
	app.post('/deleta_custo', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.notinhas set status='Apagada' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_custo_sucess', 'Atividade apagada com sucesso');
                	res.redirect('/ver_custos');    
                }
                
            });

	});


	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/update_ponto', isLoggedIn, function(req, res) {

		var id = req.body.id;
		var data = req.body.data;
		var inicio = req.body.inicio;
		var fim = req.body.fim;
		var cc = req.body.cc;
		var cargo = req.body.cargo;
		var etapa = req.body.etapa;
		var modalidade = req.body.modalidade;
		var atividade = req.body.atividade;

        var insertQuery = "UPDATE `my_schema`.`ponto` SET `data` = ?, `inicio` = ?, `fim` = ?, `cc` = ?, `cargo` = ?, `modalidade` = ?, `atividade` = ?, `etapa` = ? WHERE `id` = ?";
            connection.query(insertQuery,[ data, inicio, fim, cc, cargo, modalidade, atividade, etapa, id], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('ver_ponto_sucess', 'Atividade atualizada com sucesso');
                	res.redirect('/ver_ponto');
                }

                
            });

	});

	// =====================================
	//    Altera veículo  ==================
	// =====================================
	app.post('/update_Veiculo', isLoggedIn, function(req, res) {

		var id = req.body.id;
		var km_oleo = req.body.km_oleo;
		var data_oleo = req.body.data_oleo;
		var data_lic = req.body.data_lic;
		var data_seg = req.body.data_seg;
		var data_revisao = req.body.data_revisao;
		var km_revisao = req.body.km_revisao;

		console.log(km_revisao);

        var insertQuery = "UPDATE `my_schema`.`veiculos` SET `km_oleo` = ?, `data_oleo` = ?, `data_lic` = ?, `data_seg` = ?, `km_revisao` = ?, data_revisao = ? WHERE `id` = ?";
            connection.query(insertQuery,[km_oleo, data_oleo, data_lic, data_seg,km_revisao, data_revisao, id], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('cadastro_veiculo_sucess', 'Registro atualizado com sucesso');
                	res.redirect('/cadastro_veiculo');
                }

                
            });

	});



	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/update_despesa', isLoggedIn, function(req, res) {

		var id = req.body.id;
		var data = req.body.data;
		var cc = req.body.cc;
		var cartao = req.body.cartao;
		var descricao = req.body.descricao;
		var telefone = req.body.telefone;
		var tipo = req.body.tipo;
		var cnpj = req.body.cnpj;
		var valor = req.body.valor;


        var insertQuery = "UPDATE `my_schema`.`notinhas` SET `data` = ?, `cc` = ?, `cartao` = ?, `descricao` = ?, numero_telefone=?, tipo_gasto=?, cnpj=?, valor=? WHERE `id` = ?";
            connection.query(insertQuery,[ data, cc, cartao, descricao, telefone, tipo, cnpj, valor, id], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('ver_custo_sucess', 'Nota atualizada com sucesso');
                	res.redirect('/ver_custos_todos');
                }
                
            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/add_permission', isLoggedIn, function(req, res) {

		var url_nome = req.param('nome');
		var url_alvo = req.param('alvo');

        var insertQuery = `INSERT INTO \`my_schema\`.\`permissao_usuarios\`
							(\`id_usuario\`,
							\`id_usuario_visto\`)
							VALUES
							(?,
							(SELECT id FROM my_schema.users
							LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.username = my_schema.users.username
							WHERE nome = ?));`;

            connection.query(insertQuery,[ url_nome, url_alvo], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('editar_permissoes_sucess', 'Nota atualizada com sucesso');
                	res.redirect('/editar_permissoes');
                }
                
            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/delete_permission', isLoggedIn, function(req, res) {

		var url_nome = req.param('nome');
		var url_alvo = req.param('alvo');

        var insertQuery = `DELETE FROM \`my_schema\`.\`permissao_usuarios\`
							WHERE (\`id_usuario\` = ? AND 
					        \`id_usuario_visto\` = (SELECT id FROM my_schema.users
							LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.username = my_schema.users.username
							WHERE nome = ?));`;

            connection.query(insertQuery,[ url_nome, url_alvo], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('editar_permissoes_sucess', 'Nota atualizada com sucesso');
                	res.redirect('/editar_permissoes');
                }
                
            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/add_permission_pages', isLoggedIn, function(req, res) {

		var url_nome = req.param('nome');
		var url_alvo = req.param('alvo');

        var insertQuery = `INSERT INTO \`my_schema\`.\`acesso_paginas\`
							(\`id_usuario\`,
							\`nome_pagina\`)
							VALUES
							(?,
							?);`;

            connection.query(insertQuery,[ url_nome, url_alvo], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('editar_permissoes_sucess', 'Nota atualizada com sucesso');
                	res.redirect('/editar_acesso_paginas');
                }
                
            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/aprova_ponto', isLoggedIn, function(req, res) {

		var url_id = req.param('id');
		var url_aprovacao = req.param('aprovacao');

		console.log("--------------------------");
		console.log(url_id);
		console.log(url_aprovacao);

        var insertQuery = `UPDATE \`my_schema\`.\`ponto\`
							SET
							\`aprovacao\` = ?
							WHERE \`id\` = ?;`;

            connection.query(insertQuery,[ url_aprovacao, url_id ], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('aprovacao_sucess', 'Ponto aprovado');
                	res.redirect('/aprovacao');
                }
                
            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/delete_permission_pages', isLoggedIn, function(req, res) {

		var url_nome = req.param('nome');
		var url_alvo = req.param('alvo');

        var insertQuery = `DELETE FROM \`my_schema\`.\`acesso_paginas\`
							WHERE id_usuario = ? AND nome_pagina = ?;`;

            connection.query(insertQuery,[ url_nome, url_alvo], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('editar_permissoes_sucess', 'Nota atualizada com sucesso');
                	res.redirect('/editar_acesso_paginas');
                }
                
            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_atividade', isLoggedIn, function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `select R.id as idRelatorio, U.id, D.id_telegram,colaborador, CAST(str_to_date(Data,'%d/%m/%Y') AS char) as Data, entrada, saida, cc, tipo, cidade,motorista,veiculo,km_Inicial,km_Final, funcao, solicitante, pernoite, relatorio, aprovacao from relatorio R
							left join dados_usuario D on D.nome = R.colaborador
							left join users U on D.username = U.username
        					WHERE U.id=? AND CAST(str_to_date(Data,'%d/%m/%Y') AS char) >= STR_TO_DATE(?,'%d/%m/%Y') AND CAST(str_to_date(Data,'%d/%m/%Y') AS char) <= STR_TO_DATE(?,'%d/%m/%Y') order by R.id desc;`;
            connection.query(insertQuery, [req.user.id,url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `select id, colaborador, CAST(str_to_date(data,'%d/%m/%Y') AS char) as Data, entrada, saida, cc, tipo, cidade, funcao, solicitante, pernoite, relatorio, aprovacao from relatorio order by id desc;`;
            connection.query(insertQuery, [req.user.id], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }

	});


	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_atividade_planilha', isLoggedIn, function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT "" As nome,DATE_FORMAT(\`data\`, "%d/%m/%Y") as \`data\`, inicio, fim, cc, cargo, modalidade, local, atividade,
        					usuario, id, aprovacao, etapa, "" as cidade, "" as motorista, "" as veiculo, "" as kmi, "" as kmf, "" as kma, "" as litros, "" as valor, "" as pernoite, "" as responsavel FROM my_schema.ponto 
        					WHERE usuario = ? AND \`data\` >= STR_TO_DATE(?,'%d/%m/%Y') AND \`data\` <= STR_TO_DATE(?,'%d/%m/%Y')`;
            connection.query(insertQuery, [req.user.id,url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT "" As nome,DATE_FORMAT(\`data\`, "%d/%m/%Y") as \`data\`, inicio, fim, cc, cargo, modalidade, local, atividade,
        						usuario, id, aprovacao, etapa, "" as cidade, "" as motorista, "" as veiculo, "" as kmi, "" as kmf, "" as kma, "" as litros, "" as valor, "" as pernoite, "" as responsavel FROM my_schema.ponto  WHERE usuario = ?`;
            connection.query(insertQuery, [req.user.id], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/get_marcacao', isLoggedIn, function(req, res) {
		var url_data = req.param('data');

        var insertQuery = `SELECT quem_marcou, my_schema.marcacao.id, data, inicio, fim, cc, modalidade, etapa, nome, local FROM my_schema.marcacao
							LEFT JOIN my_schema.users ON my_schema.marcacao.quem_marcou = my_schema.users.id
							LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.username = my_schema.users.username
							WHERE data = ? AND my_schema.marcacao.id = ? ;`;

            connection.query(insertQuery, [url_data, req.user.id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_custos', isLoggedIn, function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `select N.id, N.nome, CAST(str_to_date(data,'%d/%m/%Y') AS char) as data, cc, N.cartao, descricao, cnpj, valor, status, tipo_gasto from my_schema.notinhas N
							left join my_schema.dados_usuario D on D.id_telegram= N.chat_id
							left join my_schema.users U on U.username = D.username
							where U.id=? AND STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE(?,'%d/%m/%Y') AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE(?,'%d/%m/%Y')`;
            connection.query(insertQuery, [req.user.id,url_ini,url_fim],function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `select N.id, N.nome, CAST(str_to_date(data,'%d/%m/%Y') AS char) as data, cc, cartao, descricao, cnpj, valor, status, tipo_gasto from my_schema.notinhas N
							left join my_schema.dados_usuario D on D.id_telegram= N.chat_id
							left join my_schema.users U on U.username = D.username
							where U.id=?`;
            connection.query(insertQuery,[req.user.id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_custos_todos', isLoggedIn, function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT my_schema.notinhas.id, my_schema.dados_usuario.nome, numero_telefone, CAST(str_to_date(data,'%d/%m/%Y') AS char) as data, cc, cartao, descricao FROM my_schema.notinhas
							LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.telefone = my_schema.notinhas.numero_telefone
							LEFT JOIN my_schema.users ON my_schema.users.username = my_schema.dados_usuario.username
							LEFT JOIN my_schema.permissao_usuarios ON my_schema.permissao_usuarios.id_usuario = my_schema.users.id OR my_schema.permissao_usuarios.id_usuario_visto = my_schema.users.id
							WHERE my_schema.permissao_usuarios.id_usuario = ? AND \`data\` >= STR_TO_DATE(?,'%d/%m/%Y') AND \`data\` <= STR_TO_DATE(?,'%d/%m/%Y')`;
            connection.query(insertQuery, [req.user.id,url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT my_schema.notinhas.id, my_schema.dados_usuario.nome, numero_telefone, CAST(str_to_date(data,'%d/%m/%Y') AS char) as data, cc, cartao, descricao FROM my_schema.notinhas
							LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.telefone = my_schema.notinhas.numero_telefone
							LEFT JOIN my_schema.users ON my_schema.users.username = my_schema.dados_usuario.username
							LEFT JOIN my_schema.permissao_usuarios ON my_schema.permissao_usuarios.id_usuario = my_schema.users.id OR my_schema.permissao_usuarios.id_usuario_visto = my_schema.users.id
							WHERE my_schema.permissao_usuarios.id_usuario = ?`;
            connection.query(insertQuery, [req.user.id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_custos_todos_temp', function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT N.id, D.nome, CAST(str_to_date(data,'%d/%m/%Y') AS char) as data, cc, N.cartao, descricao, cnpj, valor, status, tipo_gasto FROM my_schema.notinhas N left join dados_usuario D on N.chat_id=D.id_telegram 
							WHERE STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE(?,'%d/%m/%Y') AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE(?,'%d/%m/%Y') order by id desc`;
            
            connection.query(insertQuery, [url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT N.id, N.nome, CAST(str_to_date(data,'%d/%m/%Y') AS char) as data, cc, cartao, descricao, cnpj, valor, status, tipo_gasto FROM my_schema.notinhas N
							left join dados_usuario D on N.chat_id=D.id_telegram`;
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_custos_todos_prov', function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT * FROM my_schema.notinhas WHERE \`status\` = "Lancada";`;
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT * FROM my_schema.notinhas WHERE \`status\` = "Lancada";`;
            connection.query(insertQuery, [req.user.id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }

	});

		// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_desl_todos_prov', function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT * FROM my_schema.deslocamentos WHERE \`status\` = "Lancada";`;
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT * FROM my_schema.deslocamentos WHERE \`status\` = "Lancada";`;
            connection.query(insertQuery, [req.user.id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_atividade_todos', isLoggedIn, function(req, res) {
/*
		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT DISTINCT my_schema.ponto.id, my_schema.dados_usuario.nome,DATE_FORMAT(\`data\`, "%d/%m/%Y") as \`data\`, inicio, fim, cc, my_schema.ponto.cargo, modalidade, atividade, my_schema.permissao_usuarios.id_usuario, aprovacao FROM my_schema.ponto
								LEFT JOIN my_schema.users ON my_schema.users.id = my_schema.ponto.usuario
								LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.username = my_schema.users.username
								LEFT JOIN my_schema.permissao_usuarios ON my_schema.permissao_usuarios.id_usuario = my_schema.ponto.usuario OR my_schema.permissao_usuarios.id_usuario_visto = my_schema.ponto.usuario
								WHERE my_schema.permissao_usuarios.id_usuario = ? AND \`data\` >= STR_TO_DATE(?,'%d/%m/%Y') AND \`data\` <= STR_TO_DATE(?,'%d/%m/%Y')`;
            connection.query(insertQuery, [req.user.id,url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
	        var insertQuery = `SELECT DISTINCT my_schema.ponto.id, my_schema.dados_usuario.nome,DATE_FORMAT(\`data\`, "%d/%m/%Y") as \`data\`, inicio, fim, cc, my_schema.ponto.cargo, modalidade, atividade, my_schema.permissao_usuarios.id_usuario, aprovacao FROM my_schema.ponto
								LEFT JOIN my_schema.users ON my_schema.users.id = my_schema.ponto.usuario
								LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.username = my_schema.users.username
								LEFT JOIN my_schema.permissao_usuarios ON my_schema.permissao_usuarios.id_usuario = my_schema.ponto.usuario OR my_schema.permissao_usuarios.id_usuario_visto = my_schema.ponto.usuario
								WHERE my_schema.permissao_usuarios.id_usuario = ?`;

	            connection.query(insertQuery, [req.user.id], function(err, recordset){
	                if (err) console.log(err)

		            // send records as a response
		            res.send(recordset);

	        });
        }*/

        var colab = req.param('colab');
		var mes = req.param('mes');

		console.log(colab);

        var insertQuery = `select id, colaborador, CAST(str_to_date(data,'%d/%m/%Y') AS char) as Data, entrada, saida, cc, tipo, cidade,motorista,veiculo,km_Inicial,km_Final, funcao, solicitante, pernoite, relatorio, aprovacao from relatorio where colaborador= ? and CAST(date_format(str_to_date(data,'%d/%m/%Y'),'%m/%Y') AS char) =?  order by id desc;`;
            connection.query(insertQuery, [colab,mes], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/dados_user', isLoggedIn, function(req, res) {

        var insertQuery = "SELECT * FROM my_schema.dados_usuario LEFT JOIN my_schema.users ON my_schema.users.username = my_schema.dados_usuario.nome where id = ?";
            connection.query(insertQuery, [req.user.id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/list_users', isLoggedIn, function(req, res) {

        var insertQuery = `SELECT nome, id FROM my_schema.dados_usuario 
							LEFT JOIN my_schema.users ON my_schema.users.username = my_schema.dados_usuario.username`;
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});


	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/list_permissions', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = `SELECT id, nome, 
							IF(ISNULL((SELECT id_usuario FROM my_schema.permissao_usuarios 
							          WHERE (my_schema.permissao_usuarios.id_usuario_visto = id 
							          AND my_schema.permissao_usuarios.id_usuario = ?) 
							          )), "Negado", "Permitido") AS Acesso FROM my_schema.dados_usuario
							LEFT JOIN my_schema.users ON my_schema.users.username = my_schema.dados_usuario.username
							WHERE NOT(id=?)`;
            connection.query(insertQuery,[url_id, url_id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/list_permissions_pages', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = `SELECT paginas, IF(ISNULL(nome_pagina),"Negado","Permitido") As Acesso FROM my_schema.paginas 
							LEFT JOIN my_schema.acesso_paginas 
							ON my_schema.paginas.paginas = my_schema.acesso_paginas.nome_pagina AND id_usuario = ?`;
            connection.query(insertQuery,[url_id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_projetos', isLoggedIn, function(req, res) {

        var insertQuery = "SELECT * FROM my_schema.projetos ORDER BY cc DESC";
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/info_projeto', isLoggedIn, function(req, res) {

        url_projeto = req.param('projeto');

        var insertQuery = "SELECT * FROM my_schema.projetos WHERE cc = ?";
        connection.query(insertQuery, [url_projeto], function(err, recordset){
            if (err) console.log(err)

            // send records as a response
            res.send(recordset);

        });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/list_users', isLoggedIn, function(req, res) {

        var insertQuery = "SELECT nome FROM dados_usuario";
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Power Monitor      ==================
	// =====================================
	app.get('/power_monitor', isLoggedIn, function(req, res) {

        var insertQuery = "SELECT * FROM my_schema.power_monitor ORDER BY interacao DESC LIMIT 1;";
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/get_cnpj', isLoggedIn, function(req, res) {
		var url_cnpj = req.param('cnpj');

		console.log('##############################################');
		console.log(url_cnpj);

        cnpj.consultaCNPJ({cnpj: url_cnpj })
		.then(result => {
		    console.log(result)
		    res.send(result)
		})
		.catch(error => {
		    console.log(error)
		});
	});

	//====================================================================================================================================
	//Route: Json from python
	app.get('/python', (req, res) => {

		var largeDataSet = [];
		 // spawn new child process to call the python script
		 const python = spawn('python', ['./python/tempo_real.py']);
		 // collect data from script
		 python.stdout.on('data', function (data) {
		  console.log('Pipe data from python script ...');
		  largeDataSet.push(data);
		 });
		 // in close event we are sure that stream is from child process is closed
		 python.on('close', (code) => {
		 console.log(`child process close all stdio with code ${code}`);
		 // send data to browser
		 res.send(largeDataSet.join(""))

		 //var id = req.body.IPadress
		 //console.log('value: ' + id);
		 console.log(largeDataSet);
		 return;

	 		});
	});

	app.all('/express-flash', (req, res ) => {
	  req.flash('success', 'This is a flash message using the express-flash module.');
	  res.redirect(301, '/');
	});

};


// route middleware to make sure
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
}

//Verifica se o usuário tem acesso à página requisitada
function check_acess (req, res, page_name) {

	var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    connection.query(insertQuery, [req.user.id, page_name], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
        			console.log("CCCCCCCCCCCCC");
	        		return true;
        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
    
}
