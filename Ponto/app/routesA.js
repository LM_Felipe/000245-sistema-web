const {spawn} = require('child_process');
let CNPJ = require('consulta-cnpj-ws');
let cnpj = new CNPJ();
const bcrypt = require('bcrypt-nodejs');
const nodemailer = require('nodemailer');

// app/routes.js
module.exports = function(app, passport) {


	app.post('/finalizaProjeto', isLoggedIn, function(req, res) {

		url_cc = req.param('cc');

	    var insertQuery = "UPDATE my_schema.projetos SET status='finalizado' WHERE cc = ?";
        connection.query(insertQuery,[url_cc], function(err, rows){
            if (err) {
                //return done(err
				req.flash('error', err);
			}
			else 
			{
				console.log('Centro de custo '+url_cc+' finalizado com sucesso');
            	req.flash('editar_projeto_sucess', 'Projeto finalizado com sucesso');
            	res.redirect('/editar_projeto');           
            }
            
        });

	});

	app.post('/reabreProjeto', isLoggedIn, function(req, res) {

		url_cc = req.param('cc');

	    var insertQuery = "UPDATE my_schema.projetos SET status='aberto' WHERE cc = ?";
        connection.query(insertQuery,[url_cc], function(err, rows){
            if (err) {
                //return done(err
				req.flash('error', err);
			}
			else 
			{
				console.log('Centro de custo '+url_cc+' reaberto com sucesso');
            	req.flash('editar_projeto_sucess', 'Projeto reaberto com sucesso');
            	res.redirect('/editar_projeto');           
            }
            
        });

	});

	// =====================================
	// Altera registro de deslocamento =====
	// =====================================

	app.post('/deleta_deslocamento', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.deslocamentos set status='Apagada' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_deslocamentos_sucess', 'Deslocamento apagado com sucesso');
                	res.redirect('/ver_deslocamento');           
                }
                
            });

	});

	app.post('/lancar_deslocamento', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.deslocamentos set status='Lancada' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_deslocamentos_sucess', 'Deslocamento lançado com sucesso');
                	res.redirect('/ver_deslocamento');           
                }
                
            });

	});

	app.post('/lancar_nota', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.notinhas set status='Lancada' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_deslocamentos_sucess', 'Deslocamento lançado com sucesso');
                	res.redirect('/ver_custos_todos');           
                }
                
            });

	});

	app.post('/lancar_abastecimento', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.abastecimento set status='Lancada' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_abastecimentos_sucess', 'Abastecimento lançado com sucesso');
                	res.redirect('/ver_abastecimentos');           
                }
                
            });

	});		

	app.post('/get_img_url', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "select caminho_imagem from my_schema.notinhas WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, recordset){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	res.send(recordset);          
                }
                
            });

	});

	app.post('/get_img_url_deslocamentos', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "select caminho_imagem from my_schema.deslocamentos WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, recordset){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	res.send(recordset);          
                }
                
            });

	});

	app.post('/get_img_url_abastecimentos', isLoggedIn, function(req, res) {

		url_id = req.param('id');
		console.log("ID = " + url_id);

        var insertQuery = "select caminho_imagem from my_schema.abastecimento WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, recordset){
                if (err) {
                    //return done(err
                    console.log("NÃO deu certo");
					req.flash('error', err);
				}
				else 
				{
                	res.send(recordset);   
                	console.log("deu certo");       
                }
                
            });

	});	

	app.post('/erro_nota', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.notinhas set status='Erro detectado' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_deslocamentos_sucess', 'Erro reportado com sucesso');
                	res.redirect('/ver_custos_todos');           
                }
                
            });

	});

	app.post('/erro_deslocamento', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.deslocamentos set status='Erro detectado' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_deslocamentos_sucess', 'Erro reportado com sucesso');
                	res.redirect('/ver_custos_todos');           
                }
                
            });

	});

	app.post('/erro_abastecimento', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.abastecimento set status='Erro detectado' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_abastecimentos_sucess', 'Erro reportado com sucesso');
                	res.redirect('/ver_abastecimentos');           
                }
                
            });

	});

	app.post('/update_deslocamento', isLoggedIn, function(req, res) {

		var id = req.body.id;
		var data = req.body.data;
		var nome = req.body.nome;
		var km = req.body.km;
		var cc = req.body.cc;
		var cnpj = req.body.cnpj;
		var placa = req.body.placa;
		var descricao = req.body.descricao;
		var tipo = req.body.tipo;
		var telefone = req.body.telefone;
		var status = req.body.status;


        var insertQuery = "UPDATE `my_schema`.`deslocamentos` SET `data` = ?, `km` = ?, `cc` = ?, `cnpj` = ?, `placa` = ?, `descricao` = ?, `tipo` = ?, `numero_telefone` = ?, `status` = ? WHERE `id` = ?";
            connection.query(insertQuery,[ data, km, cc, cnpj, placa, descricao, tipo, telefone, status, id], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('ver_deslocamentos_sucess', 'Registro atualizado com sucesso');
                	res.redirect('/ver_deslocamento');
                }

                
            });

	});

	app.get('/registro_deslocamentos', function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `select id, D.nome, km, cnpj, cc, data, placa, descricao, tipo, D.numero_telefone, status from deslocamentos D left join cadastro_provisorio C on D.numero_telefone=C.numero_telefone WHERE STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE(?,'%d/%m/%Y') AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE(?,'%d/%m/%Y')`;
            connection.query(insertQuery, [url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `select id, C.nome, km, cnpj, cc, data, placa, descricao, tipo, D.numero_telefone, status from deslocamentos D left join cadastro_provisorio C on D.numero_telefone=C.numero_telefone`;
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_abastecimentos', function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT id, nome, valor, cnpj, cc, \`data\`, cartao, placa, litros, km, status FROM my_schema.abastecimento
							WHERE STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE(?,'%d/%m/%Y') AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE(?,'%d/%m/%Y') order by id desc`;
            
            connection.query(insertQuery, [url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT id, nome, valor, cnpj, cc, \`data\`, cartao, placa, litros, km, status FROM my_schema.abastecimento`;
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }
	});	

	app.get('/registro_veiculos', function(req, res) {

    	var insertQuery = `select * from veiculos`;
        connection.query(insertQuery, function(err, recordset){
	        if (err) console.log(err)

	        // send records as a response
	        res.send(recordset);

        });
 

	});

	app.post('/newVeiculo', isLoggedIn, function(req, res) {

		var marca = req.body.marca;
		var modelo = req.body.modelo;
		var placa = req.body.placa;
		var oleokm = req.body.oleokm;
		var oleodata = req.body.oleodata;
		var lic = req.body.lic;
		var seg = req.body.seg;
		var revkm = req.body.revkm;
		var dataRev = req.body.dataRev;

        var insertQuery = "INSERT INTO veiculos values (id,?,?,?,?,?,?,?,?,?)";
       
        connection.query(insertQuery,[marca,modelo,placa,oleokm,oleodata,lic,seg,revkm,dataRev], function(err, rows){
            if (err) {
                //return done(err
				req.flash('error', err);
			}
			else 
			{
            	// all is well, return successful user
            	req.flash('ponto_sucess', 'Veículo cadastrado com sucesso');
            	res.redirect('/cadastro_veiculo');
            }

            
        });

	});

	app.get('/ver_deslocamento', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "ver_deslocamento"], function(err, recordset){

	        if (err) {
	        	res.redirect('/acesso_negado');
	        	}
	        	else {
	        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('ver_deslocamentos.ejs', { message: req.flash('ver_deslocamentos_sucess')});
					        		}
	        		else
	        		{
		        		res.redirect('/acesso_negado');
		        	}
	        	} 
    	});
	});

	app.get('/ver_abastecimentos', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "ver_abastecimentos"], function(err, recordset){

	        if (err) {
	        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('ver_abastecimentos.ejs', { message: req.flash('ver_abastecimentos_sucess')});
        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    	});
	});	


	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	app.get('/', function(req, res) {
		res.render('login.ejs', { message: req.flash('loginMessage'),message_success: req.flash('password_change') });
	});

	// =====================================
	// LOGIN ===============================
	// =====================================
	// show the login form
	app.get('/login', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('login.ejs', { user : req.user, // get the user out of session and pass to template
								  message: req.flash('loginMessage'), message_success: req.flash('password_change') });
	});

	app.get('/localizar_conta', function(req, res)
	{
		res.redirect('/login');
	});

	// chama página para enviar código por e-mail
	app.post('/localizar_conta', function(req, res) {
		var email_input = req.body.email;
		var select_email = "SELECT * FROM my_schema.dados_usuario WHERE email = ?";

		connection.query(select_email, [email_input], function(err, recordset){
	        if (err) {
	        	res.render('set_password.ejs', { message: 'Algo deu errado, tente novamente mais tarde'});
	    	}
	    	else {
	    		if (recordset.length) {

	    			// selecionar nome do usuário para enviar por e-mail
	    			var select_nome = "SELECT nome FROM my_schema.dados_usuario WHERE email = ?";
				    connection.query(select_nome, [email_input], (err, rows) => {
				    	
				    	// pega o primeiro nome apenas
				    	var nome = rows[0]["nome"].split(' ')[0];

		    			// gera código para enviar por e-mail
		    			var codigo = Math.random().toString(36).slice(-6).toUpperCase();

		    			//exclui registro antigo
		    			var deleta = "DELETE FROM my_schema.codigos_senha WHERE email = ?";
				    	connection.query(deleta, [email_input]);

		    			// inserir código no banco de dados para ser consultado para verificação
		    			var select_query="INSERT INTO my_schema.codigos_senha (codigo, email) VALUES (?,?)";
		    			connection.query(select_query, [bcrypt.hashSync(codigo, null, null),email_input], (err, rows) => { 
		    				if (err) {
		    					res.render('set_password.ejs', { message: 'Algo deu errado, tente novamente mais tarde'});
							}
							else{
								// Configurações de e-mail
								const transporter = nodemailer.createTransport({
								  host: "mail.lmlogix.com.br",
								  port: 587,
								  secure: false, // true for 465, false for other ports
								  auth: {
								  	user: "noreply@lmlogix.com.br",
								  	pass: "logix16"
								  },
								  tls: { rejectUnauthorized: false }
								});

								var data = new Date();

								const mailOptions = {
								  from: 'noreply@lmlogix.com.br',
								  to: email_input,
								  subject: 'E-mail Automático para Redefinição de Senha',
								  html: "<h1>Olá, " + nome + "!</h1> Recebemos uma solicitação para redefinir a senha de sua conta no SARR. Aqui está seu código para criar uma nova senha:<br><br><h2 align='center'>" + codigo + "</h2><br>Quando isto ocorreu: <br>" + data + "."
								};

								transporter.sendMail(mailOptions, function(error, info){
								  if (error) {
								    console.log(error);
								    res.render('set_password.ejs', { message: 'Algo deu errado, tente novamente mais tarde'});
								  } else {
								    console.log('Email enviado: ' + info.response);
								    // render the page and pass in any flash data if it exists
									res.render('codigo_reseta_senha.ejs', { message: req.flash('cod_fail'),email: email_input});
								  }
								});
			                }

		    			});

			    	});
	    		}
	    		else
	    		{
	        		res.render('set_password.ejs', { message: 'Nenhum usuário encontrado'});
	        	}
	    	} 
        });
	});

	// caso "check_code" seja chamado sem passar por "set_password"
	app.get('/check_code', function(req, res)
	{
		res.redirect('/login');
	});

	// chama página para inserir código
	app.post('/check_code', function(req, res) {
		var code = req.body.code;
		var email = req.body.email;
		var select_code = "SELECT * FROM my_schema.codigos_senha WHERE email = ?";

		connection.query(select_code, [email], function(err, recordset){
	        if (err) {
	        	console.log(err);
	        	res.render('codigo_reseta_senha.ejs', { message: req.flash('message','Algo deu errado, tente novamente mais tarde'), email:email});
	    	}
	    	else {
	    		if (recordset.length) {
	    				var now= new Date();
	    				if (bcrypt.compareSync(code.toUpperCase(), recordset[0]["codigo"])) {
	    					if ((now.getTime() - recordset[0]["geradoEm"].getTime())>2*60*1000) {
	    						req.flash('expired_code','O código expirou, tente novamente');
	    						res.redirect('/set_password');

	    					}
	    					else{
	    						res.render('nova_senha.ejs', { message: req.flash('cod_fail'),email: recordset[0]["email"]});
	    					}
	    				}
	    				else{
	    					req.flash('cod_fail', 'Código inválido');
	    					res.render('codigo_reseta_senha.ejs', { message: req.flash('cod_fail'), email:email});
	    				}
				    	
				}
	    		else {
	    			req.flash('cod_fail', 'Algo deu errado');
	        		res.render('codigo_reseta_senha.ejs', { message: req.flash('message'), email:email});
	        	}
	    	}
        });
	});

	app.post('/change_password', function(req, res)
	{
		var senha = req.body.senha;
		var confirmaSenha = req.body.confirmaSenha;
		var email = req.body.email;

		console.log('este é o email: '+email);

		if(senha == confirmaSenha)
		{
			var select_senha = "SELECT username from my_schema.dados_usuario where email = ?";
			connection.query(select_senha, [email], function(err, recordset)
			{
				
		        if (err) {
	        		res.render('nova_senha.ejs', { message: 'ALgo deu errado, tente novamente mais tarde',email: email});
	    		}
	    		else
	    		{
	    			if(recordset.length)
	    			{
	    				console.log('recordset');
	    				var update_senha = "UPDATE my_schema.users set password = ? where username = ?";
	    				connection.query(update_senha, [bcrypt.hashSync(senha, null, null), recordset[0]["username"]], function(err, recordset)
	    				{
	    					if(err)
	    					{
	    						req.flash('cod_fail','Algo deu errado, tente novamente mais tarde');
	    						res.render('nova_senha.ejs', { message: req.flash('cod_fail'),email: email});
	    					}
	    					else
	    					{
	    						req.flash('password_change','Senha alterada com sucesso');
	    						res.redirect('/login');
	    					}
	    				});
	    			}
	    			else
	    			{
	    				req.flash('cod_fail','Algo deu errado, tente novamente');
	    				res.render('nova_senha.ejs', { message: req.flash('cod_fail'),email: email});
	    			}
	    		}

			});
		}
		else
		{
			req.flash('cod_fail','Senhas não conferem');
			res.render('nova_senha.ejs', { message: req.flash('cod_fail'),email: email});
		}
	});





	// show the login form
	app.get('/signup_2', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('signup_2.ejs', { message: req.flash('loginMessage') });
	});

	app.get('/cadastro_veiculo', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "cadastro_veiculo"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
				// render the page and pass in any flash data if it exists
				res.render('cadastro_veiculo.ejs', { message: req.flash('cadastro_veiculo_sucess')});
				        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});


	// show the login form
	app.get('/acesso_negado', isLoggedIn, function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('acesso_negado.ejs', { message: req.flash('ponto_sucess') });
	});

	// show the login form
	app.get('/ponto', isLoggedIn, function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('ponto_2.ejs', { message: req.flash('ponto_sucess') });
	});

	// show the login form
	app.get('/cadastro_projeto', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "cadastro_projeto"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('cadastro_projeto_2.ejs', { message: req.flash('cadastro_projeto_sucess'), message_error: req.flash('cadastro_projeto_error') });
					        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	// show the login form
	app.get('/editar_projeto', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "editar_projeto"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('editar_projeto.ejs', { message: req.flash('editar_projeto_sucess'), message_error: req.flash('editar_projeto_error') });
				}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	// show the login form
	app.get('/ver_ponto', isLoggedIn, function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('ver_ponto.ejs', { message: req.flash('ver_ponto_sucess'), message_danger: req.flash('ver_ponto_danger') });
	});

	// show the login form
	app.get('/exporta_planilha', isLoggedIn, function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('exporta_planilha.ejs', { message: req.flash('ver_ponto_sucess'), message_danger: req.flash('ver_ponto_danger') });
	});

	// show the login form
	app.get('/ver_atividades_todos', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "ver_atividades_todos"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('ver_ponto_todos.ejs', { message: req.flash('ver_ponto_todos_sucess') });
				        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	// show the login form
	app.get('/aprovacao', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "aprovacao"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('aprovacao.ejs', { message: req.flash('aprovacao_sucess') });
				        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	// show the login form
	app.get('/ver_custos', isLoggedIn, function(req, res) {

        res.render('ver_custos.ejs', { message: req.flash('ver_custo_sucess')});
	});

	// show the login form
	app.get('/ver_custos_temp', function(req, res) {

        res.render('ver_custos_todos_temp.ejs', { message: req.flash('ver_custo_sucess')});
	});

	// show the login form
	app.get('/ver_desl_prov', function(req, res) {

        res.render('ver_desl_todos_prov.ejs', { message: req.flash('ver_custo_sucess')});
	});


	// show the login form
	app.get('/ver_custos_todos', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "ver_custos_todos"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
				// render the page and pass in any flash data if it exists
				res.render('ver_custos_todos.ejs', { message: req.flash('ver_custo_sucess')});
				        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	// show the login form
	app.get('/editar_permissoes', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "editar_permissoes"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('editar_permissoes.ejs', { message: req.flash('editar_permissoes_sucess')});
					        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
	});

	// show the login form
	app.get('/editar_acesso_paginas', isLoggedIn, function(req, res) {
		var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    	connection.query(insertQuery, [req.user.id, "editar_acesso_paginas"], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
					// render the page and pass in any flash data if it exists
					res.render('editar_acesso_paginas.ejs', { message: req.flash('editar_permissoes_sucess')});
				}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	}
	});
    });

	// process the login form
	app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/ponto', // redirect to the secure profile section
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
		}),
        function(req, res) {
            console.log("hello");

            if (req.body.remember) {
              req.session.cookie.maxAge = 1000 * 60 * 3;
            } else {
              req.session.cookie.expires = false;
            }
        res.redirect('/login');
    });

    app.post('/permissao_paginas', function(req, res) {
    	var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ?";
    	connection.query(insertQuery, [req.user.id], function(err, recordset){
	        if (err) {
	        	res.send(recordset);
        	}
        	else {
        		if (recordset.length) {
        			res.send(recordset);
				}
        		else
        		{
	        		res.send(recordset);
	        	}
        	}    		
    	});
    });    

	// =====================================
	// SIGNUP ==============================
	// =====================================
	// show the signup form
	app.get('/signup', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('signup.ejs', { message: req.flash('signupMessage') });
	});

	// =====================================
	// SETPASSWORD ==============================
	// =====================================
	// show the signup form	
	app.get('/set_password', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('set_password.ejs', { message: req.flash('expired_code') });
	});

	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/tempo_real', isLoggedIn, function(req, res) {
		res.render('tempo_real.ejs', {
			user : req.user // get the user out of session and pass to template
		});
	});

	// process the signup form
	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/ponto', // redirect to the secure profile section
		failureRedirect : '/signup', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/profile', isLoggedIn, function(req, res) {
		res.render('profile.ejs', {
			user : req.user // get the user out of session and pass to template
		});
	});

	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/login');
	});


	// =====================================
	// Cadastra Projeto ====================
	// =====================================
	app.post('/cadastro_projeto', function(req, res) {

		var nome_lm = req.body.nome_lm;
		var nome_cliente = req.body.nome_cliente;
		var email = req.body.email_cliente;
		var telefone_cliente = req.body.telefone_cliente;
		var cnpj = req.body.cnpj;
		var nome_empresa = req.body.nome_empresa;
		var cep = req.body.cep;
		var cidade = req.body.cidade;
		var estado = req.body.estado;
		var pais = req.body.pais;
		var tipo_projeto = req.body.tipo_projeto;
		var olm = req.body.olm;
		var cc = req.body.cc;
		var nome_obra = req.body.nome_obra;

		var planejamento_programacao = req.body.planejamento_programacao;
		var instalacao_programacao = req.body.instalacao_programacao;
		var comissionamento_programacao = req.body.comissionamento_programacao;
		var startup_programacao = req.body.startup_programacao;
		var acompanhamento_programacao = req.body.acompanhamento_programacao;
		var documentacao_programacao = req.body.documentacao_programacao;

		var planejamento_eletrica = req.body.planejamento_eletrica;
		var instalacao_eletrica = req.body.instalacao_eletrica;
		var comissionamento_eletrica = req.body.comissionamento_eletrica;
		var startup_eletrica = req.body.startup_eletrica;
		var acompanhamento_eletrica = req.body.acompanhamento_eletrica;
		var documentacao_eletrica = req.body.documentacao_eletrica;

		var data_pedido = req.body.data;
		var valor_pedido = req.body.valor_pedido;


        var insertQuery = `INSERT INTO projetos ( nome_lm, nome_cliente, email, telefone_cliente, cnpj, nome_empresa, cep, cidade, estado, pais, tipo_projeto, olm, cc, nome_obra,
                           planejamento_programacao,instalacao_programacao,comissionamento_programacao,startup_programacao,acompanhamento_programacao,documentacao_programacao,
                           planejamento_eletrica,instalacao_eletrica,comissionamento_eletrica,startup_eletrica,acompanhamento_eletrica,documentacao_eletrica,
                           data_pedido,valor_pedido, status) 
                           values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, "aberto")`;
            connection.query(insertQuery,[ nome_lm, nome_cliente, email, telefone_cliente, cnpj, nome_empresa, cep, cidade, estado, pais, tipo_projeto, olm, cc, nome_obra,
            			   planejamento_programacao,instalacao_programacao,comissionamento_programacao,startup_programacao,acompanhamento_programacao,documentacao_programacao,
                           planejamento_eletrica,instalacao_eletrica,comissionamento_eletrica,startup_eletrica,acompanhamento_eletrica,documentacao_eletrica,data_pedido,valor_pedido], function(err, rows){
            	if (err) {
                    //return done(err

                    if (err.code == "ER_DUP_ENTRY") {
                    	req.flash('cadastro_projeto_error', "Esse número de CC já existe, favor utilizar outro número");
                    	res.redirect('/cadastro_projeto');
                    }
                    else {
                    	req.flash('cadastro_projeto_error', err.sqlMessage);
						res.redirect('/cadastro_projeto');
                    }

                    console.log(err);
					
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('cadastro_projeto_sucess', 'Projeto criado com sucesso');
                	res.redirect('/cadastro_projeto');
                }
	}); });

	// =====================================
	// Cadastra Projeto ====================
	// =====================================
	app.post('/editar_projeto', function(req, res) {

		var nome_lm = req.body.nome_lm;
		var nome_cliente = req.body.nome_cliente;
		var email = req.body.email_cliente;
		var telefone_cliente = req.body.telefone_cliente;
		var cnpj = req.body.cnpj;
		var nome_empresa = req.body.nome_empresa;
		var cep = req.body.cep;
		var cidade = req.body.cidade;
		var estado = req.body.estado;
		var pais = req.body.pais;
		var tipo_projeto = req.body.tipo_projeto;
		var olm = req.body.olm;
		var cc = req.body.cc;
		var nome_obra = req.body.nome_obra;

		var planejamento_programacao = req.body.planejamento_programacao;
		var instalacao_programacao = req.body.instalacao_programacao;
		var comissionamento_programacao = req.body.comissionamento_programacao;
		var startup_programacao = req.body.startup_programacao;
		var acompanhamento_programacao = req.body.acompanhamento_programacao;
		var documentacao_programacao = req.body.documentacao_programacao;

		var planejamento_eletrica = req.body.planejamento_eletrica;
		var instalacao_eletrica = req.body.instalacao_eletrica;
		var comissionamento_eletrica = req.body.comissionamento_eletrica;
		var startup_eletrica = req.body.startup_eletrica;
		var acompanhamento_eletrica = req.body.acompanhamento_eletrica;
		var documentacao_eletrica = req.body.documentacao_eletrica;

		var data_pedido = req.body.data;
		var valor_pedido = req.body.valor_pedido;

		var cc_old = req.body.cc;


        var insertQuery = `UPDATE projetos SET nome_lm=?, nome_cliente=?, email=?, telefone_cliente=?, cnpj=?, nome_empresa=?, cep=?, cidade=?, estado=?, pais=?, tipo_projeto=?, olm=?, cc=?, nome_obra=?,
                           planejamento_programacao=?,instalacao_programacao=?,comissionamento_programacao=?,startup_programacao=?,acompanhamento_programacao=?,documentacao_programacao=?,
                           planejamento_eletrica=?,instalacao_eletrica=?,comissionamento_eletrica=?,startup_eletrica=?,acompanhamento_eletrica=?,documentacao_eletrica=?,
                           data_pedido=?,valor_pedido=? WHERE cc=?`;

            connection.query(insertQuery,[ nome_lm, nome_cliente, email, telefone_cliente, cnpj, nome_empresa, cep, cidade, estado, pais, tipo_projeto, olm, cc, nome_obra,
            			   planejamento_programacao,instalacao_programacao,comissionamento_programacao,startup_programacao,acompanhamento_programacao,documentacao_programacao,
                           planejamento_eletrica,instalacao_eletrica,comissionamento_eletrica,startup_eletrica,acompanhamento_eletrica,documentacao_eletrica,data_pedido,valor_pedido,cc_old], function(err, rows){
            	if (err) {
                    //return done(err

                    if (err.code == "ER_DUP_ENTRY") {
                    	req.flash('editar_projeto_error', "Esse número de CC já existe, favor utilizar outro número");
                    	res.redirect('/editar_projeto');
                    }
                    else {
                    	req.flash('editar_projeto_error', err.sqlMessage);
						res.redirect('/editar_projeto');
                    }

                    console.log(err);
					
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('editar_projeto_sucess', 'Projeto alterado com sucesso');
                	res.redirect('/editar_projeto');
                }
	}); });

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/ponto', isLoggedIn, function(req, res) {

		var data = req.body.data;
		var inicio = req.body.inicio;
		var fim = req.body.fim;
		var cc = req.body.cc;
		var cargo = req.body.cargo;
		var modalidade = req.body.modalidade;
		var local = req.body.local;
		var atividade = req.body.atividade;
		var etapa = req.body.etapa;

		var colegas = req.body.colegas;

		if (typeof(colegas) != "undefined") {

			if (Array.isArray(colegas)) {
				colegas.forEach(myFunction);

				function myFunction(value, index, array) {
				  var insertQuery = "INSERT INTO marcacao (quem_marcou, id, data, inicio, fim, cc, modalidade, local, etapa) values (?,?,?,?,?,?,?,?,?)";
	              connection.query(insertQuery,[req.user.id, value, data, inicio, fim, cc, modalidade, local, etapa]);
				}
			}
			else {
			  var insertQuery = "INSERT INTO marcacao (quem_marcou, id, data, inicio, fim, cc, modalidade, local, etapa) values (?,?,?,?,?,?,?,?,?)";
              connection.query(insertQuery,[req.user.id, colegas, data, inicio, fim, cc, modalidade, local, etapa]);
		}

		}

        var insertQuery = "INSERT INTO ponto ( usuario, data, inicio, fim, cc, cargo, modalidade, local, atividade, aprovacao, etapa ) values (?,?,?,?,?,?,?,?,?,\"Não aprovado\", ?)";
            connection.query(insertQuery,[req.user.id, data, inicio, fim, cc, cargo, modalidade, local, atividade, etapa ], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('ponto_sucess', 'Atividade registrada com sucesso');
                	res.redirect('/ponto');
                }

                
            });

	});

	// =====================================
	// Deleta atividade ==================
	// =====================================
	app.post('/deleta_atividade', isLoggedIn, function(req, res) {

		url_id = req.param('id');
		origem = req.param('origem');

        var insertQuery = "DELETE FROM my_schema.ponto WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
					if (origem == 1) {
	                	// all is well, return successful user
	                	req.flash('ver_ponto_danger', 'Atividade apagada com sucesso');
	                	res.redirect('/ver_ponto'); 
	                }
	                else if (origem == 2) {
	                	// all is well, return successful user
	                	req.flash('ver_ponto_todos_sucess', 'Atividade apagada com sucesso');
	                	res.redirect('/ver_atividades_todos');  
	                }              
                }
                
            });

	});

	// =====================================
	// Deleta atividade ==================
	// =====================================
	app.post('/deleta_custo', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = "update my_schema.notinhas set status='Apagada' WHERE id = ?";
            connection.query(insertQuery,[url_id], function(err, rows){
                if (err) {
                    //return done(err
					req.flash('error', err);
				}
				else 
				{
                	req.flash('ver_custo_sucess', 'Atividade apagada com sucesso');
                	res.redirect('/ver_custos');    
                }
                
            });

	});


	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/update_ponto', isLoggedIn, function(req, res) {

		var id = req.body.id;
		var data = req.body.data;
		var inicio = req.body.inicio;
		var fim = req.body.fim;
		var cc = req.body.cc;
		var cargo = req.body.cargo;
		var etapa = req.body.etapa;
		var modalidade = req.body.modalidade;
		var atividade = req.body.atividade;

        var insertQuery = "UPDATE `my_schema`.`ponto` SET `data` = ?, `inicio` = ?, `fim` = ?, `cc` = ?, `cargo` = ?, `modalidade` = ?, `atividade` = ?, `etapa` = ? WHERE `id` = ?";
            connection.query(insertQuery,[ data, inicio, fim, cc, cargo, modalidade, atividade, etapa, id], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('ver_ponto_sucess', 'Atividade atualizada com sucesso');
                	res.redirect('/ver_ponto');
                }

                
            });

	});

	// =====================================
	//    Altera veículo  ==================
	// =====================================
	app.post('/update_Veiculo', isLoggedIn, function(req, res) {

		var id = req.body.id;
		var km_oleo = req.body.km_oleo;
		var data_oleo = req.body.data_oleo;
		var data_lic = req.body.data_lic;
		var data_seg = req.body.data_seg;
		var data_revisao = req.body.data_revisao;
		var km_revisao = req.body.km_revisao;

		console.log(km_revisao);

        var insertQuery = "UPDATE `my_schema`.`veiculos` SET `km_oleo` = ?, `data_oleo` = ?, `data_lic` = ?, `data_seg` = ?, `km_revisao` = ?, data_revisao = ? WHERE `id` = ?";
            connection.query(insertQuery,[km_oleo, data_oleo, data_lic, data_seg,km_revisao, data_revisao, id], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('cadastro_veiculo_sucess', 'Registro atualizado com sucesso');
                	res.redirect('/cadastro_veiculo');
                }

                
            });

	});



	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/update_despesa', isLoggedIn, function(req, res) {

		var id = req.body.id;
		var data = req.body.data;
		var cc = req.body.cc;
		var cartao = req.body.cartao;
		var descricao = req.body.descricao;
		var telefone = req.body.telefone;
		var tipo = req.body.tipo;
		var cnpj = req.body.cnpj;
		var valor = req.body.valor;


        var insertQuery = "UPDATE `my_schema`.`notinhas` SET `data` = ?, `cc` = ?, `cartao` = ?, `descricao` = ?, numero_telefone=?, tipo_gasto=?, cnpj=?, valor=? WHERE `id` = ?";
            connection.query(insertQuery,[ data, cc, cartao, descricao, telefone, tipo, cnpj, valor, id], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('ver_custo_sucess', 'Nota atualizada com sucesso');
                	res.redirect('/ver_custos_todos');
                }
                
            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/add_permission', isLoggedIn, function(req, res) {

		var url_nome = req.param('nome');
		var url_alvo = req.param('alvo');

        var insertQuery = `INSERT INTO \`my_schema\`.\`permissao_usuarios\`
							(\`id_usuario\`,
							\`id_usuario_visto\`)
							VALUES
							(?,
							(SELECT id FROM my_schema.users
							LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.username = my_schema.users.username
							WHERE nome = ?));`;

            connection.query(insertQuery,[ url_nome, url_alvo], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('editar_permissoes_sucess', 'Nota atualizada com sucesso');
                	res.redirect('/editar_permissoes');
                }
                
            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/delete_permission', isLoggedIn, function(req, res) {

		var url_nome = req.param('nome');
		var url_alvo = req.param('alvo');

        var insertQuery = `DELETE FROM \`my_schema\`.\`permissao_usuarios\`
							WHERE (\`id_usuario\` = ? AND 
					        \`id_usuario_visto\` = (SELECT id FROM my_schema.users
							LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.username = my_schema.users.username
							WHERE nome = ?));`;

            connection.query(insertQuery,[ url_nome, url_alvo], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('editar_permissoes_sucess', 'Nota atualizada com sucesso');
                	res.redirect('/editar_permissoes');
                }
                
            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/add_permission_pages', isLoggedIn, function(req, res) {

		var url_nome = req.param('nome');
		var url_alvo = req.param('alvo');

        var insertQuery = `INSERT INTO \`my_schema\`.\`acesso_paginas\`
							(\`id_usuario\`,
							\`nome_pagina\`)
							VALUES
							(?,
							?);`;

            connection.query(insertQuery,[ url_nome, url_alvo], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('editar_permissoes_sucess', 'Nota atualizada com sucesso');
                	res.redirect('/editar_acesso_paginas');
                }
                
            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/aprova_ponto', isLoggedIn, function(req, res) {

		var url_id = req.param('id');
		var url_aprovacao = req.param('aprovacao');

		console.log("--------------------------");
		console.log(url_id);
		console.log(url_aprovacao);

        var insertQuery = `UPDATE \`my_schema\`.\`ponto\`
							SET
							\`aprovacao\` = ?
							WHERE \`id\` = ?;`;

            connection.query(insertQuery,[ url_aprovacao, url_id ], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('aprovacao_sucess', 'Ponto aprovado');
                	res.redirect('/aprovacao');
                }
                
            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.post('/delete_permission_pages', isLoggedIn, function(req, res) {

		var url_nome = req.param('nome');
		var url_alvo = req.param('alvo');

        var insertQuery = `DELETE FROM \`my_schema\`.\`acesso_paginas\`
							WHERE id_usuario = ? AND nome_pagina = ?;`;

            connection.query(insertQuery,[ url_nome, url_alvo], function(err, rows){
                if (err) {
                    //return done(err
            		console.log(err);
					req.flash('error', err);
				}
				else 
				{
                	// all is well, return successful user
                	req.flash('editar_permissoes_sucess', 'Nota atualizada com sucesso');
                	res.redirect('/editar_acesso_paginas');
                }
                
            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_atividade', isLoggedIn, function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT DATE_FORMAT(\`data\`, "%d/%m/%Y") as \`data\`, inicio, fim, cc, cargo, modalidade, local, atividade,
        					usuario, id, aprovacao, etapa FROM my_schema.ponto 
        					WHERE usuario = ? AND \`data\` >= STR_TO_DATE(?,'%d/%m/%Y') AND \`data\` <= STR_TO_DATE(?,'%d/%m/%Y')`;
            connection.query(insertQuery, [req.user.id,url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT DATE_FORMAT(\`data\`, "%d/%m/%Y") as \`data\`, inicio, fim, cc, cargo, modalidade, local, atividade,
        						usuario, id, aprovacao, etapa FROM my_schema.ponto  WHERE usuario = ?`;
            connection.query(insertQuery, [req.user.id], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_atividade_planilha', isLoggedIn, function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT "" As nome,DATE_FORMAT(\`data\`, "%d/%m/%Y") as \`data\`, inicio, fim, cc, cargo, modalidade, local, atividade,
        					usuario, id, aprovacao, etapa, "" as cidade, "" as motorista, "" as veiculo, "" as kmi, "" as kmf, "" as kma, "" as litros, "" as valor, "" as pernoite, "" as responsavel FROM my_schema.ponto 
        					WHERE usuario = ? AND \`data\` >= STR_TO_DATE(?,'%d/%m/%Y') AND \`data\` <= STR_TO_DATE(?,'%d/%m/%Y')`;
            connection.query(insertQuery, [req.user.id,url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT "" As nome,DATE_FORMAT(\`data\`, "%d/%m/%Y") as \`data\`, inicio, fim, cc, cargo, modalidade, local, atividade,
        						usuario, id, aprovacao, etapa, "" as cidade, "" as motorista, "" as veiculo, "" as kmi, "" as kmf, "" as kma, "" as litros, "" as valor, "" as pernoite, "" as responsavel FROM my_schema.ponto  WHERE usuario = ?`;
            connection.query(insertQuery, [req.user.id], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/get_marcacao', isLoggedIn, function(req, res) {
		var url_data = req.param('data');

        var insertQuery = `SELECT quem_marcou, my_schema.marcacao.id, data, inicio, fim, cc, modalidade, etapa, nome, local FROM my_schema.marcacao
							LEFT JOIN my_schema.users ON my_schema.marcacao.quem_marcou = my_schema.users.id
							LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.username = my_schema.users.username
							WHERE data = ? AND my_schema.marcacao.id = ? ;`;

            connection.query(insertQuery, [url_data, req.user.id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_custos', isLoggedIn, function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `select N.id, N.nome, \`data\`, cc, cartao, descricao, cnpj, valor, status, tipo_gasto from my_schema.notinhas N
							left join my_schema.dados_usuario D on D.id_telegram= N.chat_id
							left join my_schema.users U on U.username = D.username
							where U.id=? AND STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE(?,'%d/%m/%Y') AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE(?,'%d/%m/%Y')`;
            connection.query(insertQuery, [req.user.id,url_ini,url_fim],function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `select N.id, N.nome, \`data\`, cc, cartao, descricao, cnpj, valor, status, tipo_gasto from my_schema.notinhas N
							left join my_schema.dados_usuario D on D.id_telegram= N.chat_id
							left join my_schema.users U on U.username = D.username
							where U.id=?`;
            connection.query(insertQuery,[req.user.id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_custos_todos', isLoggedIn, function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT my_schema.notinhas.id, my_schema.dados_usuario.nome, numero_telefone, DATE_FORMAT(\`data\`, "%d/%m/%Y") as \`data\`, cc, cartao, descricao FROM my_schema.notinhas
							LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.telefone = my_schema.notinhas.numero_telefone
							LEFT JOIN my_schema.users ON my_schema.users.username = my_schema.dados_usuario.username
							LEFT JOIN my_schema.permissao_usuarios ON my_schema.permissao_usuarios.id_usuario = my_schema.users.id OR my_schema.permissao_usuarios.id_usuario_visto = my_schema.users.id
							WHERE my_schema.permissao_usuarios.id_usuario = ? AND \`data\` >= STR_TO_DATE(?,'%d/%m/%Y') AND \`data\` <= STR_TO_DATE(?,'%d/%m/%Y')`;
            connection.query(insertQuery, [req.user.id,url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT my_schema.notinhas.id, my_schema.dados_usuario.nome, numero_telefone, DATE_FORMAT(\`data\`, "%d/%m/%Y") as \`data\`, cc, cartao, descricao FROM my_schema.notinhas
							LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.telefone = my_schema.notinhas.numero_telefone
							LEFT JOIN my_schema.users ON my_schema.users.username = my_schema.dados_usuario.username
							LEFT JOIN my_schema.permissao_usuarios ON my_schema.permissao_usuarios.id_usuario = my_schema.users.id OR my_schema.permissao_usuarios.id_usuario_visto = my_schema.users.id
							WHERE my_schema.permissao_usuarios.id_usuario = ?`;
            connection.query(insertQuery, [req.user.id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_custos_todos_temp', function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT N.id, N.nome, \`data\`, cc, cartao, descricao, cnpj, valor, status, tipo_gasto FROM my_schema.notinhas N
							left join my_schema.dados_usuario D on D.telefone=N.numero_telefone
							WHERE STR_TO_DATE(\`data\`,'%d/%m/%Y') >= STR_TO_DATE(?,'%d/%m/%Y') AND STR_TO_DATE(\`data\`,'%d/%m/%Y') <= STR_TO_DATE(?,'%d/%m/%Y') order by id desc`;
            
            connection.query(insertQuery, [url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT N.id, N.nome, \`data\`, cc, cartao, descricao, cnpj, valor, status, tipo_gasto FROM my_schema.notinhas N
							left join my_schema.dados_usuario D on D.telefone=N.numero_telefone`;
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_custos_todos_prov', function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT * FROM my_schema.notinhas WHERE \`status\` = "Lancada";`;
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT * FROM my_schema.notinhas WHERE \`status\` = "Lancada";`;
            connection.query(insertQuery, [req.user.id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }

	});

		// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_desl_todos_prov', function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT * FROM my_schema.deslocamentos WHERE \`status\` = "Lancada";`;
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
        	var insertQuery = `SELECT * FROM my_schema.deslocamentos WHERE \`status\` = "Lancada";`;
            connection.query(insertQuery, [req.user.id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_atividade_todos', isLoggedIn, function(req, res) {

		var url_ini = req.param('ini');
		var url_fim = req.param('fim');

		if((url_ini!=0)&&(url_fim!=0)){
        var insertQuery = `SELECT DISTINCT my_schema.ponto.id, my_schema.dados_usuario.nome,DATE_FORMAT(\`data\`, "%d/%m/%Y") as \`data\`, inicio, fim, cc, my_schema.ponto.cargo, modalidade, atividade, my_schema.permissao_usuarios.id_usuario, aprovacao FROM my_schema.ponto
								LEFT JOIN my_schema.users ON my_schema.users.id = my_schema.ponto.usuario
								LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.username = my_schema.users.username
								LEFT JOIN my_schema.permissao_usuarios ON my_schema.permissao_usuarios.id_usuario = my_schema.ponto.usuario OR my_schema.permissao_usuarios.id_usuario_visto = my_schema.ponto.usuario
								WHERE my_schema.permissao_usuarios.id_usuario = ? AND \`data\` >= STR_TO_DATE(?,'%d/%m/%Y') AND \`data\` <= STR_TO_DATE(?,'%d/%m/%Y')`;
            connection.query(insertQuery, [req.user.id,url_ini,url_fim], function(err, recordset){
                if (err) console.log(err)
	            res.send(recordset);
			 });
        }else {
	        var insertQuery = `SELECT DISTINCT my_schema.ponto.id, my_schema.dados_usuario.nome,DATE_FORMAT(\`data\`, "%d/%m/%Y") as \`data\`, inicio, fim, cc, my_schema.ponto.cargo, modalidade, atividade, my_schema.permissao_usuarios.id_usuario, aprovacao FROM my_schema.ponto
								LEFT JOIN my_schema.users ON my_schema.users.id = my_schema.ponto.usuario
								LEFT JOIN my_schema.dados_usuario ON my_schema.dados_usuario.username = my_schema.users.username
								LEFT JOIN my_schema.permissao_usuarios ON my_schema.permissao_usuarios.id_usuario = my_schema.ponto.usuario OR my_schema.permissao_usuarios.id_usuario_visto = my_schema.ponto.usuario
								WHERE my_schema.permissao_usuarios.id_usuario = ?`;

	            connection.query(insertQuery, [req.user.id], function(err, recordset){
	                if (err) console.log(err)

		            // send records as a response
		            res.send(recordset);

	        });
        }

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/dados_user', isLoggedIn, function(req, res) {

        var insertQuery = "SELECT * FROM my_schema.dados_usuario LEFT JOIN my_schema.users ON my_schema.users.username = my_schema.dados_usuario.nome where id = ?";
            connection.query(insertQuery, [req.user.id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/list_users', isLoggedIn, function(req, res) {

        var insertQuery = `SELECT nome, id FROM my_schema.dados_usuario 
							LEFT JOIN my_schema.users ON my_schema.users.username = my_schema.dados_usuario.username`;
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});


	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/list_permissions', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = `SELECT id, nome, 
							IF(ISNULL((SELECT id_usuario FROM my_schema.permissao_usuarios 
							          WHERE (my_schema.permissao_usuarios.id_usuario_visto = id 
							          AND my_schema.permissao_usuarios.id_usuario = ?) 
							          )), "Negado", "Permitido") AS Acesso FROM my_schema.dados_usuario
							LEFT JOIN my_schema.users ON my_schema.users.username = my_schema.dados_usuario.username
							WHERE NOT(id=?)`;
            connection.query(insertQuery,[url_id, url_id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/list_permissions_pages', isLoggedIn, function(req, res) {

		url_id = req.param('id');

        var insertQuery = `SELECT paginas, IF(ISNULL(nome_pagina),"Negado","Permitido") As Acesso FROM my_schema.paginas 
							LEFT JOIN my_schema.acesso_paginas 
							ON my_schema.paginas.paginas = my_schema.acesso_paginas.nome_pagina AND id_usuario = ?`;
            connection.query(insertQuery,[url_id], function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/registro_projetos', isLoggedIn, function(req, res) {

        var insertQuery = "SELECT * FROM my_schema.projetos ORDER BY cc DESC";
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/info_projeto', isLoggedIn, function(req, res) {

        url_projeto = req.param('projeto');

        var insertQuery = "SELECT * FROM my_schema.projetos WHERE cc = ?";
        connection.query(insertQuery, [url_projeto], function(err, recordset){
            if (err) console.log(err)

            // send records as a response
            res.send(recordset);

        });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/list_users', isLoggedIn, function(req, res) {

        var insertQuery = "SELECT nome FROM dados_usuario";
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Power Monitor      ==================
	// =====================================
	app.get('/power_monitor', isLoggedIn, function(req, res) {

        var insertQuery = "SELECT * FROM my_schema.power_monitor ORDER BY interacao DESC LIMIT 1;";
            connection.query(insertQuery, function(err, recordset){
                if (err) console.log(err)

	            // send records as a response
	            res.send(recordset);

            });

	});

	// =====================================
	// Cadastra atividade ==================
	// =====================================
	app.get('/get_cnpj', isLoggedIn, function(req, res) {
		var url_cnpj = req.param('cnpj');

		console.log('##############################################');
		console.log(url_cnpj);

        cnpj.consultaCNPJ({cnpj: url_cnpj })
		.then(result => {
		    console.log(result)
		    res.send(result)
		})
		.catch(error => {
		    console.log(error)
		});
	});

	//====================================================================================================================================
	//Route: Json from python
	app.get('/python', (req, res) => {

		var largeDataSet = [];
		 // spawn new child process to call the python script
		 const python = spawn('python', ['./python/tempo_real.py']);
		 // collect data from script
		 python.stdout.on('data', function (data) {
		  console.log('Pipe data from python script ...');
		  largeDataSet.push(data);
		 });
		 // in close event we are sure that stream is from child process is closed
		 python.on('close', (code) => {
		 console.log(`child process close all stdio with code ${code}`);
		 // send data to browser
		 res.send(largeDataSet.join(""))

		 //var id = req.body.IPadress
		 //console.log('value: ' + id);
		 console.log(largeDataSet);
		 return;

	 		});
	});

	app.all('/express-flash', (req, res ) => {
	  req.flash('success', 'This is a flash message using the express-flash module.');
	  res.redirect(301, '/');
	});

};


// route middleware to make sure
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
}

//Verifica se o usuário tem acesso à página requisitada
function check_acess (req, res, page_name) {

	var insertQuery = "SELECT * FROM my_schema.acesso_paginas WHERE id_usuario = ? AND nome_pagina = ?";
    connection.query(insertQuery, [req.user.id, page_name], function(err, recordset){

        if (err) {
        	res.redirect('/acesso_negado');
        	}
        	else {
        		if (recordset.length) {
        			console.log("CCCCCCCCCCCCC");
	        		return true;
        		}
        		else
        		{
	        		res.redirect('/acesso_negado');
	        	}
        	} 
    });
    
}
