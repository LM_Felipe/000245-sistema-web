

var mysql = require('mysql');
var dbconfig = require('../config/database');

var connection = mysql.createConnection(dbconfig.connection);

connection.query('CREATE TABLE `' + dbconfig.database + '`.`acesso_paginas` (`id_usuario` int DEFAULT NULL,`nome_pagina` varchar(45) DEFAULT NULL) ');

connection.query('CREATE TABLE `' + dbconfig.database + '`.`corrente` (`Corrente_L1` bigint DEFAULT NULL,`Corrente_L2` bigint DEFAULT NULL,`Corrente_L3` bigint DEFAULT NULL)');

connection.query('CREATE TABLE `' + dbconfig.database + '`.`dados_usuario` (`username` varchar(20) DEFAULT NULL,`nome` text,`email` text,`cargo` text,`telefone` text,`contrato` text)');

connection.query('CREATE TABLE `' + dbconfig.database + '`.`deslocamentos` (`id` int NOT NULL AUTO_INCREMENT,`data` varchar(45) DEFAULT NULL,`carro` varchar(45) DEFAULT NULL,`placa` varchar(45) DEFAULT NULL,`nome` text,`cc` varchar(45) DEFAULT NULL,`descricao` text,`km` int DEFAULT NULL,`caminho_imagem` text,`numero_telefone` varchar(45) DEFAULT NULL,`tipo` varchar(45) DEFAULT NULL,PRIMARY KEY (`id`))');

connection.query('CREATE TABLE `' + dbconfig.database + '`.`marcacao` (`quem_marcou` bigint DEFAULT NULL,`id` bigint DEFAULT NULL,`data` varchar(45) DEFAULT NULL,`inicio` time DEFAULT NULL,`fim` time DEFAULT NULL,`cc` varchar(45) DEFAULT NULL,`modalidade` varchar(45) DEFAULT NULL,`local` varchar(45) DEFAULT NULL,`etapa` varchar(45) DEFAULT NULL)');

connection.query('CREATE TABLE `' + dbconfig.database + '`.`paginas` (`paginas` varchar(45) DEFAULT NULL)');

connection.query('CREATE TABLE `' + dbconfig.database + '`.`permissao_usuarios` (`id_usuario` int DEFAULT NULL,`id_usuario_visto` int DEFAULT NULL)');

connection.query('CREATE TABLE `' + dbconfig.database + '`.`ponto` (`data` varchar(45) DEFAULT NULL,`inicio` time DEFAULT NULL,`fim` time DEFAULT NULL,`cc` varchar(45) DEFAULT NULL,`cargo` varchar(45) DEFAULT NULL,`modalidade` varchar(45) DEFAULT NULL,`local` varchar(45) DEFAULT NULL,`atividade` longtext,`usuario` varchar(45) DEFAULT NULL,`id` bigint NOT NULL AUTO_INCREMENT,`aprovacao` varchar(45) DEFAULT NULL,`etapa` varchar(45) DEFAULT NULL,PRIMARY KEY (`id`))');

connection.query('CREATE TABLE `' + dbconfig.database + '`.`projetos` (`nome_lm` varchar(200) DEFAULT NULL,`nome_cliente` varchar(200) DEFAULT NULL,`email` varchar(45) DEFAULT NULL,`telefone_cliente` varchar(45) DEFAULT NULL,`cnpj` varchar(45) DEFAULT NULL,`nome_empresa` varchar(200) DEFAULT NULL,`cep` varchar(45) DEFAULT NULL,`cidade` varchar(45) DEFAULT NULL,`estado` varchar(45) DEFAULT NULL,`pais` varchar(45) DEFAULT NULL,`tipo_projeto` varchar(45) DEFAULT NULL,`olm` varchar(100) DEFAULT NULL,`cc` varchar(45) NOT NULL,`nome_obra` varchar(200) DEFAULT NULL,`planejamento_programacao` int DEFAULT NULL,`instalacao_programacao` int DEFAULT NULL,`comissionamento_programacao` int DEFAULT NULL,`startup_programacao` int DEFAULT NULL,`acompanhamento_programacao` int DEFAULT NULL,`documentacao_programacao` int DEFAULT NULL,`planejamento_eletrica` int DEFAULT NULL,`instalacao_eletrica` int DEFAULT NULL,`comissionamento_eletrica` int DEFAULT NULL,`startup_eletrica` int DEFAULT NULL,`acompanhamento_eletrica` int DEFAULT NULL,`documentacao_eletrica` int DEFAULT NULL,`data_pedido` varchar(45) DEFAULT NULL,`valor_pedido` int DEFAULT NULL,PRIMARY KEY (`cc`))');

connection.query('CREATE TABLE `' + dbconfig.database + '`.`abastecimento` (`id` int NOT NULL AUTO_INCREMENT,`valor` float DEFAULT NULL,`cnpj` varchar(45) DEFAULT NULL,`cc` int DEFAULT NULL,`data` varchar(45) DEFAULT NULL,`cartao` text,`litros` float DEFAULT NULL,`km` int DEFAULT NULL,`numero_telefone` varchar(45) DEFAULT NULL,`caminho_imagem` text,`status` text,`responsavel_dif` int DEFAULT NULL,PRIMARY KEY (`id`));');

connection.query('CREATE TABLE `' + dbconfig.database + '`.`veiculos` (`id` int NOT NULL AUTO_INCREMENT,`marca` text,`modelo` text,`placa` varchar(45) DEFAULT NULL,`km_oleo` int DEFAULT NULL,`data_oleo` varchar(45) DEFAULT NULL,`data_lic` varchar(45) DEFAULT NULL,`data_seg` varchar(45) DEFAULT NULL,`km_revisao` int DEFAULT NULL,`data_revisao` varchar(45) DEFAULT NULL,PRIMARY KEY (`id`));');

connection.query('CREATE TABLE `' + dbconfig.database + '`.`notinhas` (`id` int NOT NULL AUTO_INCREMENT,`valor` float DEFAULT NULL,`cnpj` varchar(45) DEFAULT NULL,`cc` int DEFAULT NULL,`data` varchar(45) DEFAULT NULL,`cartao` text,`descricao` text,`numero_telefone` varchar(45) DEFAULT NULL,`caminho_imagem` text,`status` text,`responsavel_dif` int DEFAULT NULL,PRIMARY KEY (`id`));');