from pycomm3 import LogixDriver
import mysql.connector
import time
import sys
from datetime import datetime
import datetime as tempo

while True:
	ini = datetime.now()
	try:
		with LogixDriver('129.186.0.230') as plc:
			try:
				SARR_Conta=plc.read('SARR_L_Conta','SARR_E_Conta')

				sarr_l_conta = SARR_Conta[0][1]
				sarr_e_conta = SARR_Conta[1][1]

			except Exception as e:
				now = datetime.now()
				arquivo = open('C:/Ponto/python/falha.txt', 'a')
				arquivo.write('Falha de leitura: '+str(now)+'\n')
				arquivo.close()			

			if sarr_l_conta != sarr_e_conta:
				plc.write(('SARR_E_Conta',int(sarr_l_conta)))

				SARR_Conta=plc.read('SARR_L_Conta','SARR_E_Conta')

				sarr_l_conta = SARR_Conta[0][1]
				sarr_e_conta = SARR_Conta[1][1]
				
				try:
					mydb = mysql.connector.connect(
						host="45.7.171.172",
						port=1940,
						autocommit=True,
						user="admin",
						password="m%Le$3xA",
						database="my_schema"
					)
					mycursor = mydb.cursor()
					statement = '''INSERT INTO lifecounter_clp_sarr (sarr_l_conta,sarr_e_conta) 
								values ('''+str(sarr_l_conta)+''','''+str(sarr_e_conta)+''')'''
					mycursor.execute(statement)
					mycursor.close()
					mydb.close()

				except mysql.connector.Error as err:
			  		print("Something went wrong: {}".format(err))
	except Exception as e:
		now = datetime.now()
		arquivo = open('C:/Ponto/python/falha.txt', 'a')
		arquivo.write('Falha de comunicação: '+str(now)+'\nFalha: '+e+'\n')
		arquivo.close()

	dif = tempo.timedelta(0,3,0)
	fim = datetime.now()
	if (fim - ini) > dif:
		arquivo = open('C:/Ponto/python/falha.txt', 'a')
		arquivo.write('Tempo: '+str(fim - ini)+' ('+str(datetime.now())+')\n')
		arquivo.close()