
from pycomm3 import LogixDriver
import time
import sys

#Conecta com PLC e altera valor da TAG
with LogixDriver('129.186.0.230') as plc:

    timeout = sys.argv[1]

    # Escreve na tag do CLP o tempo de timeout definido pelo SARR
    plc.write(('SARR_Timer_Comunica_Valor',int(timeout)))

    print(timeout)

