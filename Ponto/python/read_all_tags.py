'''
the following import is only necessary because eip.py is not in this directory
'''
import sys
sys.path.append('..')

'''
Get the tag list from the PLC
This will fetch all the controller and program
scoped tags from the PLC.	In the case of
Structs (UDT's), it will not give you the makeup
of each	tag, just main tag names.
'''

from pylogix import PLC

#print('First param:'+sys.argv[1]+'#')

with PLC() as comm:
	comm.IPAddress = '192.168.1.115'
	comm.ProcessorSlot = 2
	tags = comm.GetTagList()

	first = 1

	JsonString = '{"data":['
			
	for t in tags.Value:
		if t.DataType != '':
			if first:
				JsonString = JsonString + '{"IP":"'+ str(sys.argv[1]) + '","Slot":"'+ str(comm.ProcessorSlot) + '","TagName":"' + t.TagName + '", "TagType":"'+ t.DataType +'"}'
				first = 0
			else:
				JsonString = JsonString + ',{"IP":"'+ str(sys.argv[1]) + '","Slot":"'+ str(comm.ProcessorSlot) + '","TagName":"' + t.TagName + '", "TagType":"'+ t.DataType +'"}'

	#Imprime no console em json os resultados
	JsonString = JsonString + ']}'
	print(JsonString)

