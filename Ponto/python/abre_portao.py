from pycomm3 import LogixDriver
import time
import sys

#Conecta com PLC e altera valor da TAG
with LogixDriver('129.186.0.230') as plc:

    #escreve a TAG conforme formulário na página.
    plc.write(('SARR_E_Acesso.1',1))