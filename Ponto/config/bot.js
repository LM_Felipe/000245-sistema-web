module.exports = function(bot) {
	const fs = require('fs');
	const fs2 = require('fs');
	const regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


	let date_ob = new Date();
	let date = 0;
	let month = 0;
	let year = 0;
	let hours = 0;
	let minutes = 0;
	let seconds = 0;

	let CNPJ = require('consulta-cnpj-ws');
	let cnpj = new CNPJ();

	//Mysql connect
	var mysql = require('mysql');
	var dbconfig = require('../config/database');
	//connection = mysql.createConnection(dbconfig.connection);

	//, função que reconecta o server em caso de queda. 
	function handleDisconnect() {
	  connection = mysql.createConnection(dbconfig.connection); // Recreate the connection, since
	                                                  // the old one cannot be reused.

	  connection.connect(function(err) {              // The server is either down
	    if(err) {                                     // or restarting (takes a while sometimes).
	      console.log('error when connecting to db:', err);
	      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
	    }                                     // to avoid a hot loop, and to allow our node script to
	  });                                     // process asynchronous requests in the meantime.
	                                          // If you're also serving http, display a 503 error.
	  connection.on('error', function(err) {
	    console.log('db error', err);
	    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
	      handleDisconnect();                         // lost due to either server restart, or a
	    } else {                                      // connnection idle timeout (the wait_timeout
	      throw err;                                  // server variable configures this)
	    }
	  });
	}

	handleDisconnect();
	connection.query('USE ' + dbconfig.database);

	//usado para fazer download das imagens enviadas ao bot. 
	const axios = require('axios'); 


	bot.start(function(ctx){
		var select_query = "select * from my_schema.dados_usuario where id_telegram=?";
        connection.query(select_query,[ctx.message.chat.id], function(err, recordset){
            if (err) {
                ctx.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
            } else {
            	if (recordset.length) {
		        	ctx.reply('Olá '+recordset[0]['username']+', bem vindo(a) ao SARR');
		    	}
		    	else{
		    		ctx.reply('Olá '+ctx.message['from']['first_name']+', bem vindo(a) ao SARR! \nInforme seu e-mail para validação');
		    	}	
	        }
    	});	
	}) 
	
	bot.launch() // start
	/*bot.hears('hi', function(ctx){
		console.log(ctx);
		ctx.reply('Hey there');
	}); // listen and handle when user type hi text
	*/


	bot.on('message', async function(ctx){
		await ctx.forwardMessage(-571036896).catch(function(e){
		    console.log(e);
		});
		console.log(ctx);
		if (ctx.message['text']) {

			if (ctx.message['text'].toUpperCase().startsWith('APAGAR_NOTA:')) {
				let msg_user = 'Olá ' + ctx.message['from']['first_name'] +', sou SARR.\n'

		        id = ctx.message['text'].slice(13);

		        var insertQuery = "UPDATE `my_schema`.`notinhas` SET `status` = \"Apagada\" WHERE `id` = ?;";
	            connection.query(insertQuery,[id], function(err, recordset){
	                if (err) {
                        ctx.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                    } else {
                        ctx.reply('O registro id: ' + id + '\n\nFoi apagado com sucesso.');
                    }
            	});
			}
			else if(ctx.message['text'].toUpperCase().startsWith('APAGAR_DESLOCAMENTO:')){
				let msg_user = 'Olá ' + ctx.message['from']['first_name'] +', sou SARR.\n'

		        id = ctx.message['text'].slice(21);

		        var insertQuery = "UPDATE `my_schema`.`deslocamentos` SET `status` = \"Apagada\" WHERE `id` = ?;";
	            connection.query(insertQuery,[id], function(err, recordset){
	                if (err) {
                        ctx.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                    } else {
                        ctx.reply('O registro id: ' + id + '\n\nFoi apagado com sucesso.');
                    }
	            });
			}
			else if(ctx.message['text'].toUpperCase().startsWith('APAGAR_ABASTECIMENTO:')){
				let msg_user = 'Olá ' + ctx.message['from']['first_name'] +', sou SARR.\n'

		        id = ctx.message['text'].slice(22);

		        var insertQuery = "UPDATE `my_schema`.`abastecimento` SET `status` = 'Apagada' WHERE `id` = ?;";
	            connection.query(insertQuery,[id], function(err, recordset){
	                if (err) {
                        ctx.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                    } else {
                        ctx.reply('O registro id: ' + id + '\n\nFoi apagado com sucesso.');
                    }
	            });

			}
			else if(ctx.message['text'].toUpperCase().startsWith('PLACA:')){
				let msg_user = 'Olá ' + ctx.message['from']['first_name'] +', sou SARR.\n'

		        carro = ctx.message['text'].slice(7);

		        var insertQuery = 'SELECT marca, modelo, placa, km_oleo, DATE_FORMAT(\`data_oleo\`, "%d/%m/%Y") as \`data_oleo\`,DATE_FORMAT(\`data_lic\`, "%d/%m/%Y") as \`data_lic\`,DATE_FORMAT(\`data_seg\`, "%d/%m/%Y") as \`data_seg\`, km_revisao,DATE_FORMAT(\`data_revisao\`, "%d/%m/%Y") as \`data_revisao\` FROM my_schema.veiculos WHERE modelo LIKE \"%' + carro + '%\"';
	            connection.query(insertQuery, function(err, info_carros){
	                if (err) {
                        ctx.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                    } else {
                        lista_carros = "";
                        var i;
                        for (i in info_carros) {
                            lista_carros = lista_carros + "\nVeículo: " +  info_carros[i].marca + " - " + info_carros[i].modelo + " \n - placa: " + info_carros[i].placa + "\n - KM Oleo: " + info_carros[i].km_oleo + "\n - Data Oleo: " + info_carros[i].data_oleo + "\n - Data licenciamento: " + info_carros[i].data_lic + "\n - Data seguro: " + info_carros[i].data_seg + "\n - KM Revisão: " + info_carros[i].km_revisao + "\n - Data revisão: " + info_carros[i].data_revisao+'\n';
                        }

                        if (lista_carros == "") {lista_carros = lista_carros + "\nEste modelo não está cadastrado.";}

                        ctx.reply(msg_user + lista_carros);   
                    }
	            });
			}
			else if (regexEmail.test(ctx.message['text'])) {
				var select_query = "select * from my_schema.dados_usuario where id_telegram=?";
		        connection.query(select_query,[ctx.message.chat.id], function(err, recordset){
		            if (err) {
		                ctx.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
		            } 
		            else {
		            	if (recordset.length) {
				        	ctx.reply('Este chat já está registrado para '+recordset[0]['username']);
				    	}
				    	else{
				    		var updateQuery = 'update my_schema.dados_usuario set id_telegram=? where email=?';
							connection.query(updateQuery,[ctx.message.chat.id,ctx.message['text']], function(err, recordset){
				                if (err) {
				                        ctx.reply('Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
				                    } 
				                else {
				                	console.log(recordset);
			                        if(recordset['affectedRows']){
			                        	var select_query = "select * from my_schema.dados_usuario where email=?";
								        connection.query(select_query,[ctx.message['text']], function(err, recordset){
								        	var numero = recordset[0]['telefone'];
								            if (err) {
								                ctx.reply('Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
								            } 
								            else {
								            	if (recordset.length) {
								            		updateQuery = 'update notinhas set chat_id=? where numero_telefone=?';
								            		connection.query(updateQuery,[ctx.message.chat.id,ctx.message['from']['first_name'],numero]);
										        	ctx.reply('Este chat agora está registrado para '+recordset[0]['username']+'.');
										    	}
									        }
								    	});	
			                        }
			                        else{
							    		ctx.reply('Não encontrei nenhum usuário com este email, tente novamente');
							    	}	
			                    }
				            });
				    	}	
			        }
		    	});	
			}
			else{

				var ajuda= 'Olá '+ctx.message['from']['first_name'];
				ajuda = ajuda +',\n\nVocê está tentando registrar um deslocamento, despesa ou abastecimento?';
				ajuda = ajuda + '\n\n Você deve enviar a imagem a ser registrada e informar os dados na legenda, seguindo os modelos abaixo:\n\n';

				await ctx.reply(ajuda,{'reply_to_message_id':ctx.message['message_id']});

				await ctx.reply('Despesa:');
				ajuda ='Desp 99,99 (valor) | CNPJ 99.99.999/9999-99 |  CC000XXX | AnoMêsDia |Cartão Final XXXX | Descrição\n\n';
				await ctx.reply(ajuda);

				ajuda = 'KMF/KMI XXXXXX | 04.580.206/0001-99 |  CC000XXX | AnoMêsDia | placa |  Descrição\n\n';
				await ctx.reply('Deslocamento:');
				await ctx.reply(ajuda);
				
				ajuda = 'abas 99,99 (valor) | 99.99.999/9999-99 | CC000XXX | AnoMêsDia | cartao final XXXX | 99,99 (litros) | KM | placa\n\n';
				await ctx.reply('abastecimento');
				await ctx.reply(ajuda);
			}
		
		}
		else if (ctx.message['photo']){
			if (ctx.message['caption']) {

				if (ctx.message['caption'].toUpperCase().startsWith('DESP')) {
					despesa(ctx);
				}
				else if (ctx.message['caption'].toUpperCase().startsWith('KMI') || ctx.message['caption'].toUpperCase().startsWith('KMF')) {
					deslocamento(ctx);
				}
				else if (ctx.message['caption'].toUpperCase().startsWith('ABAS')) {
					abastecimento(ctx);
				}
				else{
					ctx.reply('Não consegui identificar um padrão na sua legenda, você poderia tentar novamente? Se tiver dúvidas quanto ao padrão, envie qualquer palavra que enviarei os padrões');
				}
			}
			else{
				ctx.reply('Você esqueceu da legenda, tente novamente!');
			}

		}
		else if (ctx.message['document']){
			ctx.reply('Você enviou um documento, tente novamente com uma imagem!');
		}

	});
























	function deslocamento(ctx){

	    var autor = ctx.message['from']['first_name']+' '+ctx.message['from']['last_name'];

	     //let msg_user = 'Olá ' + autor +'\n\n'
	    var msg_user = 'Olá ' + autor +', sou SARR.\n'

	    var mesagem_divida = ctx.message['caption'].split("|");

	    //Verifica se o texto está certo
	    if (((mesagem_divida.length - 1) == 5) || ((mesagem_divida.length - 1) == 6) || ((mesagem_divida.length - 1) == 7)) {

	        var km = mesagem_divida[0].replace(/\D/g,'');
	        var cnpj = mesagem_divida[1].replace(/\D/g,'');
	        var cc = mesagem_divida[2].replace(/\D/g,'');
	        var data = mesagem_divida[3].replace(/\D/g,'');
	        var placa = mesagem_divida[4].replace("[^a-zA-Z0-9]", "");
	        placa = mesagem_divida[4].replace("-", "");
	        placa = placa.replace(/\s/g, '');
	        var descricao = mesagem_divida[5];
	        var responsavel_dif = 0;
	        var reembolso = 0;
	        if (((mesagem_divida.length - 1) == 6)||((mesagem_divida.length - 1) == 7)) {if (mesagem_divida[6].trim().toUpperCase().startsWith('REEMBOLSO')) { reembolso = mesagem_divida[6];} else if (mesagem_divida[6].trim().toUpperCase().startsWith('ID')) { responsavel_dif = mesagem_divida[6].replace(/\D/g,'');}}
	        if ((mesagem_divida.length - 1) == 7) {if (mesagem_divida[7].trim().toUpperCase().startsWith('REEMBOLSO')) { reembolso = mesagem_divida[7];} else if (mesagem_divida[7].trim().toUpperCase().startsWith('ID')) { responsavel_dif = mesagem_divida[7].replace(/\D/g,'');}}
	        var tipo = "kmi";
	        if (ctx.message['caption'].toUpperCase().startsWith('KMF')) {tipo = "kmf";}


	        var contem_erro = 0;
	        var erros = "";

	        data = data.slice(6, 8) + "/" + data.slice(4, 6) + "/" + data.slice(0, 4)
	        // var data_db = data.slice(6, 10) + "-" + data.slice(3, 5) + "-" + data.slice(0, 2)

	        cnpj = cnpj.slice(0, 2) + "." + cnpj.slice(2, 5) + "." + cnpj.slice(5, 8) + "/" +  cnpj.slice(8, 12) + "-" +  cnpj.slice(12, 15);
	        placa = placa.slice(0, 3) + "-" + placa.slice(3, 7)

	        //my_msg = 'Centro de custos: ' + cc + '\nData: ' + data + '\nKm: ' + km + '\nTipo: ' + tipo + '\nPlaca: ' + placa  + '\nCNPJ: ' + cnpj  + '\nDescrição: ' + descricao;
	        var my_msg = '- CC ' + cc + '\n- ' + data + '\n- ' + tipo + ' ' + km + '\n- ' + placa  + '\n- ' + cnpj  + '\n- ' + descricao;
	        if (((mesagem_divida.length - 1) == 6) && (responsavel_dif.length > 1)) {my_msg = my_msg + '\n- Deslocamento enviado para o ID: ' + responsavel_dif;}
	        if (((mesagem_divida.length - 1) == 7) && (responsavel_dif.length > 1)) {my_msg = my_msg + '\n- Deslocamento enviado para o ID: ' + responsavel_dif;}

	        if (((mesagem_divida.length - 1) == 6) && (reembolso.length > 1)) {my_msg = my_msg + '\n- Despesa enviada para : ' + reembolso;}
	        if (((mesagem_divida.length - 1) == 7) && (reembolso.length > 1)) {my_msg = my_msg + '\n- Despesa enviada para : ' + reembolso;}

	        var regex  = /[a-zA-Z]{3}-[0-9]{4}/;
	        if (regex.test(placa)) {} else {contem_erro = 1; erros = erros + "\nEsse número de placa não está no padrão correto"}
	        if ((cnpj.length) != 18 ) {contem_erro = 1; erros = erros + "\nEsse número de CNPJ não está correto"}
	        if ((cc.length) <= 1 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar um centro de custos"}
	        if ((data.length) != 10) {contem_erro = 1; erros = erros + "\nVocê não digitou uma data no formato aceitável"}
	        if ((descricao.length) <= 2 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar uma descrição do deslocamento"}
	        if (((mesagem_divida.length - 1) == 6) && (responsavel_dif.length <= 1) && (reembolso.length <= 1)) {contem_erro = 1; erros = erros + "\nNesse formato você precisa adicionar um id de responsável ou informar se é reembolso."}
	        if (((mesagem_divida.length - 1) == 7) && (responsavel_dif.length <= 1) && (reembolso.length <= 1)) {contem_erro = 1; erros = erros + "\nNesse formato você precisa adicionar um id de responsável ou informar se é reembolso."}

	        if (contem_erro) {

	    		const files = ctx.update.message.photo;
	        	fileId = files[files.length-1].file_id;

	            get_date();
	            var caminho = './public/img/deslocamento/' + ctx.message['from']['first_name'] + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';
	            var caminho_db = '/img/deslocamento/' + ctx.message['from']['first_name'] + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';

				ctx.telegram.getFileLink(fileId).then(function(url){ 
					url = url['href'];   
				    axios({url, responseType: 'stream'}).then(response => {
				        return new Promise((resolve, reject) => {
				            response.data.pipe(fs.createWriteStream(caminho));
				        });
				    }).catch(function(e){
						   console.log(e);
						})
				}).catch(function(e){
					    console.log(e);
					});

	        	var status='Erro Registro';
                descricao ='Erros: '+erros+'\n\nDados coletados:\n'+my_msg;

                var dataErro = new Date();
    
                var dd = String(dataErro.getDate()).padStart(2, '0');
                var mm = String(dataErro.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = dataErro.getFullYear();

                var dataErro = dd + '/' + mm + '/' + yyyy;



                var insertQuery = "INSERT INTO deslocamentos (data,descricao, nome, caminho_imagem, status,chat_id,msg_id) values (?,?,?,?,?,?,?)";
                connection.query(insertQuery,[dataErro,descricao,autor, caminho_db, status,,ctx.message.chat.id,ctx.message['message_id']], function(err, rows){
                if (err) {
                    ctx.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err,{'reply_to_message_id':ctx.message['message_id']});
                }
                else 
                {
                    var insertQuery = "SELECT * FROM my_schema.deslocamentos ORDER BY id DESC LIMIT 1;";
                    var info_estabelecimento = ""

                    connection.query(insertQuery, function(err, recordset){

                        //ctx.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + recordset[0].id + '\n\nId do registro: ' + recordset[0].id);
                        ctx.replyWithMarkdown(msg_user + 'Registro com Erro:\n' + erros + '\n\nFavor corrigir!\n\nPara apagar envie *Apagar_nota: ' + recordset[0].id + '*',{'reply_to_message_id':ctx.message['message_id']});
                    });
                }});

	        }
	        else {

	    		const files = ctx.update.message.photo;
	        	fileId = files[files.length-1].file_id;

	            get_date();
	            var caminho = './public/img/deslocamento/' + ctx.message['from']['first_name'] + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';
	            var caminho_db = '/img/deslocamento/' + ctx.message['from']['first_name'] + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';

				ctx.telegram.getFileLink(fileId).then(function(url){ 
					url = url['href'];   
				    axios({url, responseType: 'stream'}).then(response => {
				        return new Promise((resolve, reject) => {
				            response.data.pipe(fs.createWriteStream(caminho));
				        });
				    }).catch(function(e){
						    console.log(e);
						})
				}).catch(function(e){
					    console.log(e);
					});




	            var insertQuery = "INSERT INTO deslocamentos ( km, cnpj, cc, data, placa, descricao, nome, caminho_imagem, status, responsavel_dif, tipo, reembolso,chat_id, msg_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	            connection.query(insertQuery,[km, cnpj, cc, data, placa, descricao, autor, caminho_db, 'Aberto', responsavel_dif, tipo, reembolso,ctx.message.chat.id,ctx.message['message_id'],ctx.message.date], function(err, rows){
	            if (err) {
	                ctx.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err,{'reply_to_message_id':ctx.message['message_id']});
	            }
	            else 
	            {
	                var insertQuery = "SELECT * FROM my_schema.deslocamentos ORDER BY id DESC LIMIT 1;";
	                connection.query(insertQuery, function(err, recordset){
	                    var db_id = recordset[0].id;

	                    var psq_tipo = "kmi";
	                    if (tipo == "kmi" ) {psq_tipo = "kmf";}
	                    var insertQuery = "SELECT * FROM my_schema.deslocamentos WHERE tipo = ? AND placa = ? ORDER BY id DESC LIMIT 1;";
	                    connection.query(insertQuery,[psq_tipo,placa], function(err, info){ 
	                        if (info.length) {
	                        var ultimo_km = info[0].km;

	                        var insertQuery = "SELECT * FROM my_schema.veiculos WHERE placa = ?;";
	                        connection.query(insertQuery,[placa], function(err, data){
	                            if (data.length) {
	                               var veiculo = data[0].marca + " - " + data[0].modelo 
	                                var avisos = "\n\nAvisos:"
	                                var tem_aviso = 0;
	                                if ((tipo == "kmi")&&(km != ultimo_km)) { tem_aviso = 1; avisos = avisos + "\nO quilometro incial não bate com o último registro: " + ultimo_km;}
	                                if ((tipo == "kmf")&&(km <= ultimo_km)) { tem_aviso = 1; avisos = avisos + "\nO quilometro final é menor que o inicial: " + ultimo_km;}
	                                if (km >= data[0].km_oleo) { tem_aviso = 1; avisos = avisos + "\nO oleo já passou da quilometragem: Km " + data[0].km_oleo }
	                                if (data >= parseInt(data[0].data_oleo)) { tem_aviso = 1; avisos = avisos + "\nO oleo já passou da validade: " + data[0].data_oleo }
	                                if (data >= parseInt(data[0].data_lic)) { tem_aviso = 1; avisos = avisos + "\nO licenciamento está vencido: " + data[0].data_lic }
	                                if (data >= parseInt(data[0].data_seg)) { tem_aviso = 1; avisos = avisos + "\nO seguro está vencido: " + data[0].data_seg}
	                                if ((data >= parseInt(data[0].data_revisao))||(km >= data[0].km_revisao)) { tem_aviso = 1; avisos = avisos + "\nO veículo precisa ser revisado: " + data[0].data_revisao + " ou Km " + data[0].km_revisao}

	                                if (tem_aviso == 0) {avisos = "";}
	                                
	                                //ctx.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + veiculo + "\n" + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + db_id + '\n\nId do registro: ' + db_id + avisos);
	                                ctx.replyWithMarkdown(msg_user + 'Registrei com sucesso:\n- ' + veiculo + "\n" + my_msg + '\n\nPara apagar envie *Apagar_deslocamento: ' + db_id + '*' + avisos,{'reply_to_message_id':ctx.message['message_id']});
	                            } else { 
	                                var avisos = "\n\nAviso:\nEste veículo não está cadastrado."
	                                //ctx.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + "\n" + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + db_id + '\n\nId do registro: ' + db_id + avisos);
	                                ctx.replyWithMarkdown(msg_user + 'Registrei com sucesso:' + "\n" + my_msg + '\n\nPara apagar envie *Apagar_deslocamento: ' + db_id + '*' + avisos,{'reply_to_message_id':ctx.message['message_id']});
	                                }
	                        });
	                    } else {
	                        var insertQuery = "SELECT * FROM my_schema.deslocamentos ORDER BY id DESC LIMIT 1;";
	                        connection.query(insertQuery, function(err, recordset){
	                            var avisos = "\n\nAviso:\nPrimeiro registro deste veículo."
	                            var db_id = recordset[0].id;
	                            ctx.replyWithMarkdown(msg_user + 'Registrei com sucesso:' + "\n" + my_msg + '\n\nPara apagar envie *Apagar_deslocamento: ' + db_id + '*' + avisos,{'reply_to_message_id':ctx.message['message_id']});
	                        });
	                    }});
	                    
	                });
	            }});
	        }
	    } else {
	        var dataErro = new Date();
	        var descricao = 'A mensagem não está no padrão correto, dados coletados: \n\n' + ctx.message['caption'];
	        var dd = String(dataErro.getDate()).padStart(2, '0');
	        var mm = String(dataErro.getMonth() + 1).padStart(2, '0'); //January is 0!
	        var yyyy = dataErro.getFullYear();

	        var dataErro = dd + '/' + mm + '/' + yyyy;

	    	const files = ctx.update.message.photo;
        	fileId = files[files.length-1].file_id;

            get_date();
            var caminho = './public/img/deslocamento/' + ctx.message['from']['first_name'] + '_' +  + dataErro.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';
            var caminho_db = '/img/deslocamento/' + ctx.message['from']['first_name'] + '_' +  + dataErro.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';
            

            ctx.telegram.getFileLink(fileId).then(function(url){ 
				url = url['href'];   
			    axios({url, responseType: 'stream'}).then(response => {
			        return new Promise((resolve, reject) => {
			            response.data.pipe(fs.createWriteStream(caminho));
	                });
	            }).catch(function(e){
						console.log(e);
					})
			}).catch(function(e){
					console.log(e);
				});
	        var insertQuery = "INSERT INTO deslocamentos (descricao,nome, data,status,chat_id,msg_id,caminho_imagem) values (?,?,?,?,?,?,?)";
	        connection.query(insertQuery,[descricao, autor,String(dataErro),'Erro Registro',ctx.message.chat.id,ctx.message['message_id'],caminho_db], function(err, rows){
	            if (err) {
	                console.log(err);
	                ctx.reply(msg_user + 'A mensagem não está no padrão correto !',{'reply_to_message_id':ctx.message['message_id']});
	            }
	            else{
	                var insertQuery = "SELECT * FROM my_schema.deslocamentos ORDER BY id DESC LIMIT 1;";
	                connection.query(insertQuery, function(err, recordset){

	                     ctx.replyWithMarkdown( msg_user + '\n\nA mensagem não está no padrão correto !' + "\n\nPara apagar envie *Apagar_nota: "+recordset[0].id+ '*',{'reply_to_message_id':ctx.message['message_id']});
	                });
	            }
	        });
	    }  
	}





	function despesa(ctx){


		autor = ctx.message['from']['first_name']+' '+ctx.message['from']['last_name'];
	    //let msg_user = 'Olá ' + autor +'\n\n'
	    let msg_user = 'Olá ' + ctx.message['from']['first_name'] +', sou SARR.\n'

	    var mesagem_divida = ctx.message['caption'].split("|");

	    //Verifica se o texto está certo
	    if (((mesagem_divida.length - 1) == 5) || ((mesagem_divida.length - 1) == 6)) {

	        var valor = mesagem_divida[0].replace(/[^0-9,.]/g, '');
	        var my_cnpj = mesagem_divida[1].replace(/\D/g,'');
	        var cc = mesagem_divida[2].replace(/\D/g,'');
	        var data = mesagem_divida[3].replace(/\D/g,'');
	        var cartao = mesagem_divida[4];

	        var descricao = mesagem_divida[5];
	        var responsavel_dif = 0;
	        if ((mesagem_divida.length - 1) == 6) {responsavel_dif = mesagem_divida[6].replace(/\D/g,'');}
	        var tipo_gasto = "Cartão"
	        if (mesagem_divida[4].toUpperCase().trim().startsWith('CAIXA ')) {tipo_gasto = "Caixa";}

	        var contem_erro = 0;
	        var erros = "";

	        data = data.slice(6, 8) + "/" + data.slice(4, 6) + "/" + data.slice(0, 4)
	        // var data_db = data.slice(6, 10) + "-" + data.slice(3, 5) + "-" + data.slice(0, 2)

	        my_cnpj = my_cnpj.slice(0, 2) + "." + my_cnpj.slice(2, 5) + "." + my_cnpj.slice(5, 8) + "/" +  my_cnpj.slice(8, 12) + "-" +  my_cnpj.slice(12, 15);

	        if (cartao.toUpperCase().trim().startsWith('CARTÃO')) { cartao = "Cartão: " + cartao.replace(/\D/g,'');} 

	        //my_msg = 'Centro de custos: ' + cc + '\nData: ' + data + '\nValor: ' + valor + '\nCNPJ: ' + cnpj + '\nCartão: ' + cartao + '\nDescrição: ' + descricao;
	        var my_msg = '- CC ' + cc + '\n- ' + data + '\n- R$' + valor + '\n- ' + my_cnpj + '\n- ' + cartao + '\n- ' + descricao;
	        if ((mesagem_divida.length - 1) == 6) {my_msg = my_msg + '\nCusto enviado para o ID: ' + responsavel_dif;}

	        var regex  = /^[0-9]{1,}([,.][0-9]{1,2})?$/;
	        if (regex.test(valor)) {valor = valor.replace(",",".")} else {contem_erro = 1; erros = erros + "\nEsse valor de gasto não está no padrão correto"}
	        if ((my_cnpj.length) != 18 ) {contem_erro = 1; erros = erros + "\nEsse número de CNPJ não está correto"}
	        if ((cc.length) <= 1 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar um centro de custos"}
	        if ((data.length) != 10) {contem_erro = 1; erros = erros + "\nVocê não digitou uma data no formato aceitável"}
	        if (!(mesagem_divida[4].toUpperCase().trim().startsWith('CARTAO') || mesagem_divida[4].toUpperCase().trim().startsWith('CARTÃO') || mesagem_divida[4].toUpperCase().trim().startsWith('CAIXA'))) {contem_erro = 1; erros = erros + "\nDeve ser informado se é cartão ou caixa";}
	        if ((descricao.length) <= 2 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar uma descrição do gasto"}
	        if (((mesagem_divida.length - 1) == 6) && ((responsavel_dif.length) <= 1)) {contem_erro = 1; erros = erros + "\nNesse formato você precisa adicionar um id de responsável."}

	        //Verifica se uma imagem foi enviada
	        if(ctx.update.message.photo) {
	        	//pegar fileID da maior foto
	        	const files = ctx.update.message.photo;
	        	fileId = files[files.length-1].file_id

                get_date()
                var caminho = './public/img/notas/' + ctx.message['from']['first_name'] + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';
                var caminho_db = '/img/notas/' + ctx.message['from']['first_name'] + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';

				ctx.telegram.getFileLink(fileId).then(function(url){ 
					url = url['href'];   
				    axios({url, responseType: 'stream'}).then(response => {
				        return new Promise((resolve, reject) => {
			            	response.data.pipe(fs.createWriteStream(caminho));
		                });
		            }).catch(function(e){
						    console.log(e);
						})
				}).catch(function(e){
					    console.log(e);
					});

                /*fs.writeFile(caminho, media.data, 'base64' , function(err) { 
                    if (err) {
                    get_date()
                    console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - Error 02');
                    console.log(err); }});*/

                if (contem_erro) {
                    var status='Erro Registro';
                    descricao ='Erros: '+erros+'\n\nDados coletados:\n'+my_msg;

                    var dataErro = new Date();
        
                    var dd = String(dataErro.getDate()).padStart(2, '0');
                    var mm = String(dataErro.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var yyyy = dataErro.getFullYear();

                    var dataErro = dd + '/' + mm + '/' + yyyy;



                    var insertQuery = "INSERT INTO notinhas (data,descricao, nome, caminho_imagem, status,chat_id,msg_id) values (?,?,?,?,?,?,?)";
                    connection.query(insertQuery,[dataErro,descricao,autor, caminho_db, status,,ctx.message.chat.id,ctx.message['message_id']], function(err, rows){
                    if (err) {
                        ctx.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err,{'reply_to_message_id':ctx.message['message_id']});
                    }
                    else 
                    {
                        var insertQuery = "SELECT * FROM my_schema.notinhas ORDER BY id DESC LIMIT 1;";
                        var info_estabelecimento = ""

                        connection.query(insertQuery, function(err, recordset){

                            //ctx.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + recordset[0].id + '\n\nId do registro: ' + recordset[0].id);
                            ctx.replyWithMarkdown(msg_user + 'Registro com Erro:\n' + erros + '\n\nFavor corrigir!\n\nPara apagar envie *Apagar_nota: ' + recordset[0].id + '*',{'reply_to_message_id':ctx.message['message_id']});
                        });
                    }});



                }
                else{
                    var status='Aberto';
                    var insertQuery = "INSERT INTO notinhas ( valor, cnpj, cc, data, cartao, descricao, nome, caminho_imagem, status, responsavel_dif, tipo_gasto,chat_id,msg_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    connection.query(insertQuery,[valor, my_cnpj, cc, data, cartao, descricao, autor, caminho_db, status, responsavel_dif, tipo_gasto,ctx.message.chat.id,ctx.message['message_id']], function(err, rows){
                    if (err) {
                        ctx.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err,{'reply_to_message_id':ctx.message['message_id']});
                    }
                    else 
                    {
                        var insertQuery = "SELECT * FROM my_schema.notinhas ORDER BY id DESC LIMIT 1;";
                        var info_estabelecimento = ""

                        connection.query(insertQuery, function(err, recordset){
                            cnpj.consultaCNPJ({cnpj: my_cnpj.replace(/\D/g,'') })
                            .then(result => {
                                if (result.status.localeCompare('OK') == 0) {
                                    info_estabelecimento = '\n\nEstabelecimento:\n' + result.nome + '\n' + result.atividade_principal[0].text + '\n' + result.logradouro + ', ' + result.municipio + ' - ' + result.uf
                                    } else {
                                    info_estabelecimento = '\n\nEstabelecimento não encontrado\nErro: ' + result.message}

                                    //ctx.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + recordset[0].id + '\n\nId do registro: ' + recordset[0].id);
                                    ctx.replyWithMarkdown(msg_user + 'Registrei com sucesso:\n' + my_msg + info_estabelecimento + '\n\nPara apagar envie *Apagar_nota: ' + recordset[0].id + '*',{'reply_to_message_id':ctx.message['message_id']});
                            })
                            .catch(error => {
                                info_estabelecimento = '\n\nEstabelecimento não encontrado\nErro: ' + error
                                //ctx.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + recordset[0].id + '\n\nId do registro: ' + recordset[0].id);
                                ctx.replyWithMarkdown(msg_user + 'Registrei com sucesso:\n' + my_msg + info_estabelecimento + '\n\nPara apagar envie *Apagar_nota: ' + recordset[0].id + '*',{'reply_to_message_id':ctx.message['message_id']});
                                get_date()
                                console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - Error 03');
                                console.log(error)
                            })
                        });
                    }});
                }
	        }
	        else
	        {
	        	ctx.reply(msg_user + 'Você esqueceu de mandar a foto da notinha, mande novamente !',{'reply_to_message_id':ctx.message['message_id']});
	        }
	        
	    } else {
	        var dataErro = new Date();
	        var descricao = 'A mensagem não está no padrão correto, dados coletados: \n\n' + ctx.message['caption'];
	        var dd = String(dataErro.getDate()).padStart(2, '0');
	        var mm = String(dataErro.getMonth() + 1).padStart(2, '0'); //January is 0!
	        var yyyy = dataErro.getFullYear();

	        var dataErro = dd + '/' + mm + '/' + yyyy;

	    	const files = ctx.update.message.photo;
        	fileId = files[files.length-1].file_id;

            get_date();
            var caminho = './public/img/notas/' + ctx.message['from']['first_name'] + '_' +  + dataErro.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';
            var caminho_db = '/img/notas/' + ctx.message['from']['first_name'] + '_' +  + dataErro.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';
            

            ctx.telegram.getFileLink(fileId).then(function(url){ 
				url = url['href'];   
			    axios({url, responseType: 'stream'}).then(response => {
			        return new Promise((resolve, reject) => {
			            response.data.pipe(fs.createWriteStream(caminho));
	                });
	            }).catch(function(e){
						console.log(e);
					})
			}).catch(function(e){
					console.log(e);
				});


	        var insertQuery = "INSERT INTO notinhas (descricao,nome, data,status,chat_id,msg_id,caminho_imagem) values (?,?,?,?,?,?,?)";
	        connection.query(insertQuery,[descricao, autor,String(dataErro),'Erro Registro',ctx.message.chat.id,ctx.message['message_id'],caminho_db], function(err, rows){
	            if (err) {
	                console.log(err);
	                ctx.reply(msg_user + 'A mensagem não está no padrão correto !',{'reply_to_message_id':ctx.message['message_id']});
	            }
	            else{
	                var insertQuery = "SELECT * FROM my_schema.notinhas ORDER BY id DESC LIMIT 1;";
	                connection.query(insertQuery, function(err, recordset){

	                     ctx.replyWithMarkdown( msg_user + '\n\nA mensagem não está no padrão correto !' + "\n\nPara apagar envie *Apagar_nota: "+recordset[0].id+ '*',{'reply_to_message_id':ctx.message['message_id']});
	                });
	            }
	        });
	        
	    }

	}









	async function abastecimento(ctx){
		var autor = ctx.message['from']['first_name']+' '+ctx.message['from']['last_name'];
		let msg_user = 'Olá ' + ctx.message['from']['first_name']+' '+ctx.message['from']['last_name'] +', sou SARR.\n'

	    var mesagem_divida = ctx.message['caption'].split("|");

	    //Verifica se o texto está certo
	    if (((mesagem_divida.length - 1) == 7) || ((mesagem_divida.length - 1) == 8)) {

	        var valor = mesagem_divida[0].replace(/[^0-9,.]/g, '');
	        var cnpj = mesagem_divida[1].replace(/\D/g,'');
	        var cc = mesagem_divida[2].replace(/\D/g,'');
	        var data = mesagem_divida[3].replace(/\D/g,'');
	        if (mesagem_divida[4].toUpperCase().trim().startsWith('FATURADO')) {var cartao = "Faturado";}
	        else {var cartao = mesagem_divida[4].replace(/\D/g,'');}
	        var litros = mesagem_divida[5].replace(/[^0-9,.]/g, '');
	        var km = mesagem_divida[6].replace(/\D/g,'');
	        var placa = mesagem_divida[7].replace("[^a-zA-Z0-9]", "");
	        placa = mesagem_divida[7].replace("-", "");
	        placa = placa.replace(/\s/g, '');

	        var responsavel_dif = 0;
	        if ((mesagem_divida.length - 1) == 8) {responsavel_dif = mesagem_divida[8].replace(/\D/g,'');}

	        var contem_erro = 0;
	        var erros = "";

	        data = data.slice(6, 8) + "/" + data.slice(4, 6) + "/" + data.slice(0, 4)
	        // var data_db = data.slice(6, 10) + "-" + data.slice(3, 5) + "-" + data.slice(0, 2)

	        cnpj = cnpj.slice(0, 2) + "." + cnpj.slice(2, 5) + "." + cnpj.slice(5, 8) + "/" +  cnpj.slice(8, 12) + "-" +  cnpj.slice(12, 15);
	        placa = placa.slice(0, 3) + "-" + placa.slice(3, 7)

	        //my_msg = 'Centro de custos: ' + cc + '\nData: ' + data + '\nValor: ' + valor + '\nCNPJ: ' + cnpj + '\nCartão: ' + cartao + '\nDescrição: ' + descricao;
	        var my_msg = '- CC ' + cc + '\n- ' + data + '\n- R$' + valor + '\n- ' + cnpj + '\n- ' + cartao + '\n- ' + litros + '\n- ' + km + '\n- ' + placa;
	        if ((mesagem_divida.length - 1) == 6) {my_msg = my_msg + '\nCusto enviado para o ID: ' + responsavel_dif;}

	        var regex  = /^[0-9]{1,}([,.][0-9]{1,3})?$/;
	        if (regex.test(valor)) {valor = valor.replace(",",".")} else {contem_erro = 1; erros = erros + "\nEsse valor de gasto não está no padrão correto"}
	        if ((cnpj.length) != 18 ) {contem_erro = 1; erros = erros + "\nEsse número de CNPJ não está correto"}
	        if ((cc.length) <= 1 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar um centro de custos"}
	        if ((data.length) != 10) {contem_erro = 1; erros = erros + "\nVocê não digitou uma data no formato aceitável"}
	        if ((cartao.length) != 4 && (cartao.length) != 8  ) {contem_erro = 1; erros = erros + "\nO número do cartão está no formato incorreto"}
	        if (regex.test(litros)) {litros = litros.replace(",",".")}	else {contem_erro = 1; erros = erros + "\nEsse valor de litros não está no padrão correto"}
	        if ((km.length) < 1 ) {contem_erro = 1; erros = erros + "\nPrecisa adicionar uma quilometragem"}
	        if (((mesagem_divida.length - 1) == 7) && ((responsavel_dif.length) <= 1)) {contem_erro = 1; erros = erros + "\nNesse formato você precisa adicionar um id de responsável."}

	        if (contem_erro) {
                var status='Erro Registro';
                descricao ='Erros: '+erros+'\n\nDados coletados:\n'+my_msg;

                var dataErro = new Date();
    
                var dd = String(dataErro.getDate()).padStart(2, '0');
                var mm = String(dataErro.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = dataErro.getFullYear();

                var dataErro = dd + '/' + mm + '/' + yyyy;



                var insertQuery = "INSERT INTO abastecimento (data,descricao, nome, caminho_imagem, status,chat_id,msg_id) values (?,?,?,?,?,?,?)";
                connection.query(insertQuery,[dataErro,descricao,autor, caminho_db, status,,ctx.message.chat.id,ctx.message['message_id']], function(err, rows){
                if (err) {
                    ctx.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err,{'reply_to_message_id':ctx.message['message_id']});
                }
                else 
                {
                    var insertQuery = "SELECT * FROM my_schema.abastecimento ORDER BY id DESC LIMIT 1;";
                    var info_estabelecimento = ""

                    connection.query(insertQuery, function(err, recordset){

                        //ctx.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + recordset[0].id + '\n\nId do registro: ' + recordset[0].id);
                        ctx.replyWithMarkdown(msg_user + 'Registro com Erro:\n' + erros + '\n\nFavor corrigir!\n\nPara apagar envie *Apagar_nota: ' + recordset[0].id + '*',{'reply_to_message_id':ctx.message['message_id']});
                    });
                }});



            }
	        else {

	            const files = ctx.update.message.photo;
	        	fileId = files[files.length-1].file_id;

                get_date();
                var caminho = './public/img/abastecimento/' + ctx.message['from']['first_name'] + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';
                var caminho_db = '/img/abastecimento/' + ctx.message['from']['first_name'] + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';
                

                ctx.telegram.getFileLink(fileId).then(function(url){ 
					url = url['href'];   
				    axios({url, responseType: 'stream'}).then(response => {
				        return new Promise((resolve, reject) => {
				        	console.log('Salvando imagem: '+caminho);
				            response.data.pipe(fs.createWriteStream(caminho));
		                });
		            }).catch(function(e){
						    console.log(e);
						})
				}).catch(function(e){
					    console.log(e);
					});

                var insertQuery = "INSERT INTO abastecimento ( valor, cnpj, cc, data, cartao, litros, km, nome, caminho_imagem, status, responsavel_dif, placa,chat_id,msg_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                await connection.query(insertQuery,[valor, cnpj, cc, data, cartao, litros, km, autor, caminho_db, 'Aberto', responsavel_dif, placa,ctx.message.chat.id,ctx.message['message_id']], function(err, rows){
                	
	                if (err) {
	                    ctx.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err,{'reply_to_message_id':ctx.message['message_id']});
	                }
	                else 
	                {
	                    var insertQuery = "SELECT * FROM my_schema.abastecimento ORDER BY id DESC LIMIT 1;";
	                    connection.query(insertQuery, function(err, recordset){
	                		console.log('DADOS ARMAZENADOOOOOOOOOOOOOOOOOOOOOS: \n\n\n\n'+recordset[0]['id']);


	                        //ctx.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + recordset[0].id + '\n\nId do registro: ' + recordset[0].id);
	                        ctx.replyWithMarkdown(msg_user + 'Registrei com sucesso:\n' + my_msg + '\n\nPara apagar envie *Apagar_abastecimento: ' + recordset[0].id + '*',{'reply_to_message_id':ctx.message['message_id']});
	                });
	            }});
		    }
	    } else {
	        var dataErro = new Date();
	        var descricao = 'A mensagem não está no padrão correto, dados coletados: \n\n' + ctx.message['caption'];
	        var dd = String(dataErro.getDate()).padStart(2, '0');
	        var mm = String(dataErro.getMonth() + 1).padStart(2, '0'); //January is 0!
	        var yyyy = dataErro.getFullYear();

	        var dataErro = dd + '/' + mm + '/' + yyyy;

	    	const files = ctx.update.message.photo;
        	fileId = files[files.length-1].file_id;

            get_date();
            var caminho = './public/img/abastecimento/' + ctx.message['from']['first_name'] + '_' +  + dataErro.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';
            var caminho_db = '/img/abastecimento/' + ctx.message['from']['first_name'] + '_' +  + dataErro.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';
            

            ctx.telegram.getFileLink(fileId).then(function(url){ 
				url = url['href'];   
			    axios({url, responseType: 'stream'}).then(response => {
			        return new Promise((resolve, reject) => {
			            response.data.pipe(fs.createWriteStream(caminho));
	                });
	            }).catch(function(e){
						console.log(e);
					})
			}).catch(function(e){
					console.log(e);
				});




	        var insertQuery = "INSERT INTO abastecimento (descricao,nome, data,status,chat_id,msg_id,caminho_imagem) values (?,?,?,?,?,?,?)";
	        connection.query(insertQuery,[descricao, autor,String(dataErro),'Erro Registro',ctx.message.chat.id,ctx.message['message_id'],caminho_db], function(err, rows){
	            if (err) {
	                console.log(err);
	                ctx.reply(msg_user + 'A mensagem não está no padrão correto !',{'reply_to_message_id':ctx.message['message_id']});
	            }
	            else{
	                var insertQuery = "SELECT * FROM my_schema.abastecimento ORDER BY id DESC LIMIT 1;";
	                connection.query(insertQuery, function(err, recordset){

	                     ctx.replyWithMarkdown( msg_user + '\n\nA mensagem não está no padrão correto !' + "\n\nPara apagar envie *Apagar_abastecimento: "+recordset[0].id+ '*',{'reply_to_message_id':ctx.message['message_id']});
	                });
	            }
	        });
	        
	    }
	}



	function get_date() {

	    // current date
	    // adjust 0 before single digit date
	    date = ("0" + date_ob.getDate()).slice(-2);
	    // current month
	    month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
	    // current year
	    year = date_ob.getFullYear();
	    // current hours
	    hours = date_ob.getHours();
	    // current minutes
	    minutes = date_ob.getMinutes();
	    // current seconds
	    seconds = date_ob.getSeconds();

	}

};